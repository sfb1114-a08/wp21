#!/bin/bash

#SBATCH --array=1-9

PYPATH=/net/scratch/cippool/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/pyscripts/dist_plot.py
TRAPATH=/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/era5/traj/2016/

# Define an array of parameters
PARAMS=(1 2 5 10 20 50 100 200 500)

# Select a parameter based on SLURM_ARRAY_TASK_ID
PARAM=${PARAMS[$SLURM_ARRAY_TASK_ID]}

python ${PYPATH} ${TRAPATH} "all" ${PARAM}
