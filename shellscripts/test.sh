#!/bin/bash

#SBATCH --nodes=1                                                                                       
#SBATCH --ntasks-per-node=1                                                                             
#SBATCH --cpus-per-task=1 
#SBATCH --array=1-29

# bei array=1-1 kein Problem, bei array=1-24 Problem für alle tasks

year=2017
PFAD=/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/era5
PYPFAD=${PFAD}/../pyscripts
CSPFAD=/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/csstandalone
OPATH=/home/schoelleh96/Nextcloud/ObsidianVault/Work/Projects/n_t.md

#for i in {13..24}
#do
cd ${PFAD}/startf/${year}
startfiles=`ls -v startf*`
CurrStartf=`echo $startfiles | cut --delimiter " " --fields $SLURM_ARRAY_TASK_ID`
# CurrStartf=`echo $startfiles | cut --delimiter " " --fields $i`

#    echo "Startfiles:"
#    echo ${startfiles}
    #echo ${SLURM_ARRAY_TASK_ID}
echo ${CurrStartf}

cd ${PFAD}

#./traj_slurm_era5_calc.sh ${CurrStartf} > ${CurrStartf}_traj.out

filename=${CurrStartf##*/}
Y1=${filename:7:4}
M1=${filename:11:2}
D1=${filename:13:2}
H1=${filename:16:2}

#    if [ "$H1" = "00" ] ; then

#	S1=$(date -d "$Y1-$M1-$D1" "+%s")
#	S2=$((${S1} - ((4*86400))))
#	date2=$(date --date="@$S2" "+%Y%m%d")
#	Y2=${date2:1:4}
#	M2=${date2:5:2}
#	D2=${date2:7:2}

#	rm -r ${PFAD}/cache/${Y1}${M1}/${D2}
#    fi


eval "$(conda shell.bash hook)"
conda activate /home/schoelleh96/miniforge3/envs/wp21

#source /home/schoelleh96/Applications/envs/wp21env/bin/activate

#python ${PYPFAD}/epsloglog.py ${PFAD}/traj/${year}/ ${Y1}${M1}${D1}_${H1}
    
#sbatch --wrap "$CSPFAD/calc_bound.sh $PFAD/traj/$year/  $Y1 $M1 $D1 $H1"

#python ${CSPFAD}/calc_dist.py ${PFAD}/traj/${year}/  ${Y1} ${M1} ${D1} ${H1}

#python ${PYPFAD}/dist_plot.py ${PFAD}/traj/${year}/ ${Y1}${M1}${D1}_${H1} "100"

python ${CSPFAD}/calc_E.py ${PFAD}/traj/${year}/ ${Y1} ${M1} ${D1} ${H1}

#python ${PYPFAD}/BoundR_plot.py ${PFAD}/traj/${year}/ ${Y1}${M1}${D1}_${H1}

#n_p=$(wc -l < ${PFAD}/startf/${year}/${CurrStartf})

#echo ${n_p}

#echo "| ${Y1}${M1}${D1}_${H1} | ${n_p} |"

#echo "| ${Y1}${M1}${D1}_${H1} | ${n_p} |" >> ${OPATH}

#done
