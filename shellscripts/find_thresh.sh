#!/bin/bash                                                                                             

# calculate clusters for all dates and plot their respective dth distributions

year=2016
basedir=/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21
tradir=traj/${year}/

cd ${basedir}/${tradir}

trfiles=`ls -v traj*`

cd ${basedir}
i=1

for trfile in ${trfiles}
do
    trf=${trfile##*/}

    Y=${trf:5:4}
    M=${trf:9:2}
    D=${trf:11:2}
    H=${trf:14:2}
    echo "$Y $M $D $H"

    python find_threshold.py ${basedir}/${tradir} ${Y} ${M} ${D} ${H} "hist"

done
