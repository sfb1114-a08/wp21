#!/bin/bash

#SBATCH --array=1-25

# 28 for 2017

eval "$(conda shell.bash hook)"
conda activate /home/schoelleh96/miniforge3/envs/wp21

PYPATH=/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/pyscripts/epsloglog.py
TRAPATH=/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/era5/traj/2016/

cd ${TRAPATH}
Trafiles=`ls -v traj*`
CurrTraF=`echo $Trafiles | cut --delimiter " " --fields $SLURM_ARRAY_TASK_ID`

echo $CurrTraF

filename=${CurrTraF##*/}
Y1=${filename:5:4}
M1=${filename:9:2}
D1=${filename:11:2}
H1=${filename:14:2}

python ${PYPATH} ${TRAPATH} ${Y1}${M1}${D1}_${H1} 

#python ${PYPATH} ${TRAPATH} "all"
