#!/bin/bash

# plot trajectories from all starting points divided by wether they were heated
# or not and stich together in a movie

plot_case="split4"
year=2016
basedir=/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21
tradir=traj/${year}/

cd ${basedir}/${tradir}

trfiles=`ls -v traj*`

cd ${basedir}
i=1

for trfile in ${trfiles}
do
    trf=${trfile##*/}
    
    Y=${trf:5:4}
    M=${trf:9:2}
    D=${trf:11:2}
    H=${trf:14:2}
    echo "$Y $M $D $H"

    python main.py ${basedir}/${tradir} ${Y} ${M} ${D} ${H} ${plot_case}

    case $plot_case in
	"sets")
	    cp ${basedir}/im/sets/set_${Y}${M}${D}_${H}*.png ${basedir}/im/sets/forgif/
	    cd ${basedir}/im/sets/forgif
	    ffmpeg -i set_${Y}${M}${D}_${H}0001.png -vf palettegen=256 palette.png
	    ffmpeg -framerate 8 -i set_${Y}${M}${D}_${H}%04d.png -i palette.png -filter_complex "fps=8, scale=1440:-1:flags=lanczos[x];[x][1:v]paletteuse" -y ${year}${M}${D}${H}.gif
	    rm ./*.png
	    cd ${basedir} ;;
	"set_comb3d")
	    cp ${basedir}/im/setsc3d/setc_${Y}${M}${D}_${H}*.png ${basedir}/im/setsc3d/forgif/
            cd ${basedir}/im/setsc3d/forgif
            ffmpeg -i setc_${Y}${M}${D}_${H}0001.png -vf palettegen=256 palette.png
            ffmpeg -framerate 8 -i setc_${Y}${M}${D}_${H}%04d.png -i palette.png -filter_complex "fps=8, scale=1440:-1:flags=lanczos[x];[x][1:v]paletteuse" -y ${year}${M}${D}${H}.gif
            rm ./*.png
            cd ${basedir} ;;
	"split2") cp ${basedir}/im/split2/traj_${Y}${M}${D}_${H}.png ${basedir}/im/split2/forgif/$(printf "%03d.png" $i) ;;
	"split4") cp ${basedir}/im/split4/traj4_${Y}${M}${D}_${H}.png ${basedir}/im/split4/forgif4/$(printf "%03d.png" $i) ;;
    esac
    
    i=$(expr $i + 1)
done

case $plot_case in
    "split2")
        cd ${basedir}/im/split2/forgif
#	echo "hier"
	ffmpeg | echo
	ffmpeg -i 001.png -vf palettegen=256 palette.png
	ffmpeg -framerate 1 -i %03d.png -i palette.png -filter_complex "fps=1, scale=1440:-1:flags=lanczos[x];[x][1:v]paletteuse" -y ${year}.gif;;
     "split4")
	 cd ${basedir}/im/split4/forgif4
	 ffmpeg -i 001.png -vf palettegen=256 palette.png
	 ffmpeg -framerate 1 -i %03d.png -i palette.png -filter_complex "fps=1, scale=1440:-1:flags=lanczos[x];[x][1:v]paletteuse" -y ${year}_4.gif;;
esac

rm ./*.png
