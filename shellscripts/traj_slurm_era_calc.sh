#!/bin/bash

# SBATCH --job-name=era5traj #_%a
# SBATCH --ntasks-per-node=1
# SBATCH --nodes=1
# SBATCH --partition=main
# SBATCH --output=./out/era5traj.out
# SBATCH --error=./out/era5traj.err
# SBATCH --mail-type=ALL
# SBATCH --mail-user=henry.schoeller@fu-berlin.de

# !!! call: sbatch --array=0-41 traj_slurm_era_calc.sh

# Waehle uebergeordneten Arbeitsordner und Zielordner
FLOC=/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/vars
PFAD=/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21
TRA_DIR=${PFAD}/traj
ARC_DIR=${PFAD}/arch

#===============================================================================
# DEFINITIONSTEIL
#===============================================================================

module purge
#module load CDO/1.9.6-foss-2018b
#module load netCDF-Fortran/4.4.4-foss-2018b
#module load netcdf-c/4.3.3.1
#module load netcdf-gfortran/4.4.2
module load CDO/1.9.10-gompi-2021a
module load netCDF-Fortran/4.5.3-gompi-2021b

# Leite Jahr aus Slurm-TaskID ab
#year=$(($SLURM_ARRAY_TASK_ID+1979))
year=2016
echo "year ${year}"

# Erstelle temporaere Ordner
DATA_CACHE=${PFAD}/tmp/${year}
INP_DIR=${PFAD}/cache/${year}
mkdir -p ${DATA_CACHE}
mkdir -p ${INP_DIR}
mkdir -p ${TRA_DIR}/${year}

# Waehle Art und Pfad der Daten
INPUT_TYPE=era5kit
DATA_ERA5KIT="/daten/erafive/arch/lagranto/KIT_05Grad"
ERA5_DHR=3
USE_DHR=no # no ... jede Datei wird genutzt, yes ... nur im angeforderten Abstand einbinden


# Waehle Lagrantoversion, abhaengig von INPUT_TYPE

case $INPUT_TYPE in
    cclm-vast )
	export LAGRANTO=${LAGRANTO:-/net/opt/lagranto/cosmo/gfortran-6.3.0/2.0} ;;
    era5kit|era5 )
	export LAGRANTO=${LAGRANTO:-/net/opt/lagranto/ecmwf/gfortran-11.2.0/2.7} ;;
    iconeu )
	export LAGRANTO=${LAGRANTO:-/net/opt/lagranto/iconeu/gfortran-6.3.0/2.7} ;;
    * ) echo "# ${PROC_NAME}: Input Procedure [ ${INPUT_TYPE} ] not implemented"; exit 99;;
esac

# Printe die verwendete Lagrantoversion
echo "LAGRANTO = $LAGRANTO" 

# Setze das netcdf-Format und den default-Pfad fuer Lagranto
export NETCDF_FORMAT=CF
# evtl. PATH-Variable zuruecksetzen mit
#PATH=$(getconf PATH)
export PATH=${LAGRANTO}:${PATH}

# Definiere Pfade zu Hilfsdateien:
TOP_DIR=/net/opt/lagranto/traj_showcases/l_all
PFADlag=/net/opt/lagranto/traj_showcases/l_all/base
PFADec=/net/opt/lagranto/ecmwf/gfortran-6.3.0/2.7
TOOL_DIR=${TOP_DIR}/tools
ERA5KIT_INPUT=${TOOL_DIR}/create_symlinks.py # bindet Daten aus era5kit Archiv ein
PGEO_TOOL=${TOOL_DIR}/calc_geopot.sh      # Berechung der Geopot-Fläche

# error checking: http://web.archive.org/web/20110314180918/http://www.davidpashley.com/articles/writing-robust-shell-scripts.html
set -e 

cd ${PFAD}/startf/${year}

startfiles=`ls -v startf*`

echo "startfiles = $startfiles"

#===============================================================================
# ENDE DES DEFINITIONSTEILS
#===============================================================================
cd ${INP_DIR}

#===============================================================================

for startf in ${startfiles} 
do
    
    if [ -f ${PFAD}/startf/${year}/${startf} ]; then

	echo "*** $startf ***"

	# link startf                                                                                    
	ln -sf ${PFAD}/startf/${year}/${startf} .
	# get date                                                                                       
	file2=${startf##*/}
        #-------------------------------------------------------------------------------                 
	# 1: Datumsbestimmung: Ermittle                                                                  
        #               date1,Y1,M1,D1,H1 (Enddatum aus Dateinamen) und                                  
        #               date2,Y2,M2,D2,H2 (=Enddatum - DT Tage)                                          
	#-------------------------------------------------------------------------------                 
        Y1=${file2:7:4}
        M1=${file2:11:2}
        D1=${file2:13:2}
        H1=${file2:16:2}
	DT=7
	
        if [ $(( 10#$D1 )) -le $(( 10#$DT )) ]
	then
	    if [ ${M1} -eq 1 ]
            then
		Y2=$(( 10#$Y1 - 1 ))
		M2=12
		D2=$(( 10#$D1 + 31 - 10#$DT ))
	    else
		Y2=${Y1}
		M2=$(( 10#$M1-1 ))
		if [ ${M2} -eq 1 ] ||  [ ${M2} -eq 3 ] || [ ${M2} -eq 5 ] || [ ${M2} -eq 7 ] || [ ${M2} -eq 8 ] || [ ${M2} -eq 10 ]
		then
		    D2=$(( 10#$D1+31-10#$DT ))
		fi
		if [ ${M2} -eq 2 ]
		then
		    if [ $Y1%4 -eq 0 ] && [ $Y1%100 -ne 0 ] || [ $Y1 -eq 2000 ]
		    then
			D2=$(( 10#$D1+29-10#$DT ))
		    else
			D2=$(( 10#$D1+28-10#$DT ))
		    fi
		fi
		if [ ${M2} -eq 4 ] ||  [ ${M2} -eq 6 ] || [ ${M2} -eq 9 ] || [ ${M2} -eq 11 ]
		then
		    D2=$(( 10#$D1+30-10#$DT ))
		fi
            fi
	    else
		Y2=${Y1}
		D2=$(( 10#$D1-10#$DT ))
		M2=${M1}
        fi

	M2=$( printf '%02d' $M2 )
        D2=$( printf '%02d' $D2 )
        H2=${H1}
        date=${Y1}${M1}${D1}_${H1}
        date2=${Y2}${M2}${D2}_${H2}
        echo ${date}, ${date2}

        YMD1=${Y1}${M1}${D1}${H1}
        YMD2=${Y2}${M2}${D2}${H2}
	
	#-------------------------------------------------------------------------------
	# 2. Verlinkung der ERA5-Daten
	# 		a. Erstelle Links in DATA_CACHE auf ERA5-Daten der ausgewaehlten 10 Tage
	#		b. Schleife ueber alle (auch evtl. bereits vorher vorhandenen) Links in DATA_CACHE:
	#				Nur falls Datum und Uhrzeit passend ist: Erstelle Links in INP_DIR auf die Entsprechungen in DATA_CACHE.
	#-------------------------------------------------------------------------------

	# a. Erstelle Links in DATA_CACHE auf ERA5-Daten der ausgewaehlten 10 Tage
	# Wechsle Ordner (von INP_DIR zu DATA_CACHE)
	#BASE_DIR=${DATA_ERA5KIT}
	cd ${DATA_CACHE}
			
	#${ERA5KIT_INPUT} -a ${BASE_DIR} -c ${DATA_CACHE} \
	    ${ERA5KIT_INPUT} -a ${DATA_ERA5KIT} -c ${DATA_CACHE} \
			     --yr1 ${Y2} --mo1 ${M2} --dy1 ${D2} --hr1 ${H2} \
			     --yr2 ${Y1} --mo2 ${M1} --dy2 ${D1} --hr2 ${H1} \
			     -i ${ERA5_DHR}
				
			
	    # b. Schleife ueber alle Links zu ERA5-P-(netcdf-)Dateien in DATA_CACHE

	    # Wechsle zurueck zu INP_DIR
	    cd ${INP_DIR}
	    
	    for FILE in ${DATA_CACHE}/P*
	    do
		# Ermittle YMD und HR aus Dateinamen, zB:
		YMD=${FILE##*/P} # 19910703_18
		HR=${YMD##????????_} # 18
		YMD=${YMD%%_??}${HR} # 19910703

		case $USE_DHR in
		    # Wenn USE_DHR=yes, setze CHECK auf HR modulo ERA5_DHR. 
		    # Bsp1: HR=09, ERA5_DRH=3 -> CHECK=0, die Daten werden also verwendet.
		    # Bsp2: HR=11, ERA5_DRH=3 -> CHECK=2, die Daten werden also nicht verwendet
		    yes ) CHECK=$(echo $HR $ERA5_DHR | awk '{ print $1%$2}') ;; 
		    # Wenn USE_DRH=no, setze CHECK einfach auf 0, die Daten werden also in jedem Fall verwendet.
		    * )   CHECK=0 ;;
		esac
		
		# Wenn CHECK==0 und das Datum im richtigen Zeitraum liegt: 
		if [ ${YMD2} -le ${YMD} ] && [ ${YMD} -le ${YMD1} ] && [ $CHECK -eq 0 ]
		then
		    echo "Zeitschritt $YMD (Stunde $HR) aus [ $YMD2 , $YMD1 ] wird bearbeitet"
		    # NEWFILE: nur der Dateiname von FILE, ohne den Pfad (FILE enthaelt auch den Pfad)
		    NEWFILE=$(basename ${FILE})
		    
		    # Erzeuge Link mit dem Namen von NEWFILE auf die existierende Datei/den Link FILE
		    ln -sf ${FILE} ${NEWFILE} && \
			echo "file $NEWFILE linked [ $YMD $HR $CHECK ]"

				    # Binde auch die Dateien ein, deren Name mit H und Z beginnt (Dateien, die mit C beginnen, liegen nur sechsstuendig vor und enthalten nur CAPE)
				    #for SFILE in $(dirname ${FILE})/C${FILE##*/P} \
		    for SFILE in $(dirname ${FILE})/Z${FILE##*/P} \
							$(dirname ${FILE})/H${FILE##*/P}
		    do
			if [ -f ${SFILE} ] # if SFILE exists
			then
			    # Erzeuge Link mit dem Namen von NEWSFILE auf die existierende Datei/den Link SFILE
			    NEWSFILE=$(basename ${SFILE})
			    ln -sf ${SFILE} ${NEWSFILE} && \
				echo "file $NEWSFILE linked [ $YMD $HR $CHECK ]"
			fi
		    done
		fi
	    done

	    echo "Data prepared"

	    #-------------------------------------------------------------------------------
	    # 3. Rufe Lagranto zur Berechnung der Startfiles und Trajektorien auf
	    #-------------------------------------------------------------------------------

#	    ${PFADec}/create_startf.sh ${datstop} startf.2 "file(startfile) @  ${SETUP_VERT} @ hPa,agl" -changet
	    #${PFADec}/caltra.sh ${date} ${date2} ${file} traj.2 -j -o 60
            ${PFADec}/caltra.sh ${date} ${date2} ${startf} traj.2 -j -o 60
	    cp ${FLOC}/tracevars .
	    ${PFADec}/trace.sh traj.2 traj.2 -v tracevars
	    #echo "${PFADec}/caltra.sh ${date} ${date2} ${file} traj.2 -j -o 360"
	    # Verschiebe die Trajektoriendatei in passenden Ordner

	    mv traj.2 ${TRA_DIR}/${year}/traj_${date}
	    mv ${startf} ${ARC_DIR}/startf_${date}
    fi
done

#if [ -d ${TRA_DIR}/${region}/${year} ]; then 
#       tar -czvf ${TRA_DIR}/${region}/${year}.tar.gz ${TRA_DIR}/${region}/${year}
#       rm ${TRA_DIR}/${region}/${year}/*
#    fi

# Wenn der Job fertig ist (wenn alle Startfiles aus dem Jahr ${year} von allen Regionen behandelt wurden):

# Loesche DATA_CACHE und INP_DIR-Ordner 
#rm -rf $DATA_CACHE
#rm -rf $INP_DIR

# Loesche Elemente in DATA_CACHE und INP_DIR-Ordner (behalte erstmal leere Ordner zur Kontrolle)
rm -f $DATA_CACHE/*
rm -f $INP_DIR/*
	
# Loesche die temporaere Datei startf.2 (traj.2 wurde bereits verschoben)
#rm -f startf.2
