#!/bin/bash -l

#SBATCH --job-name=build_cache
#SBATCH --partition=main
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --array=0-13

export TASKS=1
export CACHE=/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/era5/cache/
source /net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/era5/build_cache.rc

module purge
module load CDO/1.9.10-gompi-2020a
hostname -f
which cdo

# first day in seconds:

D1=$(date -d "2017-01-20" "+%s")

# day for job array instance

DCurr=$((${D1} + ((${SLURM_ARRAY_TASK_ID}*86400))))
DCURR=$(date --date="@$DCurr" "+%Y%m%d")

echo ${DCURR}

BuildCacheDaily ${DCURR:0:4} ${DCURR:4:2} ${DCURR:6:2}

wait

cd ${CACHE}${DCURR:0:4}${DCURR:4:2}/${DCURR:6:2}

#files=`find ./ -name "*2016*" -type f`

files=`ls -v ./`
currp=`pwd`
echo ${currp}

for f in ${files}
do
    echo ${f}
    
    ln -s ${currp}/${f} ../../${f}
done



