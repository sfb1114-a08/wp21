#!/bin/bash

# SBATCH --job-name=era5traj #_%a
# SBATCH --ntasks-per-node=1
# SBATCH --nodes=1
# SBATCH --partition=main
# SBATCH --output=./out/era5traj.out
# SBATCH --error=./out/era5traj.err
# SBATCH --mail-type=ALL
# SBATCH --mail-user=henry.schoeller@fu-berlin.de

# !!! call: sbatch --array=0-41 traj_slurm_era_calc.sh

# Waehle uebergeordneten Arbeitsordner und Zielordner
FLOC=/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/vars
PFAD=/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/era5
TRA_DIR=${PFAD}/traj
ARC_DIR=${PFAD}/arch

#===============================================================================
# DEFINITIONSTEIL
#===============================================================================

module purge
module load CDO/1.9.10-gompi-2021a
module load netCDF-Fortran/4.5.3-gompi-2021b

year=2017
echo "year ${year}"
#DT=-3

# Erstelle temporaere Ordner
DATA_CACHE=${PFAD}/tmp/${year}
INP_DIR=${PFAD}/cache
mkdir -p ${DATA_CACHE}
mkdir -p ${TRA_DIR}/${year}

# Waehle Art und Pfad der Daten
INPUT_TYPE=era5

# Waehle Lagrantoversion, abhaengig von INPUT_TYPE

case $INPUT_TYPE in
    cclm-vast )
	export LAGRANTO=${LAGRANTO:-/net/opt/lagranto/cosmo/gfortran-6.3.0/2.0} ;;
    era5kit|era5 )
	export LAGRANTO=${LAGRANTO:-/net/opt/lagranto/ecmwf/gfortran-11.2.0/2.7} ;;
    iconeu )
	export LAGRANTO=${LAGRANTO:-/net/opt/lagranto/iconeu/gfortran-6.3.0/2.7} ;;
    * ) echo "# ${PROC_NAME}: Input Procedure [ ${INPUT_TYPE} ] not implemented"; exit 99;;
esac

# Printe die verwendete Lagrantoversion
echo "LAGRANTO = $LAGRANTO" 

# Setze das netcdf-Format und den default-Pfad fuer Lagranto
export NETCDF_FORMAT=CF
# evtl. PATH-Variable zuruecksetzen mit
#PATH=$(getconf PATH)
export PATH=${LAGRANTO}:${PATH}

# Definiere Pfade zu Hilfsdateien:
TOP_DIR=/net/opt/lagranto/traj_showcases/l_all
PFADlag=/net/opt/lagranto/traj_showcases/l_all/base
PFADec=/net/opt/lagranto/ecmwf/gfortran-6.3.0/2.7
TOOL_DIR=${TOP_DIR}/tools

set -e 

cd ${PFAD}/startf/${year}

startfiles=`ls -v startf*`

echo "startfiles = $startfiles"

#===============================================================================
# ENDE DES DEFINITIONSTEILS
#===============================================================================
cd ${INP_DIR}

#===============================================================================

for startf in ${startfiles} 
do

#startf=${1}

    echo ${startf}
    
    if [ -f ${PFAD}/startf/${year}/${startf} ]; then

	echo "*** $startf ***"

	# link startf                                                                                    
	ln -sf ${PFAD}/startf/${year}/${startf} .
	# get date                                                                                       


	file2=${startf##*/}
	#-------------------------------------------------------------------------------                 
	# 1: Datumsbestimmung: Ermittle                                                                  
	#               date1,Y1,M1,D1,H1 (Enddatum aus Dateinamen) und                                  
	#               date2,Y2,M2,D2,H2 (=Enddatum + DT Tage)                                          
	#-------------------------------------------------------------------------------                 
	Y1=${file2:7:4}
	M1=${file2:11:2}
	D1=${file2:13:2}
	H1=${file2:16:2}

	# Convert to seconds

	S1=$(date -d "$Y1-$M1-$D1" "+%s")

	echo ${S1}

	for DT in -3 3;
	do

	    # Add DT days

	    S2=$((${S1} + ((${DT}*86400))))

	    echo ${S2}
	    date2=$(date --date="@$S2" "+%Y%m%d")

	    echo ${date2}

	    date2=${date2}_${H1}

	    date=${Y1}${M1}${D1}_${H1}
	    echo ${date}, ${date2}

	    ${PFADec}/caltra.sh ${date} ${date2} ${startf} traj.2 -j -o 60
	    cp ${FLOC}/tracevars .
	    ${PFADec}/trace.sh traj.2 traj.2 -v tracevars

	    if [ $DT -gt 0 ]; then
		mv traj.2 ${TRA_DIR}/${year}/ftraj_${date}
	    else
		mv traj.2 ${TRA_DIR}/${year}/btraj_${date}
	    fi

	done
	mv ${startf} ${ARC_DIR}/startf_${date}
	#    fi
	#done
	
	source /home/schoelleh96/Applications/envs/wp21env/bin/activate
	python ${PFAD}/../pyscripts/comb_trajs.py ${TRA_DIR}/${year}/ ${date}

	rm ${TRA_DIR}/${year}/ftraj_${date}
	rm ${TRA_DIR}/${year}/btraj_${date}
    fi
done
# Loesche Elemente in DATA_CACHE und INP_DIR-Ordner (behalte erstmal leere Ordner zur Kontrolle)
#rm -f $DATA_CACHE/*
#rm -f $INP_DIR/*


