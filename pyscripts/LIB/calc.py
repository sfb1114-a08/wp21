#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 13 10:56:55 2023

@author: schoelleh96
"""
import numpy as np
import data as dd
from sklearn.neighbors import KDTree, BallTree
# from numba import jit

def omg2w(omg, T, p):
    '''
    calculates vertical velocity in m/s from omega in hPa, T in K and p in hPa
    assumes hydrostatic pressure, ideal gas behaviour

    Parameters
    ----------
    omg : array
        vertical velocity in pressure coordinates.
    T : array
        temperature in K.
    p : array
        pressure in hPa.

    Returns
    -------
    array
        w .

    '''
    # ! p and omg both in hPa or Pa
    R = 8.3145 # universal gas constant in Nm/(mol*K)
    M = 0.02896 # molar mass of air in kg/mol
    g = 9.806 # gravitational constant in m/^2

    return -(R * T * omg)/(M * p * g)

def calc_k(u, v, w):
    '''
    Calculate velocity-based scaling parameter

    Parameters
    ----------
    u : array
        horizontal velocity x direction.
    v : array
        horizontal velocity y direction.
    w : array
        vertical velocity.

    Returns
    -------
    float
        scaling parameter.

    '''
    u_h = np.sqrt(u**2 + v**2)
    return u_h.mean()/(np.abs(w)).mean()

def norm(x):
    '''
    Normalize along 0th axis

    Parameters
    ----------
    x : array
        data.

    Returns
    -------
    array
        normalized.

    '''
    return (x-x.mean(axis=0))/x.std(axis=0)

def project(lat, lon, srcp='+proj=latlon', dstp='+proj=stere +lat_0=90'+
                  ' +lat_ts=50', Inverse=False):
    '''
    Project a 2 coordinate arrays in projection srcp to projection dstp
    Returns a tuple.

    Parameters
    ----------
    lat : array
        first coordinate.
    lon : array
        second coordinate.
    srcp : string, optional
        defines first projection using PROJ4 string. The default is 'latlong'.
    dstp : string, optional
        defines second projection using PROJ4 string. The default is
        '+proj=stere +lat_0=90 +lat_ts=50'.

    Returns
    -------
    tupel of arrays
        transformed x and y coordinates.

    '''
    from pyproj import CRS, Transformer
    crs1 = CRS.from_proj4(srcp)
    crs2 = CRS.from_proj4(dstp)
    trans = Transformer.from_crs(crs1, crs2, always_xy=True)

    if Inverse:
        return trans.transform(lat, lon, direction="INVERSE")
    else:
        return trans.transform(lon, lat)

def ptoh(p, p0=1013.25, H0=8.435):
    '''
    converts pressure (in hPa) to height (in km) assuming standard
    atmosphere

    Parameters
    ----------
    p : numpy array
        pressure.
    p0 : scalar
        ground level pressure.
    H0 : scalar
        constant.

    Returns
    -------
    h : numpy array
        height in km.

    '''
    from numpy import log

    h = -log(p/p0) * H0
    return h

def lonlatrtoxyz(lon, lat, r):
    '''
    Convert lon, lat and r to 3D cartesian coordinates.

    Parameters
    ----------
    lon : numpy array
        longitude.
    lat : numpy array
        latitude.
    r : numpy array
        distance from earth's center.

    Returns
    -------
    X : numpy array
        1st cartesian coordinate.
    Y : numpy array
        2nd cartesian coordinate.
    Z : numpy array
        3rd cartesian coordinate.
    '''
    from numpy import sin, cos, pi
    lon = lon * pi/180
    lat = lat * pi/180

    x = r * cos(lat) * cos(lon)
    y = r * cos(lat) * sin(lon)
    z = r * sin(lat)

    return (x, y, z)

def kcluster_idx(E, N_k, N_v=None):
    '''
    k-means clustering based on space-time diffusion coordinates

    Parameters
    ----------
    E : list
        eigenvalues at 0 and eigenvectors at 1.
    N_k :int
        number of desired clusters.
    N_v : int
        number of eigenvectors to use for clustering

    Returns
    -------
    kcluster : fitted estimator
        contains .labels_ which labels the given points, cluster_centers_,
        which gives the cluster centers.

    '''
    from numpy import real
    from sklearn.cluster import KMeans

    evecs = real(E[1])
    if N_v is None:
        N_v = N_k-1
    kcluster = KMeans(n_clusters=N_k, n_init='auto').fit(evecs[:,0:(N_v)])
    return kcluster

# @jit(nopython=True)
def eigen(m, Tmin, Tmax, x_list, y_list, v_list, epsilon, bounds=None, N_k=20,
          d_path=None, x=None, y=None, z=None, customMetric=None, k=None,
          z_coord_type=None):
    '''
    Compute diffusion map eigenfunctions

    Parameters
    ----------
    m : int
        number of points.
    Tmin : int
        first time slice.
    Tmax : int
        last time slice.
    x_list : list
        x position  values of sparse matrix entries.
    y_list : list
        y position  values of sparse matrix entries.
    v_list : list
        values of sparse matrix entries
    epsilon: float
        diffusion coefficient
    bounds : 1D numpy array, optional
        array defining boundary points, default value None
    N_k : int, optional
        Number of eigenvectors and eigenvalues. Default value 20
    Returns
    -------
    list
        eigenvalues and eigenvectors (diffusion maps).

    '''
    from scipy.sparse import csc_matrix, find, identity
    from scipy.sparse.linalg import eigs
    from scipy.linalg import eig
    from warnings import catch_warnings, simplefilter

    P = csc_matrix((m,m))
    for j in range(0, Tmax-Tmin):
        # load distances in place to save RAM
        if not(d_path is None):
            # reduce path to dat folder
            d_path = d_path[:(len(d_path) - d_path[::-1].find("/"))]
            # x_list, y_list, v_list = dd.io_dist(d_path, epsilon,
            #                                     x, y, z, customMetric,
            #                                     k, z_coord_type, [j],
            #                                     force_calc=True)
            # K = csc_matrix((np.exp(-v_list[0]**2/epsilon), (x_list[0],
                                                        # y_list[0])),
                       # shape=(m,m))
            # calc because of lack of storage

            x_list, y_list, v_list = distances(x[:,j], y[:,j],
                                                  3*np.sqrt(epsilon),
                                                  x.shape[0],
                                                  dataZ=z[:,j],
                                                  customMetric=customMetric,
                                                  k=k)
            K = csc_matrix((np.exp(-v_list**2/epsilon), (x_list,
                                                        y_list)),
                       shape=(m,m))
        else:
            K = csc_matrix((np.exp(-v_list[j]**2/epsilon), (x_list[j],
                                                        y_list[j])),
                       shape=(m,m))
        K = K + identity(m) + K.T

        with catch_warnings():
            simplefilter("ignore")
            K = K.multiply(1/K.sum(axis=1))# 1/rowsums
        if not(bounds is None):
            K = K.tolil()
            K[np.where(bounds[:,j]==1)[0],:]=0
            K[:,np.where(bounds[:,j]==1)[0]]=0
            K = K.tocsr()
            K.eliminate_zeros()

        sparsity = find(K)[0].shape[0]/(K.shape[0]*K.shape[1])

        print("sparsity: {}".format(sparsity))

        P = P + K

    P = 1/(Tmax - Tmin) * P

    sparsity = find(P)[0].shape[0]/(P.shape[0]*P.shape[1])

    print("sparsity for epsilon {}: {}".format(epsilon, sparsity))
    try:
        vals, vecs = eigs(P, k=N_k)
    except:
        print("Eigs failed, using eig")
        vals, vecs = eig(P.toarray())

    vals = np.flip(np.sort(vals))[:N_k]
    return list((vals, vecs))

def dth_minmax(data, hmin):
    '''
    max/min change of Theta until time where smallest/biggest Theta

    Parameters
    ----------
    data : numpy array
        containing th.
    hmin : int
        timesteps maximum.

    Returns
    -------
    dth : list
        biggest difference in theta.

    '''

    from numpy import argmin, argmax, min, max, empty, absolute, r_, size

    # this takes always the biggest difference:
    # dth = max(data['TH'][:,:hmin],axis=1) - min(data['TH'][:,:hmin], axis=1)
    ind_r = argmin(data[:,:hmin], axis=1) #; ind_c = arange(0, len(ind_r))
    ind_l = argmax(data[:,:hmin], axis=1)
    dth = empty(0)
    for jj in range(0,size(ind_r)):
        y = max(data[jj,:(ind_r[jj]+1)]) - data[jj,ind_r[jj]]
        x = min(data[jj,:(ind_l[jj]+1)]) - data[jj,ind_l[jj]]
        if (absolute(y) >= absolute(x)) or (absolute(y) >= 2):
            dth = r_[dth,y]
        else:
            dth = r_[dth,x]
    return dth

def dom_evec(E, k):
    '''
    get dominant eigenvector

    Parameters
    ----------
    E : list
        contains eigenvalues at 0 and eigenvectors at 1.
    k : int
        which eigenvector to choose.

    Returns
    -------
    dom: array
        chosen eigenvector with adjusted sign.

    '''
    from numpy import real, sign

    dom = real(E[1][:,k])
    return sign(max(dom) + min(dom))*dom

def dlonlat_dxy(lon1, lon2, lat1, lat2):
    '''
    Converts lat/lon position of two points to distance in km based on
    the assumption that earth is a sphere

    Parameters
    ----------
    lon1 : numpy array
        longitude 1.
    lon2 : numpy array
        longitude 2.
    lat1 : numpy array
        latitude 1.
    lat2 : numpy array
        latitude 2.

    Returns
    -------
    dx : numpy array
        zonal distance in meters.
    dy : numpy array
        meridonal distance in meters.

    '''
    from numpy import cos, pi

    if (lon1-lon2) > 180:
        dx = -(360 - (lon1-lon2))*40000*cos((lat1+lat2)*pi/360)/360
    elif (lon1 - lon2) <-180:
        dx = -(360 + (lon1-lon2))*40000*cos((lat1+lat2)*pi/360)/360
    else:
        dx = -(lon1-lon2)*40000*cos((lat1+lat2)*pi/360)/360
    dy = -(lat1-lat2)*40000/360

    return (dx, dy)

def normalize(lon, lat, H):
    '''
    Normalize vertical scale of motion by computing the mean haversine and
    vertical distance of points btw. two timesteps and scaling the vertical
    height by the ratio.

    Parameters
    ----------
    lon : numpy array
        longitude.
    lat : numpy array
        latitude.
    H : numpy array
        height.

    Returns
    -------
    H_new : numpy array
        normalized height.

    '''
    from haversine import haversine_vector

    lon[lon>=180.] = lon[lon>=180.] - 360
    points_1 = np.array([lat[:,1:].flatten(), lon[:,1:].flatten()]).T
    points_0 = np.array([lat[:,:-1].flatten(), lon[:,:-1].flatten()]).T
    haveDist =  np.mean(haversine_vector(points_0, points_1))

    dH = np.mean(np.abs(H[:,1:] - H[:,:-1]))

    print("haveDistmean: {}, dHmean: {}, factor: {}".format(haveDist,
                                                            dH,
                                                            haveDist/dH))
    H_new = H * (haveDist/dH)
    return H_new


def coord_trans(lon, lat, p, v_trans, scaling, proj=None):
    '''
    Call coordinate transformation functions if specified.

    Parameters
    ----------
    lon : numpy array
        longitude.
    lat : numpy array
        latitude.
    p : numpy array
        pressure.
    v_trans : string
        vertical transformation flag.
    scaling : string
        flag for scaling coordinates to adjust for different scales of motion.
    proj : string
        projection specifier

    Returns
    -------
    X : numpy array
        1st cartesian coordinate.
    Y : numpy array
        2nd cartesian coordinate.
    Z : numpy array
        3rd cartesian coordinate.

    '''
    if v_trans == "std_atm":
        H = ptoh(p, 1013.25, 8.435) # standard atmosphere; in km
    elif v_trans == "None":
        H = p

    if proj is None:

        if scaling == 100:
            H = 100 * H
        elif scaling == 1000:
            H = H * 1000
        elif scaling == -1:
            H = normalize(lon, lat, H)
        elif scaling == 1:
            pass
        else:
            H = H * scaling

        X, Y, Z = lonlatrtoxyz(lon, lat, H + 6371)

    elif proj == "stereo":
        X, Y = project(lat, lon)
        if isinstance(X, tuple):
            X = tuple(x/1e3 for x in X)
            Y = tuple(y/1e3 for y in Y)
            Z = H
        else:
            X = X/1e3 #convert to km
            Y = Y/1e3
            Z = H * scaling

    return (X, Y, Z)

def adj_dist(x, y, k):
    '''
    Calculate adjusted distance between two points in the atmosphere

    Parameters
    ----------
    x : array
        lat, lon, h point 1.
    y : array
        lat, lon, h point 2.
    k : float
        scaling parameter for the vertical

    Returns
    -------
    d : float
        adjusted distance.

    '''
    from math import acos, sin, cos, sqrt, pi

    a = sin(x[0] * pi/180)*sin(y[0] * pi/180)
    b = cos(x[0] * pi/180)*cos(y[0] * pi/180)*cos(abs(x[1]-y[1]) * pi/180)
    if a + b > 1:
        # if points have same lat & lon
        return  k*abs(x[2]-y[2])
    else:
        return sqrt((6371*acos(a + b))**2 + (k*abs(x[2]-y[2]))**2)

def adj_dist_vec(x, y, k):
    '''
    Calculate adjusted distance between two points in the atmosphere. Vectors
    of coordinates are allowed.

    Parameters
    ----------
    x : array
        lat, lon, h point 1.
    y : array
        lat, lon, h point 2.
    k : float
        scaling parameter for the vertical

    Returns
    -------
    d : float
        adjusted distance.

    '''
    from numpy import arccos, sin, cos, sqrt, pi

    a = sin(x[0] * pi/180)*sin(y[0] * pi/180)
    b = cos(x[0] * pi/180)*cos(y[0] * pi/180)*cos(abs(x[1]-y[1]) * pi/180)
    c = a + b
    c[c>1] = 1
    return sqrt((6371*arccos(c))**2 + (k*abs(x[2]-y[2]))**2)

# @jit(nopython=True)
def distances(dataX, dataY, r, m, dataZ=None, sphere=True,
              customMetric=False, k=1):
    '''
    compute sparse distance matrix (stored as x, y, v
    where x, y and v are the vectors that form the sparse matrix)
    X is the index of the point from which the distance is measured and y the
    index to which the distance is measured
    If only x and y is supplied, it is assumed to be lat and lon and distances
    are calculated using haversine. If z is additionally supplied, cartesian
    space is assumed.

    Parameters
    ----------
    dataX : numpy array
        contains all the x values.
    dataY : numpy array
        contains all the y values.
    r : float
        maximum radius for nearest neighbour search.
    m : int
        number of points.
    T : int
        number of time slices.
    dataZ: numpy array, optional
        contains all the z values. The default is None
    sphere : Boolean, optional
        whether to use use distance on sphere metric for 2d.
        The default is True.
    customMetric : Boolean, optional
        Whether to use a custom metric. The default is False.
    k : float, optional
        scaling parameter for the vertical. The default is 1.

    Returns
    -------
    list
        contains three arrays with x, y, v
    where x, y and v are the vectors that form the sparse matrix.
    '''

    if customMetric:
        # Calculate horizontal distances, if horizontal distance > r, 3d
        # distance will be > r, too (duh!)
        dd = np.array([np.deg2rad(dataX), np.deg2rad(dataY)]).T
        BT = BallTree(dd, metric='haversine')
        idx, hdist = BT.query_radius(dd, r=r / 6371, return_distance=True)
        hdist = hdist * 6371
        # each element in idx/hdist corresponds to a point whose NN has
        # been queried
        x = list()
        y = list()
        v = list()

        for i in range(m):
            # Save only one triangle of symmetric matrix
            hdist[i] = hdist[i][idx[i] > i]
            idx[i] = idx[i][idx[i] > i]

            vdist = dataZ[idx[i]] - dataZ[i]

            dist = np.sqrt(np.power(hdist[i], 2) + np.power(k * vdist, 2))

            # Now with the custom distance
            valid = np.where(dist < r)[0]
            x.extend([i] * len(valid))
            y.extend(idx[i][valid])
            v.extend(dist[valid])

        x = np.asarray(x)
        y = np.asarray(y)
        v = np.asarray(v)

        # Legacy: way too slow
        # metric needs (lat, lon) not (lon, lat)
        # dd = array([dataY[:,j],dataX[:,j], dataZ[:,j]]).T
        # NN = NearestNeighbors(
        #     metric = lambda x, y: adj_dist(x, y, k),
        #     n_neighbors=20)
        # NN = NN.fit(dd)
        # dist, idx = NN.kneighbors(dd)
        # idx[dist>r] = dd.shape[0]
    else:
        if (dataZ is None) & (sphere == True):
            dd = np.array([np.deg2rad(dataX), np.deg2rad(dataY)]).T
            BT = BallTree(dd, metric='haversine')
            dist, idx = BT.query(dd, k=20)
            dist = dist * 6371 # distance in km
            idx[dist>r] = dd.shape[0]
        elif dataZ is None:
            dd = np.array([dataX,dataY]).T
            Tree = KDTree(dd)
            dist, idx = Tree.query(dd, k=20)
            idx[dist>r] = dd.shape[0]
        else:
            dd = np.array([dataX,dataY, dataZ]).T
            Tree = KDTree(dd)
            dist, idx = Tree.query(dd, k=20)
            idx[dist>r] = dd.shape[0]

        lv = sum(sum(idx != dd.shape[0]))

        # no. of neighours inside r
        x = np.zeros(lv)
        y = np.zeros(lv)
        v = np.zeros(lv)
        i_curr = 0
        for i in range(m):
            idx_there = idx[i] != dd.shape[0]
            li = sum(idx_there)
            x[i_curr:(i_curr+li)] = i
            y[i_curr:(i_curr+li)] = idx[i,idx_there]
            v[i_curr:(i_curr+li)] = dist[i,idx_there]
            i_curr = i_curr + li

    return list((np.int_(x), np.int_(y), v))

def calc_cs(X, Y, Z=None, r=2, epsilon=0.1, Tmin=0, Tmax=-1, N_k=2,
            sphere=True):
    '''
    Calculate coherent sets given the positions of traced air parcels. Based
    on Banisch/Koltai 2017

    Parameters
    ----------
    X : numpy array
        contains all the x values.
    Y : numpy array
        contains all the y values.
    Z : numpy array, default None
        contains all the z values. The default is None.
    r : float, optional
        cut-off radius. The default is 2.
    epsilon : float, optional
        diffusion bandwidth. The default is 0.1.
    Tmin : int, optional
        first timeslice to consider in calculation. The default is 0.
    Tmax : int, optional
        last timeslice to consider in calculation. The default is -1.
    N_k : int, optional
        Number of clusters. The default is 2.

    Returns
    -------
    E : list
         contains eigenvalues at 0 and eigenvectors at 1.
    kclust : fitted estimator
        contains .labels_ which labels the given points, cluster_centers_,
        which gives the cluster centers.

    '''

    m = X.shape[0]
    T = X.shape[1]
    if Tmax == -1:
        Tmax = T
    if Z is None:
        D = distances(X, Y, r, m, T, sphere=sphere)
    else:
        D = distances(X, Y, r, m, T, dataZ=Z)
    E = eigen(m , Tmin, Tmax, D[0], D[1], D[2], epsilon, N_k=N_k)

    # dom = dom_evec(E, k)
    kclust = kcluster_idx(E, N_k)

    return (E, kclust)

def opt_alpha(alpha_0, points, max_iter, max_no_change,
              check_watertight=False):
    '''
    Find the highes alpha shape parameter that leads to a hull
    that contains all points

    Parameters
    ----------
    alpha_0 : float
        initial alpha.
    points : list of tupel
        the points.
    max_iter : int
        maximum number of iterations.
    max_no_change : int
        maximum number of iterations showing no change in the best alpha.

    Returns
    -------
    best alpha : float
        the best alpha found.

    '''
    from alphashape import alphashape

    i_best_alpha = 999
    best_alpha = 0
    alpha = alpha_0
    i = 0
    while i < max_iter:
        ashp = alphashape(points, alpha)
        if check_watertight:
            if ashp.is_watertight:
                return alpha
            else:
                alpha = abs(np.random.default_rng().normal(
                    alpha_0, alpha_0 * (i+1)/max_iter, 1)[0])
                print("current alpha:", alpha)
        else:
            if ashp.faces.shape[0]==0: # alphashape is degenerate
                out_no_bound = 999
            else:
                bound_arr = ashp.vertices.__array__() # array of boundary coords
                bound = list(map(tuple, np.column_stack((bound_arr[:,0],
                                          bound_arr[:,1],
                                          bound_arr[:,2]))))
                boundary = np.asarray([p in bound for p in points])
                in_out = ashp.contains(points) # boolean whether points are inside
                out_no_bound = np.isin(np.where(in_out==False),
                                       np.where(boundary==False)).sum()
                # out_no_bound are the number of points that are neither inside,
                # nor on the boundary
            if out_no_bound > 0:
                alpha = alpha*np.sqrt(np.sqrt(0.1))
            else:
                if alpha > best_alpha:
                    best_alpha = alpha
                    i_best_alpha = i
                    print("current alpha:", best_alpha)
                alpha = alpha + alpha*np.sqrt(np.sqrt(0.1))
        if i - i_best_alpha > max_no_change:
            break
        i = i + 1
    return best_alpha

def get_bounds(X, Y, Z=None, alpha=None, convex=False, ini=False):
    '''
    Return a boolean array of size like X whether points of a given set
    of points are outside the hull points.

    Parameters
    ----------
    X : N-D numpy array
        X Coordinates (Lon).
    Y : N-D numpy array
        Y Coordinates (Lat).
    Z : N-D numpy array, optional
        Z Coordinates (p). The default is None.
    alpha : float or array, optional
        alphashapes parameter. The default is None.
    ini : boolean, optional
        Are the points in start configuration? The default is False.

    Returns
    -------
    boundary : numpy array
        [i] = True if the ith point is a boundary point.
    hull : Trimesh
        the hull
    '''
    from alphashape import alphashape
    from scipy.spatial import ConvexHull
    from warnings import warn
    from trimesh.base import Trimesh

    def get_ind(x, z):
        y = np.empty_like(x.flatten())
        for i, xx in enumerate(x.flatten()):
            y[i] = np.where(z == xx)[0]
        return y.reshape(x.shape)

    def bounds_once(X, Y, Z=None, alpha=None, convex=False):
        if Z is None:
            points = list(map(tuple, np.column_stack((X, Y))))
        else:
            points = list(map(tuple, np.column_stack((X, Y, Z))))
        if (not convex):
            if alpha is None:
                alpha = opt_alpha(0.01, points, 100, 10)
            print("alpha={:.3E}".format(alpha))
            alpha_shape = alphashape(points, alpha)


            if (not alpha_shape.is_watertight):
                print("Mesh is not watertight ") #+
            #          "finding similar alpha with watertight mesh")
            #     alpha = opt_alpha(alpha, points, 100, 10,
            #                       check_watertight=True)
            #     print("alpha={:.3E}".format(alpha))
            #     alpha_shape = alphashape(points, alpha)

            if hasattr(alpha_shape, "vertices"):
                bound_arr = alpha_shape.vertices.__array__()
                bound = list(map(tuple, np.column_stack((bound_arr[:,0],
                                                      bound_arr[:,1],
                                                      bound_arr[:,2]))))
                boundary = np.asarray([p in bound for p in points])
                in_out = alpha_shape.contains(points) # boolean whether points
                # are inside
                out = np.logical_or(boundary, np.invert(in_out))
                return out, alpha_shape
            elif hasattr(alpha_shape, "boundary"):
                # alpha shape is a polygon object
                if Z is None:
                    return alpha_shape.boundary.coords.xy
                else:
                    warn("alpha shape is a 2d polygon; points probably "+
                         "lie on 2d surface. Returning Convex Hull")
                    hull = ConvexHull(points)
                    Hull = Trimesh(hull.points[hull.vertices, :],
                                   get_ind(hull.simplices, hull.vertices))
                    return (np.isin(np.arange(len(points)), hull.vertices),
                            Hull)
        else:
            hull = ConvexHull(points)
            Hull = Trimesh(hull.points[hull.vertices, :],
                                   get_ind(hull.simplices, hull.vertices))
            return np.isin(np.arange(len(points)), hull.vertices), Hull

    if ini:
        # Legacy stuff: probably not working anymore
        # start position
        # X: lat, Y: lon, Z: p
        bounds = np.logical_or(Z==np.min(Z), Z==np.max(Z)) #top and bottom

        points = list(map(tuple, np.column_stack((Y[Z == np.min(Z)],
                                               X[Z == np.min(Z)]))))
        alpha_shape = alphashape(points, alpha)
        x_hor_bound, y_hor_bound = alpha_shape.exterior.coords.xy
        hor_bound = list(map(tuple, np.column_stack((x_hor_bound,
                                                     y_hor_bound))))
        horizontal_bound = [p in hor_bound for p in points]
        bounds = np.logical_or(bounds, horizontal_bound*len(np.unique(Z)))
        return bounds
    else:
        if len(X.shape) == 1:
            return bounds_once(X, Y, Z, alpha, convex=convex)
        else:
            bounds = np.empty_like(X)
            hulls = list()
            for t in range(X.shape[1]):
                if isinstance(alpha, np.ndarray):
                    alp = alpha[t]
                else:
                    alp = alpha
                if Z is None:
                    bounds[:,t], hull = bounds_once(X[:,t], Y[:,t], alp,
                                              convex=convex)
                    hulls.append(hull)
                else:
                    bounds[:,t], hull = bounds_once(X[:,t], Y[:,t], Z[:,t],
                                                    alp, convex=convex)
                    hulls.append(hull)
            return bounds, hulls

def nn_dist(points, k):
    '''
    Calculate the k nearest neighbor distances of points.

    Parameters
    ----------
    points : Trimesh or appropriate array
        the points.
    k : int
        number of nearest neighbors (1 returns only the points themselves).

    Returns
    -------
    dist : array
        distances.
    idx : array
        indices of NN.

    '''
    import trimesh
    from scipy.spatial import KDTree

    if isinstance(points, trimesh.base.Trimesh):
        pcd = trimesh.points.PointCloud(points.vertices)
        kdt = pcd.kdtree
        dist, idx = kdt.query(pcd, k)
    else:
        kdt = KDTree(points)
        dist, idx = kdt.query(pcd, k)
    return dist, idx

def est_dth_dist(th, hmin, dist_space):
    '''
    Gives estimates for the distribution of theta changes based on the given
    distribution space

    Parameters
    ----------
    th : numpy array
        containing th.
    hmin : int
        timesteps maximum.
    dist_space : numpy array
        distribution space.

    Returns
    -------
    f : numpy array
        distribution space.

    '''

    from scipy.stats import gaussian_kde

    dth = dth_minmax(th, hmin)
    kde = gaussian_kde(dth)
    f = kde(dist_space)

    return f

def calc_traj_diff(lon1, lat1, h1, lon2, lat2, h2):
    '''
    Calculate the distance between points at each time step

    Parameters
    ----------
    lon1 : array
        lon1.
    lat1 : array
        lat1.
    h1 : array
        height1.
    lon2 : array
        lon2.
    lat2 : array
        lat2.
    h2 : array
        h2.

    Returns
    -------
    D : array
        distances.

    '''
    from numpy import empty_like, vstack
    D = empty_like(lon1)

    for t in range(lon1.shape[1]):
        x = vstack((lat1[:,t], lon1[:,t], h1[:,t]))
        y = vstack((lat2[:,t], lon2[:,t], h2[:,t]))
        dist = adj_dist_vec(x, y, 1)
        D[:,t] = dist
    return D

