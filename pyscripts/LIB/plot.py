#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 13 10:23:29 2023

@author: schoelleh96
"""
import numpy as np
import pandas as pd
import seaborn as sb
from matplotlib import pyplot as plt
import cmcrameri.cm as cmc
from sys import path
path.append("../wp21/pyscripts/LIB")
import calc as cc
import data as dd

def plot_spectra(D, r, eps, X, Y, Z, k, ax, bounds, customMetric, d_path,
                 t_sel, z_coord_type=None, force_calc=False):
    '''
    Plots the EV spectra of each of the given epsilon to ax

    Parameters
    ----------
    r : array o floats
        cut-off radius.
    eps : array of floats
        diffusion bandwidth.
    X : array of floats
        x-coordinates.
    Y : array of floats
        y-coordinates.
    Z : array of floats
        z-coordinates.
    sc : float
        scaling parameter for the vertical.
    ax : Axes object
        the axes to plot to.
    bounds : boolean array
        indicates which points are boundary points.
    customMetric : boolean
        flag indicating whether to use a custom metric

    Returns
    -------
    None.

    '''
    from matplotlib.ticker import MaxNLocator, ScalarFormatter

    ax.cla()
    colors = plt.cm.viridis(np.linspace(0, 1, len(eps)))
    markers = ['o', 's', '^', 'v', '*']
    def format_scientific(value):
        """Custom function to format number in scientific notation without '+' sign."""
        return "${:.0e}$".format(value).replace('e+0', 'e').replace('e+', 'e')
    for i,e in enumerate(eps):
        print("epsilon = {}, r={}".format(e, r[i]))
        # if alpha is to change according to the epsilon used
        # if bound_type=="spnalp":
        #     bounds, hulls = dd.io_bounds(
        #                 d_path + "stereop", "$\\alpha$", xcs, ycs, zcs,
        #                 alpha)

        vals, vecs = dd.io_eigen((d_path  + str(t_sel[0]) +
                                 str(t_sel[-1])), X.shape[0], t_sel[0],
                                 t_sel[-1],
                                 D[0], D[1], D[2], e, bounds, 20, X, Y, Z,
                                 customMetric, k, z_coord_type,
                                 force_calc=force_calc)
        print(f"First eigenvalue = {vals[0]}")
        vals = (vals - 1)/e
        ax.scatter(x=np.arange(1, 11), y=np.real(vals)[:10],
                   label=format_scientific(e), zorder=5,
                   color=colors[i], marker=markers[i % len(markers)], s = 72)
        # Modify x-axis
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.set_xlabel("$i$")

    # Modify y-axis
    ax.set_ylabel("$\lambda_i$")
    ax.yaxis.set_major_formatter(ScalarFormatter(useMathText=True))
    ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

    # Add grid
    ax.grid(True, which='both', linestyle="-", linewidth=0.5, zorder = 1)

    # Modify legend to be more compact
    legend = ax.legend(loc="lower left", bbox_to_anchor=(0.01, -0.01),
                       reverse=True,
                       ncols=3, frameon=False,
                       title="$\epsilon$ $[\mathrm{km}^2]$", borderpad=0.5,
                       labelspacing=0.25,
                       handletextpad=0.5, borderaxespad=0, columnspacing=0.8,
                       handleheight=0.1, handlelength=0.1, shadow=False)
    legend.get_title().set_ha('left')
    legend._legend_box.align = "left"
    # Tight layout for making everything compact
    # plt.tight_layout()

def plotxyz3d(ax3d, X, Y, Z, azim, elev, dist, c=None):
    '''
    Do a simple 3d plot.

    Parameters
    ----------
    ax3d : axes object
        3d axes to plot to.
    X : array
        x coord.
    Y : array
        y coord.
    Z : array
        z coord.
    azim : float
        azimuth view angle.
    elev : float
        elevation view angle.
    dist : float
        distance.
    c : array or list of colors or color, optional
        marker colors (see plt.scatter)

    Returns
    -------
    None.

    '''
    from numpy import min, max
    from warnings import catch_warnings, simplefilter

    ax3d.view_init(azim=azim, elev=elev)
    with catch_warnings():
        simplefilter("ignore")
        ax3d.dist=dist
    ax3d.set_xlim((min(X), max(X)))
    ax3d.set_ylim((min(Y), max(Y)))
    ax3d.set_zlim((min(Z), max(Z)))

    ax3d.scatter(X,Y,Z,c=c)

def make_segments(x, y):
    '''
    Create list of line segments from x and y coordinates, in the correct
    format for LineCollection:
    an array of the form   numlines x (points per line) x 2 (x and y) array

    Parameters
    ----------
    x : float
        coordinate.
    y : float
        coordinate.

    Returns
    -------
    segments : line segment
        for LineCollection.

    '''
    from numpy import array, concatenate

    points = array([x, y]).T.reshape(-1, 1, 2)
    segments = concatenate([points[:-1], points[1:]], axis=1)

    return segments

def colorline(x, y, z=None, cmap=None, norm=None, linewidth=3,
              alpha=1.0, ax=None):
    '''
    Plot a colored line with coordinates x and y
    Optionally specify colors in the array z
    Optionally specify a colormap, a norm function and a line width
    Parameters
    ----------
    x : float
        coordinate.
    y : float
        coordinate.
    z : array, optional
        specify colorspacing. The default is None.
    cmap : colorbar, optional
        colorbar. The default is None.
    norm : normalize.colors, optional
        normalize.colors. The default is None.
    linewidth : float, optional
        linewidth. The default is 3.
    alpha : float, optional
        alpha. The default is 1.0.

    Returns
    -------
    lc : lineCollection
        Collection of segments.

    '''

    from matplotlib.pyplot import get_cmap, Normalize, gca
    from numpy import linspace, array, asarray
    from matplotlib.collections import LineCollection
    from cartopy.crs import PlateCarree

    if cmap is None:
        cmap=get_cmap('copper')

    if norm is None:
        norm=Normalize(0.0, 1.0)

    # Default colors equally spaced on [0,1]:
    if z is None:
        z = linspace(0.0, 1.0, len(x))

    # Special case if a single number:
    if not hasattr(z, "__iter__"):  # to check for numerical input (hack)
        z = array([z])

    z = asarray(z)

    segments = make_segments(x, y)
    lc = LineCollection(segments, array=z, cmap=cmap, norm=norm,
                        linewidth=linewidth, alpha=alpha,
                        transform=PlateCarree())

    if ax is None:
        ax = gca()

    ax.add_collection(lc)

    return lc

def scatter_hist(x, y, ax, ax_histx, ax_histy):
    '''
    Plots a scatterplot with adjacent histograms.

    Parameters
    ----------
    x : 1-D numpy array
        x-data.
    y : 1-D numpy array
        y-data.
    ax : ax object
        scatter plot ax.
    ax_histx : ax object
        histogram plot ax for x.
    ax_histy : ax object
        histogram object for y.

    Returns
    -------
    None.

    '''
    from numpy import max, abs, linspace
    # no labels
    ax_histx.tick_params(axis="x", labelbottom=False)
    ax_histy.tick_params(axis="y", labelleft=False)

    # the scatter plot:
    ax.scatter(x, y)

    max_x = max(abs(x))
    max_y = max(abs(y))
    bins_x = linspace(-max_x, max_x, 100)
    bins_y = linspace(-max_y, max_y, 100)
    ax_histx.hist(x, bins=bins_x)
    ax_histy.hist(y, bins=bins_y, orientation='horizontal')

def plot_scales(h, X=None, Y=None):
    '''
    Plot the scales of change with respect to either X or Y and h.

    Parameters
    ----------
    X : 2D numpy array
        containing X information.
    Y : 2D numpy array
        containing Y information.
    h : 2D numpy array
        containing h information.

    Returns
    -------
    fig : pyplot figure object
         containing the plot.

    '''
    from matplotlib.pyplot import figure

    hdiff = (h[:,4:] - h[:,:-4]).flatten()
    if X is None:
        Ydiff = (Y[:,4:] - Y[:,:-4]).flatten()
    elif Y is None:
        Xdiff = (X[:,4:] - X[:,:-4]).flatten()
        # parcels crossing the dateline distort
        hdiff = hdiff[np.abs(Xdiff)<20000]
        Xdiff = Xdiff[np.abs(Xdiff)<20000]

    # Start with a square Figure.
    fig = figure(figsize=(6, 6))
    # Add a gridspec with two rows and two columns and a ratio of 1 to 4
    # between the size of the marginal axes and the main axes in both
    # directions. Also adjust the subplot parameters for a square plot.
    gs = fig.add_gridspec(2, 2,  width_ratios=(4, 1), height_ratios=(1, 4),
                          left=0.1, right=0.9, bottom=0.1, top=0.9,
                          wspace=0.05, hspace=0.05)
    # Create the Axes.
    ax = fig.add_subplot(gs[1, 0])
    ax_histx = fig.add_subplot(gs[0, 0], sharex=ax)
    ax_histy = fig.add_subplot(gs[1, 1], sharey=ax)
    # Draw the scatter plot and marginals.
    if X is None:
        scatter_hist(Ydiff.flatten(), hdiff.flatten(), ax, ax_histx,
                     ax_histy)
        ax.set_xlabel("$\Delta$ Y in km")
        ax.set_ylabel("$\Delta$ Z in km")
    elif Y is None:
        scatter_hist(Xdiff.flatten(), hdiff.flatten(), ax, ax_histx,
                     ax_histy)
        ax.set_xlabel("$\Delta$ X in km")
        ax.set_ylabel("$\Delta$ Z in km")

    return fig

def plot_set_comb(trajs, cmap, norm, date, dth=None, kclustL=None):
    '''
    Plot a collection of plots: green for cooled/cluster1, purple for
    heated/cluster2, showing parcel locations at individual timesteps.

    Parameters
    ----------
    trajs : trajectories
        from lagranto.
    cmap : Colormap
        matplotlib.colors.
    norm : Normalize
        matplotlib.colors.
    dth : array
        contains max abolute change in theta
    date : date type
        startf date.
    kclustL : array
        contains assignments of points to clusters

    Returns
    -------
    None.

    '''
    from matplotlib.pyplot import (subplots, savefig, close, legend)
    from cartopy.crs import NorthPolarStereo, PlateCarree
    from numpy import vstack, nonzero, abs, diff, split

    # plot trajectories
    n_tra = len(trajs['lon'])
    n_t = len(trajs['lon'][0])

    # loop through timesteps
    for t in range(-(n_t - 1), 1):

        fig, ax = subplots(1, 1, figsize=(9, 9),
                   subplot_kw={'projection': NorthPolarStereo()})

        ax.coastlines()
        ax.set_extent([-180, 180, 30, 90], crs=PlateCarree())

        ax.scatter(trajs['lon'][:,0], trajs['lat'][:,0],
                   color='black', s=7, zorder=5,
                   transform=PlateCarree(), alpha=0.4)
        if kclustL is None:
            ax.scatter(trajs['lon'][:,(-t)][dth>2], trajs['lat'][:,(-t)][dth>2],
                       color='purple', s=7, zorder=6, label="heated",
                       transform=PlateCarree())
            ax.scatter(trajs['lon'][:,(-t)][dth<2], trajs['lat'][:,(-t)][dth<2],
                       color='green', s=7, zorder=6, label="cooled",
                       transform=PlateCarree())
        else:
            ax.scatter(trajs['lon'][:,(-t)][kclustL == 0],
                       trajs['lat'][:,(-t)][kclustL == 0],
                       color='purple', s=7, zorder=6, label="cluster 1",
                       transform=PlateCarree())
            ax.scatter(trajs['lon'][:,(-t)][kclustL == 1],
                       trajs['lat'][:,(-t)][kclustL == 1],
                       color='green', s=7, zorder=6, label="cluster 2",
                       transform=PlateCarree())

        # loop through trajectories
        for i in range(0,n_tra,1): # plot only every nth trajectory

            # cosmetic: lines that cross the 180° lon create ugly artefacts

            segment = vstack((trajs['lon'][i], trajs['lat'][i]))
            lon0 = 180 #center of map
            bleft = lon0-181.
            bright = lon0+181.
            segment[0,segment[0]> bright] -= 360.
            segment[0,segment[0]< bleft]  += 360.
            threshold = 180.  # CHANGE HERE
            isplit = nonzero(abs(diff(segment[0])) > threshold)[0]
            subsegs = split(segment,isplit+1,axis=+1)

            #plot the tracks
            for seg in subsegs:
                x,y = seg[0],seg[1]

                h4 = colorline(x, y, trajs['p'][i], norm=norm,
                               linewidth=0.8, cmap=cmap, alpha=0.2, ax=ax)

        if kclustL is None:
            fraction_lh = sum(x >= 0 for x in dth)*100/len(dth)
            ax.title.set_text("Heated Parcels (" + "{:.2f}".format(fraction_lh)
                              + "%)")
        legend(loc="lower left")
        #add colorbar
        cbar = fig.colorbar(h4, ax=ax, orientation='horizontal',
                            fraction=0.1, pad=0.05)

        cbar.set_label('$p$ [hPa]',size=9)
        cbar.ax.tick_params(labelsize=9)

        if kclustL is None:
            savefig("im/setsc/" + date.strftime('setc_%Y%m%d_%H') +
                    "{:04d}.png".format((t+n_t)))
        else:
            savefig("im/setsc/" + date.strftime('setcs_%Y%m%d_%H') +
                    "{:04d}.png".format((t+n_t)))
        close()

def plot_set_sep(trajs, cmap, norm, dth, date):
    '''
    Plot a collection of double subplots: left for heated, right for
    non-heated, showing parcel locations at individual timesteps.

    Parameters
    ----------
    trajs : trajectories
        from lagranto.
    cmap : Colormap
        matplotlib.colors.
    norm : Normalize
        matplotlib.colors.
    dth : array
        contains max abolute change in theta
    date : date type
        startf date.

    Returns
    -------
    None.

    '''
    #  imports
    from matplotlib.pyplot import (subplots, savefig, sca, close)
    from cartopy.crs import NorthPolarStereo, PlateCarree
    from numpy import vstack, nonzero, abs, diff, split

    # main

    # plot trajectories
    n_tra = len(trajs['lon'])
    n_t = len(trajs['lon'][0])

    # loop through timesteps
    for t in range(-(n_t - 1), 1):

        fig, ax = subplots(1, 2, figsize=(18, 9),
                   subplot_kw={'projection': NorthPolarStereo()})

        ax[0].coastlines()
        ax[0].set_extent([-180, 180, 30, 90], crs=PlateCarree())

        ax[1].coastlines()
        ax[1].set_extent([-180, 180, 30, 90], crs=PlateCarree())


        ax[0].scatter(trajs['lon'][:,0], trajs['lat'][:,0],
                      color='black', s=7, zorder=5,
                      transform=PlateCarree(), alpha=0.4)
        ax[1].scatter(trajs['lon'][:,0], trajs['lat'][:,0],
                      color='black', s=7, zorder=5,
                      transform=PlateCarree(), alpha=0.4)
        ax[0].scatter(trajs['lon'][:,(-t)][dth>2], trajs['lat'][:,(-t)][dth>2],
                   color='purple', s=7, zorder=6,
                   transform=PlateCarree())
        ax[1].scatter(trajs['lon'][:,(-t)][dth<2], trajs['lat'][:,(-t)][dth<2],
                   color='green', s=7, zorder=6,
                   transform=PlateCarree())

        # loop through trajectories
        for i in range(0,n_tra,1): # plot only every nth trajectory

            # cosmetic: lines that cross the 180° lon create ugly artefacts

            segment = vstack((trajs['lon'][i], trajs['lat'][i]))
            lon0 = 180 #center of map
            bleft = lon0-181.
            bright = lon0+181.
            segment[0,segment[0]> bright] -= 360.
            segment[0,segment[0]< bleft]  += 360.
            threshold = 180.  # CHANGE HERE
            isplit = nonzero(abs(diff(segment[0])) > threshold)[0]
            subsegs = split(segment,isplit+1,axis=+1)

            if dth[i] >= 2:
                sca(ax[0])
            else:
                sca(ax[1])

            #plot the tracks
            for seg in subsegs:
                x,y = seg[0],seg[1]
                h4 = colorline(x, y, trajs['p'][i], norm=norm,
                               linewidth=0.8, cmap=cmap, alpha=0.2)

        fraction_lh = sum(x >= 0 for x in dth)*100/len(dth)
        ax[0].title.set_text("Heated Parcels (" + "{:.2f}".format(fraction_lh)
                             + "%)")
        ax[1].title.set_text("Cooled Parcels (" +
                             "{:.2f}".format(100 - fraction_lh) + "%)")

        #add colorbar
        cbar = fig.colorbar(h4, ax=ax.ravel().tolist(),
                            orientation='horizontal', fraction=0.1, pad=0.05)

        cbar.set_label('$p$ [hPa]',size=9)
        cbar.ax.tick_params(labelsize=9)

        savefig("im/sets/" + date.strftime('set_%Y%m%d_%H') +
                "{:04d}.png".format((t+n_t)))

        close()

def plot_set_sep4(trajs, cmap, norm, dth, date, kclustL):
    '''
    Plot a collection of four subplots: left for heated, right for
    non-heated, top/bottom for coherent sets showing parcel locations at
    individual timesteps.

    Parameters
    ----------
    trajs : trajectories
        from lagranto.
    cmap : Colormap
        matplotlib.colors.
    norm : Normalize
        matplotlib.colors.
    dth : array
        contains max abolute change in theta
    date : date type
        startf date.
    kclustL : array
        contains assignments of points to clusters

    Returns
    -------
    None.

    '''
    from matplotlib.pyplot import (subplots, savefig, sca, close,
                                   tight_layout)
    from cartopy.crs import NorthPolarStereo, PlateCarree
    from numpy import vstack, nonzero, abs, diff, split

    # plot trajectories
    n_tra = len(trajs['lon'])
    n_t = len(trajs['lon'][0])

    # loop through timesteps
    for t in range(-(n_t - 1), 1):

        fig, ax = subplots(2, 2, figsize=(18, 16),
                   subplot_kw={'projection': NorthPolarStereo()})

        for j in range(4):
            ax.flatten()[j].coastlines()
            ax.flatten()[j].set_extent([-180, 180, 30, 90], crs=PlateCarree())
            ax.flatten()[j].scatter(trajs['lon'][:,0], trajs['lat'][:,0],
                                    color='black', s=7, zorder=5,
                                    transform=PlateCarree(), alpha=0.4)

            ax.flatten()[j].scatter(trajs['lon'][:,0], trajs['lat'][:,0],
                                    color='black', s=7, zorder=5,
                                    transform=PlateCarree(), alpha=0.4)

        ax.flatten()[0].scatter(trajs['lon'][:,(-t)][(dth>2) & (kclustL == 0)],
                      trajs['lat'][:,(-t)][(dth>2) & (kclustL == 0)],
                      color='purple', s=7, zorder=6,
                      transform=PlateCarree())
        ax.flatten()[1].scatter(trajs['lon'][:,(-t)][(dth<2) & (kclustL == 0)],
                      trajs['lat'][:,(-t)][(dth<2) & (kclustL == 0)],
                      color='green', s=7, zorder=6,
                      transform=PlateCarree())
        ax.flatten()[2].scatter(trajs['lon'][:,(-t)][(dth>2) & (kclustL == 1)],
                      trajs['lat'][:,(-t)][(dth>2) & (kclustL == 1)],
                      color='purple', s=7, zorder=6,
                      transform=PlateCarree())
        ax.flatten()[3].scatter(trajs['lon'][:,(-t)][(dth<2) & (kclustL == 1)],
                      trajs['lat'][:,(-t)][(dth<2) & (kclustL == 1)],
                      color='green', s=7, zorder=6,
                      transform=PlateCarree())
        # loop through trajectories
        for i in range(0,n_tra,1): # plot only every nth trajectory

            # cosmetic: lines that cross the 180° lon create ugly artefacts

            segment = vstack((trajs['lon'][i], trajs['lat'][i]))
            lon0 = 180 #center of map
            bleft = lon0-181.
            bright = lon0+181.
            segment[0,segment[0]> bright] -= 360.
            segment[0,segment[0]< bleft]  += 360.
            threshold = 180.  # CHANGE HERE
            isplit = nonzero(abs(diff(segment[0])) > threshold)[0]
            subsegs = split(segment,isplit+1,axis=+1)

            if dth[i] >= 2:
                if kclustL[i] == 0:
                    sca(ax.flatten()[0])
                else :
                    sca(ax.flatten()[1])
            else:
                if kclustL[i] == 1:
                    sca(ax.flatten()[2])
                else :
                    sca(ax.flatten()[3])

            #plot the tracks
            for seg in subsegs:
                x,y = seg[0],seg[1]
                h4 = colorline(x, y, trajs['p'][i], norm=norm,
                               linewidth=0.8, cmap=cmap, alpha=0.2)

        fraction_lh = sum(x >= 0 for x in dth)*100/len(dth)
        ax.flatten()[0].title.set_text(
            "Heated Parcels (" + "{:.2f}".format(fraction_lh)
                             + "%)")
        ax.flatten()[1].title.set_text("Cooled Parcels (" +
                             "{:.2f}".format(100 - fraction_lh) + "%)")

        rows = ["Coherent Set {}".format(row) for row in ["1","2"]]

        for axes, row in zip(ax[:,0], rows):
            axes.annotate(row, xy=(0, 0.5), xytext=(-0.1 , 0.5),
                          xycoords="axes fraction", rotation="vertical",
                          size='large', ha='right', va='center')

        tight_layout()
        #add colorbar
        cbar = fig.colorbar(h4, ax=ax.ravel().tolist(),
                            orientation='horizontal', fraction=0.1, pad=0.05)

        cbar.set_label('$p$ [hPa]',size=9)
        cbar.ax.tick_params(labelsize=9)

        savefig("im/sets4/" + date.strftime('set4_%Y%m%d_%H') +
                "{:04d}.png".format((t+n_t)))

        close()

def plot_traj(trajs, cmap, norm):
    '''
    Plot one subplot with trajectories

    Parameters
    ----------
    trajs : trajectories
        from lagranto.
    cmap : Colormap
        matplotlib.colors.
    norm : Normalize
        matplotlib.colors.

    Returns
    -------
    fig : figure
        from subplot.
    ax : axes
        from subplot.

    '''
    from matplotlib.pyplot import subplots, tight_layout
    from cartopy.crs import Stereographic, PlateCarree
    from numpy import vstack, nonzero, abs, diff, split

    fig, ax = subplots(1, 1, figsize=(3.25, 2),
                       subplot_kw={'projection': Stereographic(
                            central_latitude=90.0, true_scale_latitude=50.0,
                            central_longitude=-120)})
                           # central_latitude=90.0, true_scale_latitude=50.0,
                           # central_longitude=30)})

    ax.coastlines()
    ax.gridlines(linestyle='--', alpha=0.5)
    ax.set_extent([-210, -30, 30, 90], crs=PlateCarree())
    # ax.set_extent([-60, 120, 30, 90], crs=PlateCarree())

    # plot trajectories
    Lon = trajs['lon']
    Lat = trajs['lat']
    p = trajs['p']
    n_tra = Lon.shape[0]
    every_n = 50
    # loop through trajectories
    for i in range(0,n_tra,every_n): # plot only every nth trajectory

        # cosmetic: lines that cross the 180° longitude create ugly artefacts

        segment = vstack((trajs['lon'][i], trajs['lat'][i]))
        lon0 = 180 #center of map
        bleft = lon0-181.
        bright = lon0+181.
        segment[0,segment[0]> bright] -= 360.
        segment[0,segment[0]< bleft]  += 360.
        threshold = 180.  # CHANGE HERE
        isplit = nonzero(abs(diff(segment[0])) > threshold)[0]
        subsegs = split(segment,isplit+1,axis=+1)

        #plot the tracks
        for seg in subsegs:
            x,y = seg[0],seg[1]

            h4 = colorline(x, y, trajs['p'][i], norm=norm,
                           linewidth=0.4, cmap=cmap)

    ax.scatter(Lon[::every_n,[0, 72, 144]], Lat[::every_n,[0, 72, 144]],
               color='black',
               s=.4, zorder=5, transform=PlateCarree())

    # add colorbar
    cbar = fig.colorbar(h4, ax=ax, orientation='horizontal',
                        fraction=0.1, pad=0.05)

    cbar.set_label('$p$ [hPa]')#,size=14)
    # cbar.ax.tick_params(labelsize=14)
    tight_layout()

    return fig, ax


def plot_traj_sep(trajs, cmap, norm, dth):
    '''
    Plot two subplots: left for heated trajectories and right for not-heated

    Parameters
    ----------
    trajs : trajectories
        from lagranto.
    cmap : Colormap
        matplotlib.colors.
    norm : Normalize
        matplotlib.colors.
    dth : array
        contains max abolute change in theta

    Returns
    -------
    fig : figure
        from subplot.
    ax : axes
        from subplot.

    '''
    from matplotlib.pyplot import subplots, sca
    from cartopy.crs import NorthPolarStereo, PlateCarree
    from numpy import vstack, nonzero, abs, diff, split

    fig, ax = subplots(1, 2, figsize=(18, 9),
                       subplot_kw={'projection': NorthPolarStereo()})

    ax[0].coastlines()
    ax[0].set_extent([-180, 180, 20, 90], crs=PlateCarree())

    ax[1].coastlines()
    ax[1].set_extent([-180, 180, 20, 90], crs=PlateCarree())

    # plot trajectories
    n_tra = len(trajs['lon'])
    # loop through trajectories
    for i in range(0,n_tra,1): # plot only every nth trajectory

        # cosmetic: lines that cross the 180° longitude create ugly artefacts

        segment = vstack((trajs['lon'][i], trajs['lat'][i]))
        lon0 = 180 #center of map
        bleft = lon0-181.
        bright = lon0+181.
        segment[0,segment[0]> bright] -= 360.
        segment[0,segment[0]< bleft]  += 360.
        threshold = 180.  # CHANGE HERE
        isplit = nonzero(abs(diff(segment[0])) > threshold)[0]
        subsegs = split(segment,isplit+1,axis=+1)

        if dth[i] >= 2:
            sca(ax[0])
        else:
            sca(ax[1])

        #plot the tracks
        for seg in subsegs:
            x,y = seg[0],seg[1]

            h4 = colorline(x, y, trajs['p'][i], norm=norm,
                           linewidth=0.8, cmap=cmap)

    ax[0].scatter(trajs['lon'][:,0], trajs['lat'][:,0], color='black', s=7,
               zorder=5, transform=PlateCarree())

    ax[1].scatter(trajs['lon'][:,0], trajs['lat'][:,0], color='black', s=7,
               zorder=5, transform=PlateCarree())

    fraction_lh = sum(x >= 0 for x in dth)*100/len(dth)
    ax[0].title.set_text("Heated Parcels (" + "{:.2f}".format(fraction_lh)
                         + ")")
    ax[1].title.set_text("Cooled Parcels (" +
                         "{:.2f}".format(100 - fraction_lh) + ")")

    # tight_layout()
    #add colorbar
    cbar = fig.colorbar(h4, ax=ax.ravel().tolist(), orientation='horizontal',
                        fraction=0.1, pad=0.05)

    cbar.set_label('$p$ [hPa]',size=9)
    cbar.ax.tick_params(labelsize=9)

    return fig, ax

def plot_traj_sep4(trajs, cmap, norm, dth, kclustL):
    '''
    Plot four subplots: left for heated trajectories, right for not-heated
    top for cluster 1 and bottom for cluster 2

    Parameters
    ----------
    trajs : trajectories
        from lagranto.
    cmap : Colormap
        matplotlib.colors.
    norm : Normalize
        matplotlib.colors.
    dth : array
        contains max abolute change in theta
    kclustL : array
        contains assignments of points to clusters

    Returns
    -------
    fig : figure
        from subplots.
    ax : axes
        from subplots.

    '''

    from matplotlib.pyplot import subplots, sca, tight_layout
    from cartopy.crs import NorthPolarStereo, PlateCarree
    from numpy import vstack, nonzero, abs, diff, split

    fig, ax = subplots(2, 2, figsize=(18, 16),
                           subplot_kw={'projection': NorthPolarStereo()})

    for j in range(4):
        ax.flatten()[j].coastlines()
        ax.flatten()[j].set_extent([-180, 180, 30, 90], crs=PlateCarree())

    # plot trajectories
    n_tra = len(trajs['lon'])
    # loop through trajectories
    for i in range(0,n_tra,1): # plot only every nth trajectory

        # cosmetic: lines that cross the 180° longitude create ugly artefacts

        segment = vstack((trajs['lon'][i], trajs['lat'][i]))
        lon0 = 180 #center of map
        bleft = lon0-181.
        bright = lon0+181.
        segment[0,segment[0]> bright] -= 360.
        segment[0,segment[0]< bleft]  += 360.
        threshold = 180.  # CHANGE HERE
        isplit = nonzero(abs(diff(segment[0])) > threshold)[0]
        subsegs = split(segment,isplit+1,axis=+1)

        if dth[i] >= 2:
            if kclustL[i] == 0:
                sca(ax.flatten()[0])
            else :
                sca(ax.flatten()[2])
        else:
            if kclustL[i] == 0:
                sca(ax.flatten()[1])
            else :
                sca(ax.flatten()[3])

        #plot the tracks
        for seg in subsegs:
            x,y = seg[0],seg[1]

            h4 = colorline(x, y, trajs['p'][i], norm=norm,
                           linewidth=0.8, cmap=cmap)

    for j in range(4):
        ax.flatten()[j].scatter(trajs['lon'][:,0], trajs['lat'][:,0],
                                color='black', s=7, zorder=5,
                                transform=PlateCarree())

        ax.flatten()[j].scatter(trajs['lon'][:,0], trajs['lat'][:,0],
                                color='black', s=7, zorder=5,
                                transform=PlateCarree())

    fraction_lh = sum(x >= 0 for x in dth)*100/len(dth)
    ax.flatten()[0].set_title("Heated Parcels (" +
                              "{:.2f}".format(fraction_lh) + " %)",
                              size="large")
    ax.flatten()[1].set_title("Cooled Parcels (" +
                              "{:.2f}".format(100 - fraction_lh) + " %)",
                              size="large")


    rows = ["Coherent Set {}".format(row) for row in ["1","2"]]

    for axes, row in zip(ax[:,0], rows):
        axes.annotate(row, xy=(0, 0.5), xytext=(-0.1 , 0.5),
                      xycoords="axes fraction", rotation="vertical",
                      size='large', ha='right', va='center')

    tight_layout()
    #add colorbar
    cbar = fig.colorbar(h4, ax=ax.ravel().tolist(), orientation='horizontal',
                        fraction=0.1, pad=0.05)

    cbar.set_label('$p$ [hPa]',size=9)
    cbar.ax.tick_params(labelsize=9)

    return fig, ax

def plot_set_comb3d(trajs, cmap, norm, date, dth=None, kclustL=None):
    '''
    Plot a collection of plots: x for cooled/cluster1, o for heated/cluster2,
    showing parcel locations at individual timesteps.

    Parameters
    ----------
    trajs : trajectories
        from lagranto.
    cmap : Colormap
        matplotlib.colors.
    norm : Normalize
        matplotlib.colors.
    dth : array
        contains max abolute change in theta
    date : date type
        startf date.

    Returns
    -------
    None.

    '''
    from matplotlib.pyplot import subplots, savefig, close, legend
    from cartopy.crs import NorthPolarStereo, PlateCarree
    import matplotlib.cm as cm

    # plot trajectories
    n_t = len(trajs['lon'][0])

    # loop through timesteps
    for t in range(-(n_t - 1), 1):

        fig, ax = subplots(1, 1, figsize=(12, 12),
                   subplot_kw={'projection': NorthPolarStereo()})

        ax.coastlines()
        ax.set_extent([-180, 180, 30, 90], crs=PlateCarree())

        if kclustL is None:
            ax.scatter(trajs['lon'][:,0][dth>2], trajs['lat'][:,0][dth>2],
                       facecolor='none', s=20, zorder=5, edgecolor=(0,0,0,.2),
                       marker='o',transform=PlateCarree())
            ax.scatter(trajs['lon'][:,0][dth<2], trajs['lat'][:,0][dth<2],
                       color='black', s=20, zorder=5, marker='x',
                       transform=PlateCarree(), alpha=0.2)
            mapper = cm.ScalarMappable(norm=norm, cmap=cmap)
            ax.scatter(trajs['lon'][:,(-t)][dth>2],
                       trajs['lat'][:,(-t)][dth>2], marker='o',
                       facecolor='none',
                       edgecolors=mapper.to_rgba(trajs['p'][:,(-t)][dth>2]),
                       s=20, zorder=6, label="heated", transform=PlateCarree())
            ax.scatter(trajs['lon'][:,(-t)][dth<2],
                       trajs['lat'][:,(-t)][dth<2], marker='x',
                       c=trajs['p'][:,(-t)][dth<2], cmap=cmap,
                       norm=norm, s=20,
                       zorder=6, label="cooled", transform=PlateCarree())
        else:
            ax.scatter(trajs['lon'][:,0][kclustL == 0],
                       trajs['lat'][:,0][kclustL == 0],
                       facecolor='none', s=20, zorder=5, edgecolor=(0,0,0,.2),
                       marker='o', transform=PlateCarree())
            ax.scatter(trajs['lon'][:,0][kclustL == 1],
                       trajs['lat'][:,0][kclustL == 1],
                       color='black', s=20, zorder=5, marker='x',
                       transform=PlateCarree(), alpha=0.2)
            mapper = cm.ScalarMappable(norm=norm, cmap=cmap)
            ax.scatter(trajs['lon'][:,(-t)][kclustL == 0],
                       trajs['lat'][:,(-t)][kclustL == 0], marker='o',
                       facecolor='none',
                       edgecolors=mapper.to_rgba(
                           trajs['p'][:,(-t)][kclustL == 0]), s=20,
                       zorder=6, label="cluster 1", transform=PlateCarree())
            ax.scatter(trajs['lon'][:,(-t)][kclustL == 1],
                       trajs['lat'][:,(-t)][kclustL == 1], marker='x',
                       c=trajs['p'][:,(-t)][kclustL == 1], cmap=cmap,
                       norm=norm, s=20,
                       zorder=6, label="cluster 2", transform=PlateCarree())

        if kclustL is None:
            fraction_lh = sum(x >= 0 for x in dth)*100/len(dth)
            ax.title.set_text("Heated Parcels (" + "{:.2f}".format(fraction_lh)
                              + "%)")
        legend(loc="lower left")
        #add colorbar
        cbar = fig.colorbar(mapper, ax=ax, orientation='horizontal',
                            fraction=0.1, pad=0.05)

        cbar.set_label('$p$ [hPa]',size=9)
        cbar.ax.tick_params(labelsize=9)

        if kclustL is None:
            savefig("im/setsc3d/" + date.strftime('setc_%Y%m%d_%H') +
                    "{:04d}.png".format((t+n_t)))
        else:
            savefig("im/setsc3d/" + date.strftime('setcs_%Y%m%d_%H') +
                    "{:04d}.png".format((t+n_t)))
        close()

def plot_wrapper(trajs, dth, date_0, plotCase, kclustL=None):
    '''
    Call the respective plot function based on the flag "plotCase"

    Parameters
    ----------
    trajs : trajectories
        from lagranto.
    dth : array
        contains max abolute change in theta
    date_0 : date type
        startf date.
    plotCase : String
        Decision Flag.
    kclustL :  array, optional
        contains assignments of points to clusters. The default is None.

    Returns
    -------
    None.

    '''
    from numpy import array
    from matplotlib.colors import from_levels_and_colors
    from matplotlib.pyplot import savefig
    from matplotlib.colors import Normalize

    # colormap for trajectories

    colors = [#more detailed 18 colors
        [130,0,0], #rot
        [160,0,0],
        [190,0,0],
        [220,30,0],
        [250,60,0],
        [250,90,0],
        [250,120,0],   #orange
        [250,170,30],   #yellow
        [250,200,90],
        [250,220,150], #MITTE
        [0,220,250],   #cyan
        [0,90,250],  #blue
        [0,60,250],
        [0,60,220],
        [0,30,190],
        [0,30,160],
        [0,30,130],
    ]
    levels =  [200,250,300,350,400,450,500,550,600,650,700,750,800,850,900,950]
    # 17 levels
    # convert RGB values to range between 0 - 1
    colors = array(colors)/255

    colors = cmc.glasgow_r(np.linspace(0, 1, len(levels)+1))
    # creat colormap
    cmap, norm = from_levels_and_colors(levels, colors, extend='both')

    if plotCase == "traj_sep":
        fig, ax = plot_traj_sep(trajs, cmap, norm, dth)
        savefig("im/split2/" + date_0.strftime('traj_%Y%m%d_%H') + ".png")
    elif plotCase == "split4":
        fig, ax = plot_traj_sep4(trajs, cmap, norm, dth, kclustL)
        savefig("im/split4/" + date_0.strftime('traj4_%Y%m%d_%H') + ".png")
    elif plotCase == "set_sep":
        plot_set_sep(trajs, cmap, norm, dth, date_0)
    elif plotCase == "set_sep4":
        plot_set_sep4(trajs, cmap, norm, dth, date_0, kclustL)
    elif plotCase == "set_comb":
        plot_set_comb(trajs, cmap, norm, date_0, dth=dth, kclustL=kclustL)
    elif plotCase == "set_comb3d":
        plot_set_comb3d(trajs, cmap, norm, date_0, dth=dth, kclustL=kclustL)
    elif plotCase == "traj":
        fig, ax = plot_traj(trajs, cmap, norm)
        savefig("../im/trajs/" + date_0.strftime('traj_%Y%m%d_%H') + ".pdf",
                bbox_inches="tight", dpi=300)

def plot_dth_dist(f7, f3, dist_space, f7b=None, f3b=None):
    '''
    Plot the distribution of potential temperature changes

    Parameters
    ----------
    f7 : numpy array
        discretized distribution for 7 days past. set 1 if f7b !is None
    f3 : numpy array
        discretized distribution for 3 days past. set 1 if f3b !is None
    dist_space : numpy array
        distribution space.
    f7b : numpy array, optional
        discretized distribution for 7 days past for set 2.The default is None.
    f3b : numpy array, optional
        discretized distribution for 3 days past for set 2.The default is None.

    Returns
    -------
    f : figure handle
        the figure.
    ax : ax object
        the ax.

    '''
    from matplotlib.pyplot import rc, subplots, axvline
    from numpy import arange
    rc('font', family='serif')
    #plt.style.use('dark_background')
    f, ax = subplots(1, 1, figsize=(8,6))

    if f7b is None:
        # plot Era-Interim
        ax.plot(dist_space, f7, linewidth=1.5, color='black',
                linestyle='-', label = '7 days', zorder=2)
        ax.plot(dist_space, f3, linewidth=1.5, color='red', linestyle='-',
                label = '3 days', zorder=2)
    else:
        ax.plot(dist_space, f7, linewidth=1.5, color='black',
                linestyle='-', label = '7 days, set 1', zorder=2)
        ax.plot(dist_space, f7b, linewidth=1.5, color='black',
                linestyle=':', label = '7 days, set 2', zorder=2)
        ax.plot(dist_space, f3, linewidth=1.5, color='red', linestyle='-',
                label = '3 days, set 1', zorder=2)
        ax.plot(dist_space, f3b, linewidth=1.5, color='red', linestyle=':',
                label = '3 days, set 2', zorder=2)

    axvline(2, color='lightgray',linewidth=0.7)#, linestyle='--')
    #plt.figtext(0.36, 0.038, r"2", color='black', fontsize=11)

    ax.set_xlim(-15, 20)
    ax.set_xticks(arange(-15, 20+1, 5))
    # ax.set_ylim(0.0, 0.06)
    #ax.set_yticks(np.arange(0, 0.151, 0.03))
    ax.legend(loc="upper right", prop={'size': 9},frameon=False)
    ax.set_xlabel(r"$\Delta\theta$ (K)")
    ax.set_ylabel('Probability density')

    return f, ax


def plot_set_hist(data7, data3, kclustL=None):
    '''
    Plot a histogram for the distribution of variables. Optionally separate
    by coherent sets using kclustL

    Parameters
    ----------
    data7 : numpy array
        7 day backwards data.
    data3 : numpy array
        3 day backwards data.
    kclustL : array, optional
        assigning trajectories to clusters. The default is None.

    Returns
    -------
    fig : figure handle
        the figure.
    ax : axes object
        set 1.
    ax2 : axes object
        set 2.

    '''
    from matplotlib.pyplot import subplots, tight_layout

    n_bin = 60
    fig, ax = subplots(1,1,figsize=(8,6))
    ax2 = ax.twinx()
    colors = ['b','g']

    if kclustL is None:
        ax.hist(data7, bins=n_bin, color=colors[0], label="7 days", alpha=0.5)
        ax.hist(data3, bins=n_bin, color=colors[0], label="3 days", alpha=0.5,
                hatch='/')
        ax.set_ylabel("Count", color=colors[0])
    else:
        data71 = data7[kclustL == 0]
        data72 = data7[kclustL == 1]
        data31 = data3[kclustL == 0]
        data32 = data3[kclustL == 1]

        n, bins, patches = ax.hist([data71,data72, data31, data32], bins=n_bin)
        ax.cla() #clear the axis
        width = (bins[1] - bins[0]) * 0.45
        bins_shifted = bins + width
        ax.bar(bins[:-1], n[0], width, align='edge', color=colors[0],
               label="7 days, set 1", alpha=0.5)
        ax.bar(bins[:-1], n[2], width, align='edge', color=colors[0],
               label="3 days, set 1", alpha=0.5, hatch='/')
        ax2.bar(bins_shifted[:-1], n[1], width, align='edge',
                color=colors[1], label="7 days, set 2", alpha=0.5)
        ax2.bar(bins_shifted[:-1], n[3], width, align='edge',
                color=colors[1], label="3 days, set 2", alpha=0.5, hatch='/')

        #finishes the plot
        ax.set_ylabel("Count", color=colors[0])
        ax2.set_ylabel("Count", color=colors[1])
        ax.tick_params('y', colors=colors[0])
        ax2.tick_params('y', colors=colors[1])
    ax.set_xlim((-20,50))
    ax.set_xlabel(r"$\Delta\theta$ (K)")
    ax.legend(loc='upper left')
    ax2.legend(loc='upper right')
    tight_layout()

    return fig, ax, ax2


def plot_spaghetti(ax, dat, alpha, colors=None, labels=None, inv=None,
                   text_coords=None):
    '''
    Do a spaghetti plot

    Parameters
    ----------
    ax : axes object
        the axes to which to plot.
    dat : array
        contains tha data to be plotted.
    alpha : float
        alpha of lines to be plotted.
    colors : array
        contains rgb values of for each line.

    Returns
    -------
    None.

    '''
    if not (inv is None):
        plot_dat = pd.DataFrame(dat)
        plot_dat['id'] = inv
        plot_dat = plot_dat.melt(id_vars=['id']).rename(
            columns={'variable': 't'})
        plot_dat['t'] = plot_dat['t'] - 72
        sb.lineplot(x='t', y='value', hue='id', palette=colors,
                    estimator="median", errorbar=('pi', 50), data=plot_dat,
                    ax=ax)
        if not (text_coords is None):
            medians = plot_dat.groupby(['id', 't']).median().reset_index()
            for i in np.unique(inv):
                medianLine = medians[medians['id'] == i]
                lastPoint = medianLine.iloc[text_coords[i][0]]
                ax.annotate(f'{i}', xy=(lastPoint['t'],
                                            lastPoint['value']),
                            xytext=text_coords[i][1],
                            textcoords='offset points',
                            color=colors[i],
                            arrowprops=dict(arrowstyle="-", color=colors[i]))
        ax.grid()
        ax.set_xlabel("$t$ [h]")
        ax.set_xlim([plot_dat['t'].iloc[0], plot_dat['t'].iloc[-1]])

    else:
        for i in np.arange(0,dat.shape[0]-1):
            if colors is None:
                ax.plot( np.arange(0, dat.shape[1]), dat[i, :],
                    alpha=alpha)
            elif labels is None:
                ax.plot( np.arange(0, dat.shape[1]), dat[i, :],
                    c=colors[i],# if i == 0 else "",
                    alpha=alpha)
            else:
                ax.plot( np.arange(0, dat.shape[1]), dat[i, :],
                    c=colors[i], label=labels[i],# if i == 0 else "",
                    alpha=alpha)

def draw_vert_t(ax, t_0, t_n, t_i):
    '''
    Draw a red dotted vertical line at t_i and red full lines at t_0 and t_i

    Parameters
    ----------
    ax : axes object
        the axes to which to plot.
    t_0 : integer
        initial time step.
    t_n : integer
        last time step.
    t_i : integer
        current time step.

    Returns
    -------
    list
        containes the line objects.

    '''
    yy = ax.get_ylim()
    yy = (yy-np.mean(yy))*0.9 + np.mean(yy)
    v1 = ax.vlines(t_0, yy[0], yy[1], alpha=0.4, color="red")
    v2 = ax.vlines(t_n, yy[0], yy[1], alpha=0.4, color="red")
    v3 = ax.vlines(t_i, yy[0], yy[1],
              alpha=0.4, color="red", linestyle="dashed")
    return [v1, v2, v3]


def plot_clustering(ax3d, X, Y, Z, azim, elev, dist, t_i, c, plot_0=0,
                    coord="xyz", mean_traj=True, block=None, coast=None):
    '''
    Plot air parcels above wireframed earth surface at initial and t_i time
    step. Color according to c.

    Parameters
    ----------
    ax3d : axes object
        3d axes to plot to.
    X : array
        x coord.
    Y : array
        y coord.
    Z : array
        z coord.
    azim : float
        azimuth view angle.
    elev : float
        elevation view angle.
    dist : float
        distance.
    t_i : int
        time slice to plot.
    c : array
        integer that assigns each point to a cluster.
    bounds : array
        boolean array that shows whether point is boundary or not.

    Returns
    -------
    None.

    '''
    from warnings import catch_warnings, simplefilter

    if coord == "xyz":

        lons = np.linspace(-180, 180,100)
        lats = np.linspace(0, 90, 100)
        (lons, lats) = np.meshgrid(lons, lats)
        (x, y, z) = cc.lonlatrtoxyz(lons, lats, r=6371)

    ax3d.view_init(azim=azim, elev=elev)
    with catch_warnings():
        simplefilter("ignore")
        ax3d.dist=dist
    minxy = np.min([np.min(X), np.min(Y)])
    maxxy = np.max([np.max(X), np.max(Y)])
    ax3d.set_xlim((minxy, maxxy))
    ax3d.set_ylim((minxy, maxxy))
    # ax3d.set_xlim((np.min(X), np.max(X)))
    # ax3d.set_ylim((np.min(Y), np.max(Y)))
    ax3d.set_zlim((np.min(Z), np.max(Z)))

    if coord == "xyz":
        ax3d.plot_wireframe(x, y, z, color="blue", rstride=10, cstride=10,
                            alpha=0.2)

    if not(isinstance(plot_0, bool)):
        points = ax3d.scatter(X[:,plot_0],Y[:,plot_0],Z[:,plot_0],c=c,
                              cmap="Set1", alpha=0.25)

    points = ax3d.scatter(X[:,t_i],Y[:,t_i],Z[:,t_i],c=c, cmap="Set1",
                          alpha=0.25)

    if mean_traj:
        for i, c_i in enumerate(np.unique(c)):
            mean_x = np.mean(X[c==c_i, :], axis=0)
            mean_y = np.mean(Y[c==c_i, :], axis=0)
            mean_z = np.mean(Z[c==c_i, :], axis=0)
            ax3d.plot(mean_x, mean_y, mean_z, color=points.to_rgba(c_i))

    if not(block is None):
        if coord=="stereo":
            xb, yb, _ = cc.coord_trans(block[1], block[2], block[1], "None", 1,
                                       proj="stereo")
            ax3d.contour(xb, yb, block[0][int(t_i/6),:,:], [0.5], zdir="z",
                         offset=1050,
                         colors="k")
            if plot_0:
                ax3d.contour(xb, yb, block[0][int(plot_0/6),:,:], [0.5], zdir="z",
                             offset=1050,
                             colors="k")

    if not(coast is None):
        if coord=="stereo":
            for lon, lat in coast:
                xc, yc, _ = cc.coord_trans(lon, lat, lon, "None", 1,
                                           proj="stereo")
                z = [1050] * len(xc)
                ax3d.plot(xc, yc, z, color='grey')
                # This will connect the points as line segments

            # longitudes = np.arange(-270, -91, 60)
            # longitudes = np.arange(-60, 121, 60)
            longitudes = np.arange(-180, 181, 60)
            latitudes = np.arange(10, 91, 20)
            # Plot longitudes (meridians)
            for lon in longitudes:
                lats = np.linspace(latitudes[0], latitudes[-1], 180//20 + 1)
                lons = np.full(lats.shape, lon)
                xc, yc, _ = cc.coord_trans(lons, lats, lons, "None", 1,
                                           proj="stereo")
                z = np.full(xc.shape, 1050)
                ax3d.plot(xc, yc, z, linestyle='--', color='grey')

            # Plot latitudes (parallels)
            for lat in latitudes:
                if abs(lat) == 90:
                    continue  # Skip poles
                lons = np.linspace(longitudes[-1], longitudes[0], 360//2 + 1)
                lats = np.full(lons.shape, lat)
                xc, yc, _ = cc.coord_trans(lons, lats, lons, "None", 1,
                                           proj="stereo")
                z = np.full(xc.shape, 1050)
                ax3d.plot(xc, yc, z, linestyle='--', color='grey')
    return points
