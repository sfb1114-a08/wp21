 #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 13 12:13:23 2023

@author: schoelleh96
"""

from sys import path
import numpy as np
# path.append("../wp21/pyscripts/LIB")
import calc as cc
import pickle
from os.path import exists
from os import makedirs
import cdsapi
import netCDF4 as nc
from mpl_toolkits.basemap import Basemap
from datetime import datetime

def get_coasts(llcrnrlat, urcrnrlat, llcrnrlon, urcrnrlon):
    # Initialize a Basemap instance
    # (projection does not matter in this case since we won't use it)
    m = Basemap(resolution='c', llcrnrlat=llcrnrlat, urcrnrlat=urcrnrlat,
            llcrnrlon=llcrnrlon, urcrnrlon=urcrnrlon)
    # Access coastlines directly in lon/lat format
    coastline_lon_lat = []

    for polygon in m.coastsegs:
        lon, lat = zip(*polygon)
        coastline_lon_lat.append((tuple(x + 360 for x in lon), lat))

    return coastline_lon_lat

def get_block_dat(date_0):

    # Blocking data
    blockpath = "/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/Blocks/"

    if isinstance(date_0, datetime):
        # single start date
        blockfile = blockpath + date_0.strftime("%Y-cropped") + '.nc'

        with nc.Dataset(blockfile) as ncf:
            #print(ncf)
            # convert hour to datetime
            times = ncf.variables['time'] #  days since refdate
            date_list = [nc.num2date(xx,units = "hours since 1979-01-01 00:00:00",
                                     calendar="proleptic_gregorian")
                         for xx in times]
            flag = ncf.variables['FLAG'][:][(date_list.index(date_0)-13):
                                            (date_list.index(date_0)+12),:,:]
            LO = ncf.variables['longitude'][:]
            LA = ncf.variables['latitude'][:]

        LO, LA = np.meshgrid(LO, LA)

        return list((flag, LO, LA))
    else:
        # list of start dates
        blockfile = blockpath + date_0[0].strftime("%Y-cropped") + '.nc'
        with nc.Dataset(blockfile) as ncf:
            # convert hour to datetime
            times = ncf.variables['time'] #  days since refdate
            date_list = [nc.num2date(xx,units = "hours since 1979-01-01 00:00:00",
                                     calendar="proleptic_gregorian")
                         for xx in times]
            flag = ncf.variables['FLAG'][:][
                (date_list.index(date_0[0])-13):
                    (date_list.index(date_0[-1])+12),:,:]
            LO = ncf.variables['longitude'][:]
            LA = ncf.variables['latitude'][:]

        LO, LA = np.meshgrid(LO, LA)

        return list((flag, LO, LA))


def comb_traj(path, btraj, ftraj):
    """
    stich together forward and backward trajectories

    Parameters
    ----------
    path : string
        traj location.
    btraj : string
        backwards traj name.
    ftraj : string
        forward traj name.

    Returns
    -------
    None.

    """
    bt = np.load(path + btraj)
    ft = np.load(path + ftraj)
    np.save(path + btraj[1:], np.concatenate((np.flip(bt, axis=1), ft),
                                             axis=1))


def retrieve_dat(date, path, level):
    """
    Download upper-level pv fields on pressure levels from ERA5 at date to
    path

    Parameters
    ----------
    dates : datetime format
        date to be downloaded.
    path : string
        absolute path to retrieve data to.

    Returns
    -------
    None.

    """
    c = cdsapi.Client()

    if level=="upper":
        if not exists(path + "plevdat" + date.strftime('/%Y/%m-%d.nc')):
            makedirs(path + "plevdat" + date.strftime('/%Y'), exist_ok=True)
            c.retrieve("reanalysis-era5-pressure-levels", {
                'product_type': 'reanalysis',
                'variable': ['potential_vorticity','u_component_of_wind',
                'v_component_of_wind', 'geopotential', 'vertical_velocity'],
                'pressure_level': [
                    '100', '125', '150',
                    '175', '200', '225',
                    '250', '300', '350',
                    '400', '450', '500',
                    '550', '600', '650',
                    '700', '750', '775',
                    '800', '825', '850',
                    '875', '900', '925',
                    '950', '975', '1000'
                ],
                "date": date.strftime('%Y-%m-%d'),
                'time': [
                    '00:00', '01:00', '02:00',
                    '03:00', '04:00', '05:00',
                    '06:00', '07:00', '08:00',
                    '09:00', '10:00', '11:00',
                    '12:00', '13:00', '14:00',
                    '15:00', '16:00', '17:00',
                    '18:00', '19:00', '20:00',
                    '21:00', '22:00', '23:00',
                ],
                'format' : 'netcdf',
                'grid'    : '0.5/0.5',
                'area': [90, -180, 0, 180],
            }, path + "plevdat" + date.strftime('/%Y/%m-%d.nc'))
    elif level=="surf":
        if not exists(path + "surfdat" + date.strftime('/%Y/%m-%d-%H.nc')):
            makedirs(path + "surfdat" + date.strftime('/%Y'), exist_ok=True)
            c.retrieve('reanalysis-era5-single-levels', {
                'product_type': 'reanalysis',
                'format': 'netcdf',
                'variable': 'mean_sea_level_pressure',
                'area': [90, -180, 0, 180],
                "date": date.strftime('%Y-%m-%d'),
                "time": date.strftime('%H:00:00'),
                'grid'    : '0.5/0.5',
                'format' : 'netcdf',
            }, path + "surfdat" + date.strftime('/%Y/%m-%d-%H.nc'))

def io_eigen(d_path, m, Tmin, Tmax, x_list, y_list, v_list, epsilon, bounds,
             N_k=20, x=None, y=None, z=None, customMetric=None, k=None,
             z_coord_type=None, force_calc=False):
    '''
    Depending on whether calculated eigenvalues and vectors exist or not,
    calculate or load.

    Parameters
    ----------
    d_path : string
        relative path to data.
    m : int
        number of points.
    Tmin : int
        first time slice.
    Tmax : int
        last time slice.
    x_list : list
        x position  values of sparse matrix entries.
    y_list : list
        y position  values of sparse matrix entries.
    v_list : list
        values of sparse matrix entries
    epsilon: float
        diffusion coefficient
    bounds : 1D numpy array, optional
        array defining boundary points, default value None
    N_k : int, optional
        Number of eigenvectors and eigenvalues. Default value 20
    Returns
    -------
    vals : array
        eigenvalues.
    vecs : array
        eigenvectors.

    '''
    if (not exists(d_path + str(int(epsilon))) or force_calc):
        print("calculating E")
        vals, vecs = cc.eigen(m, Tmin, Tmax, x_list, y_list, v_list,
                              epsilon, bounds, N_k, d_path,
                              x, y, z, customMetric,
                              k, z_coord_type)
        with open(d_path + str(int(epsilon)), "wb") as f:
            pickle.dump(list((vals, vecs)), f)
    else:
        print("loading E")
        with open(d_path + str(int(epsilon)), "rb") as f:
            vals, vecs = pickle.load(f)
    return vals, vecs


def io_dist(d_path, e, x, y, z, customMetric, k, z_coord_type, t_sel,
            returnD=True, force_calc=False):
    '''
    Depending on whether calculated distances exist or not calculate or load

    Parameters
    ----------
    d_path : string
        relative path to data.
    e : float
        epsilon.
    x : array
        x-coord.
    y : array
        y-coords.
    z : array
        z-coord.
    customMetric : boolean
        whether to use the custom metric.
    k : float
        scaling parameter for the vertical.
    z_coord_type : string
        which kind of vertical coordinates are used.

    Returns
    -------
    D : list
        contains three lists with x, y, v
    where x[j], y[j] and v[j] are the vectors that form the jth sparse matrix.

    '''
    if not exists(d_path + "dist" + z_coord_type + "/"):
        makedirs(d_path + "dist" + z_coord_type + "/")
    D = list((list(), list(), list()))
    for t in t_sel:
        if ((not exists(d_path + "dist" + z_coord_type + "/" + str(t)))
            or force_calc):
            print("calculating distances at {}".format(t))
            D_t = cc.distances(x[:,t-t_sel[0]], y[:,t-t_sel[0]], 3*np.sqrt(e),
                             x.shape[0], dataZ=z[:,t-t_sel[0]],
                             customMetric=customMetric, k=k)
            with open(d_path + "dist" + z_coord_type + "/" + str(t),
                      "wb") as f:
                pickle.dump(D_t, f)
        else:
            print("Loading Distances at {}".format(t))
            with open(d_path + "dist" + z_coord_type + "/" + str(t),
                      "rb") as f:
                D_t = pickle.load(f)
        if returnD:
            D[0].append(D_t[0])
            D[1].append(D_t[1])
            D[2].append(D_t[2])
    return D

def io_bounds(d_path, meth, x, y, z, alph, force_calc=False):
    '''
    Depending on whether calculated boundaries exist or not calculate or load

    Parameters
    ----------
    d_path : string
        relative path to data.
    meth : string
        which boundary method to use.
    x : array
        x-coord.
    y : array
        y-coords.
    z : array
        z-coord.
    alph : float
        alpha value for alpha shapes.

    Returns
    -------
    bounds : array
        for every point at every timestep 1 if boundary and 0 if not.
    hulls : list
        trimesh for every timestep showing the boundary.

    '''

    if meth == "Convex":
        if (not exists(d_path + "bound_convex")) or force_calc:
            bounds, hulls = cc.get_bounds(x, y, z, convex=True)
            with open(d_path + "bound_convex", "wb") as f:
                pickle.dump(list((bounds, hulls)), f)
        else:
            with open(d_path + "bound_convex", "rb") as f:
                bounds, hulls = pickle.load(f)
            print("Boundary Loaded")
    elif meth == "$\\alpha$":
        if (not exists(d_path +
                      "bound_alph" + str(alph))) or force_calc:
            bounds, hulls = cc.get_bounds(x, y, z,
                                          alpha=alph)
            with open(d_path +
                      "bound_alph" + str(alph), "wb") as f:
                pickle.dump(list((bounds, hulls)), f)
        else:
            with open(d_path +
                      "bound_alph" + str(alph), "rb") as f:
                bounds, hulls = pickle.load(f)
                print("Boundary Loaded")
    elif meth == "opt. $\\alpha$":
        if (not exists(d_path + "bound_opt")) or force_calc:
            bounds, hulls = cc.get_bounds(x, y, z,
                                          alpha=None)
            with open(d_path + "bound_opt", "wb") as f:
                pickle.dump(list((bounds, hulls)), f)
        else:
            with open(d_path + "bound_opt", "rb") as f:
                bounds, hulls = pickle.load(f)
            print("Boundary Loaded")
    return bounds, hulls

def save_D(D, k, date_0, r):

    path = date_0.strftime("./DATA/%Y/D/D_%Y%m%d_%H_k" + "{:.2f}".format(k)
                           + "r_" + str(r))

    with open(path, "wb") as fp:
        pickle.dump(D, fp)



def save_overlap(kclustL, v_trans, scaling, use_p, date_0, ctype, max_T):
    '''
    Write the partitioning of some set of trajectories to a csv file along
    with all necessary information.

    Parameters
    ----------
    kclustL : numpy array
        contains cluster labels.
    h_trans : string
        horizontal transformation flag.
    v_trans : string
        vertical transformation flag.
    scaling : string
        flag for scaling coordinates to adjust for different scales of motion.
    scaling : boolean
        flag for whether vertical coordinates should be used
    date_0 : datetime
        start date of trajectories.
    ctype : string
        type of clustering.

    Returns
    -------
    None.

    '''

    from pandas import DataFrame

    path = date_0.strftime("./DATA/%Y/overlap.csv")
    D = DataFrame(data={'date_0':date_0, 'cID':kclustL, 'v_trans': v_trans,
                        'scaling': scaling, 'use_p': use_p, 'ctype': ctype,
                        'max_T': max_T})
    D.to_csv(path, mode='a', header=not exists(path))

def save_to_file(date, trajs, dth3, dth7, kclust):
    '''
    Saves necessary information about the trajectories and the clusters to a
    csv file.

    Parameters
    ----------
    date : datetime
        Trajectory start date.
    trajs : trajectory object
        The trajectories.
    dth3 : array
        Contains the max/min theta difference.
    kclust : cKD cluster object
        obtained from performing spectral clustering on the sparse distance
        matrix.

    Returns
    -------
    None.

    '''
    from pandas import DataFrame

    path = './DATA/stats.csv'

    D = DataFrame(data={'date':date_0, 'lon': trajs['lon'][:,0].data,
                        'lat': trajs['lat'][:,0].data,
                        'p': trajs['p'][:,0].data, 'dth3': dth3, 'dth7':dth7,
                        'cID': kclust.labels_})
    D.to_csv(path, mode='a', header=not exists(path))