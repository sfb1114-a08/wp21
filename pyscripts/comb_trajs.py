#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 21 10:05:31 2023

@author: schoelleh96
"""

from os.path import  dirname, abspath
from os import chdir
chdir(dirname(abspath(__file__)))

from sys import argv
import numpy as np
from lagranto import Tra
from sys import path
path.append("./LIB")
import calc as cc

# %%

print(argv)

trapath = argv[1]
date_0 = argv[2]
btrajs = Tra()
btrajs.load_ascii(trapath + "btraj_" + date_0)
ftrajs = Tra()
ftrajs.load_ascii(trapath + "ftraj_" + date_0)
print(trapath)
print(date_0)
combtraj = np.concatenate((np.flip(btrajs.get_array().transpose().data,
                                   axis=1),
                           ftrajs.get_array().transpose().data[:,1:]), axis=1)
print(combtraj.shape)
np.save(trapath + "traj_" + date_0, combtraj)

U = combtraj['U']
V = combtraj['V']
Omg = combtraj['OMEGA']

k_p = cc.calc_k(U/1000, V/1000, Omg/100)

with open("/home/schoelleh96/Nextcloud/ObsidianVault/Work/Projects/kp.md",
          "a") as kptable:
    kptable.write("| " + date_0 + " | {:2.2f} |\n".format(k_p))

