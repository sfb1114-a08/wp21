#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 19 09:46:21 2023

@author: schoelleh96
"""

# =============================================================================
# This script plots differences in trajectories calculated from era5 vs era5kit
# Usage: python era_diff.py <directory with era5 data> <directory with era5kit
# data> <Year> <Month> <Day> <Hour>
# =============================================================================

# %% library imports

# This will move the console to the right working directory.
from os.path import  dirname, abspath
from os import chdir, getcwd
chdir(dirname(abspath(__file__)))

from LIB import plot as pp
from LIB import calc as cc
from sys import argv
from datetime import datetime
from lagranto import Tra
from matplotlib.pyplot import subplots, figure
from numpy import sqrt, unique, where
from matplotlib.cm import Set1

# %% get the data

if argv[0]=='':
    date_0 = datetime(2016,5,1,6)
    tradirkit = date_0.strftime("/net/scratch/schoelleh96/WP2/WP2.1/" +
                             "LAGRANTO/wp21/traj/%Y/")
    tradirera = date_0.strftime("/net/scratch/schoelleh96/WP2/WP2.1/" +
                             "LAGRANTO/wp21/era5/traj/%Y/")
else:
    tradirkit = argv[2]
    tradirera = argv[1]
    date_0 = datetime(int(argv[3]),int(argv[4]),int(argv[5]),int(argv[6]))
    from matplotlib import use
    use('agg')

print("cwd: ", getcwd())
print("tradirkit:", tradirkit)
print("tradirera:", tradirera)
print("Date: ", date_0)

# define filename
traname = date_0.strftime('traj_%Y%m%d_%H')

trajskit = Tra()
trajskit.load_ascii(tradirkit + traname, gz=False)
trajskit.set_array(trajskit.get_array().transpose()[:,:73])#three days only
print(trajskit)
trajsera = Tra()
trajsera.load_ascii(tradirera + traname, gz=False)
trajsera.set_array(trajsera.get_array().transpose())#[:,:73])#three days only
print(trajsera)

# %% plotting

xkit = trajskit['lon']
ykit = trajskit['lat']
pkit = trajskit['p']
hkit = cc.ptoh(pkit, 1013.25, 8.435)
xera = trajsera['lon']
yera = trajsera['lat']
pera = trajsera['p']
hera = cc.ptoh(pera, 1013.25, 8.435)

scaling = 1
azim = 150
elev = 45
dist = 7
t_i = 0

def get_n_colors(n):
    return[ Set1(float(i)/n) for i in range(n) ]

cols = get_n_colors(unique(pera[:,0]).shape[0])

inds = [where(unique(pera[:,0]) == p)[0][0]
        for p in pera[:,0]]

colv = [cols[i] for i in inds]
diff = cc.calc_traj_diff(xkit, ykit, hkit, xera, yera, hera)

fig, ax = subplots(1,1)

pp.plot_spaghetti(ax, diff, 0.4,
                  colors=colv, labels=pera[:,0])

ax.set_title("Difference btw. Era5 and Era5kit")
ax.set_xlabel("timestep")
ax.set_ylabel("km")
handout=[]
lablout=[]
for h,l in zip(hand,labl):
   if l not in lablout:
        lablout.append(l)
        handout.append(h)
leg = fig.legend(handout, lablout)

for lh in leg.legendHandles:
    lh.set_alpha(1)


fig = figure()#, constrained_layout=True)
rect = 0, 0, 1, 1
ax3d = fig.add_axes(rect, projection="3d")

Xera, Yera, Zera = cc.coord_trans(xera, yera, pera, v_trans="std_atm",
                                  scaling=scaling)
Xkit, Ykit, Zkit = cc.coord_trans(xkit, ykit, pkit, v_trans="std_atm",
                                  scaling=scaling)

pp.plot_clustering(ax3d, Xera, Yera, Zera, azim, elev, dist, t_i, c=None,
                   bounds=None)
pp.plot_clustering(ax3d, Xkit, Ykit, Zkit, azim, elev, dist, t_i, c=None,
                   bounds=None)

DDIFF = sqrt((Xera-Xkit)**2 + (Yera-Ykit)**2 + (Zera-Zkit)**2)

fig, ax = subplots(1,1)

pp.plot_spaghetti(ax, DDIFF, 0.2)

difdif = DDIFF - diff
fig, ax = subplots(1,1)

pp.plot_spaghetti(ax, difdif, 0.2)
fig, ax = subplots(1,1)

ax.scatter(difdif, diff)
fig, ax = subplots(1,1)

ax.scatter(difdif, DDIFF)
fig, ax = subplots(1,1)

ax.scatter(diff, DDIFF)
