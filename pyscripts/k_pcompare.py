#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  1 10:16:24 2024

@author: schoelleh96
"""
# %% Library Imports

# This will move the console to the right working directory.
from os.path import  dirname, abspath, exists
from os import chdir
chdir(dirname(abspath(__file__)))

import pandas as pd
from datetime import datetime, timedelta
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sb
import matplotlib.dates as mdates
from matplotlib.ticker import MaxNLocator
import cmcrameri.cm as cmc
import locale

# %% Initials

YEAR = "2016"
# Initialize dates
if YEAR == "2016":
    dates = [datetime(2016, 4, 29, 6) + timedelta(hours=i)
             for i in range(0, 144, 6)]
elif YEAR == "2017":
    dates = [datetime(2017, 1, 23, 12) + timedelta(hours=i)
             for i in range(0, 28*6, 6)]


trapath = "../era5/traj/" + YEAR + "/"

base_font_size = 10  # pt
locale.setlocale(locale.LC_TIME, 'en_US')

plt.rcParams['text.usetex'] = True
plt.rcParams['pgf.texsystem'] = "pdflatex"
plt.rcParams['text.latex.preamble'] = (
    r'\usepackage{amsmath,amsfonts,amssymb,cmbright,standalone}')
plt.rcParams['font.size'] = base_font_size

# %% functions


def calc_k(trapath, date_0):

    trajs = np.load(trapath + "traj_" + date_0 + ".npy")

    U = trajs['U']
    V = trajs['V']
    U_h = np.sqrt(U**2 + V**2)

    Omg = trajs['OMEGA']

    df = pd.DataFrame({'date': date_0,
                       'm': U.shape[0],
                       't': np.arange(-72, 73),
                       'U': U.mean(axis=0),
                       'V': V.mean(axis=0),
                       'Uh': U_h.mean(axis=0),
                       'Omg': Omg.mean(axis=0),
                       'Omg_abs': np.abs(Omg).mean(axis=0),
                       'k': ((U_h.mean(axis=0)/1000)/(
                           (np.abs(Omg).mean(axis=0))/100))})

    return df

# %% Loop-de-loop


k_list = list()
for date in dates:
    print(date)
    k_list.append(calc_k(trapath, date.strftime('%Y%m%d_%H')))

k_df = pd.concat(k_list, ignore_index=True)

# %% plot

def plot_k(k_df, name, var):

    fig = plt.figure(figsize=(3.2, 3.4))
    ax = fig.add_subplot(111)

    # Create the heatmap
    data = k_df.pivot(index="date", columns="t",
                      values=var)
    if var in ["U", "V", "Omg"]:
        cax = sb.heatmap(data, annot=False, cmap=cmc.vik, ax=ax,
                         antialiased=True, center=0,
                         linewidths=0, rasterized=True,
                         cbar_kws={"orientation": "horizontal"})
    else:
        cax = sb.heatmap(data, annot=False, cmap=cmc.batlow, ax=ax,
                         antialiased=True,
                         linewidths=0, rasterized=True,
                         cbar_kws={"orientation": "horizontal"})
    cbar = cax.collections[0].colorbar
    ax.set_yticks([])  # Remove y-axis ticks
    ax.set_yticklabels([])  # Remove y-axis tick labels

    # if name == "2016":
    #     ax.set_yticks(np.arange(3.5, len(dates), 4))
    #     ax.set_yticklabels(
    #         [datetime.strptime(d, "%Y%m%d_%H").strftime("%d %B")
    #          for d in dates[3::4]])
    # elif name == "2017":
    #     ax.set_yticks(np.arange(1.5, len(dates), 4))
    #     ax.set_yticklabels(
    #         [datetime.strptime(d, "%Y%m%d_%H").strftime("%d %B")
    #          for d in dates[1::4]])
    if var == "U":
        cbar.set_label('$\\overline{u}$ [m s\\textsuperscript{-1}]')
    elif var == "V":
        cbar.set_label('$\\overline{v}$ [m s\\textsuperscript{-1}]')
    elif var == "Uh":
        # ax.set_yticklabels([])
        cbar.set_label('$\\overline{u_h}$ [m s\\textsuperscript{-1}]')
    elif var == "Omg":
        cbar.set_label('$\\overline{\\omega}$ [Pa s\\textsuperscript{-1}]')
    elif var == "k":
        cbar.set_label('$\\kappa$ [km hPa\\textsuperscript{-1}]')

    t = data.columns.tolist()
    ax.set_xticks([0.5, int(len(t)/2 + 0.5) - 0.5, len(t)-0.5])
    tick_labels = [r'${}$'.format(i) for i in
                   [t[0], t[int(len(t)/2)], t[-1]]]
    ax.set_xticklabels(tick_labels, rotation=0)

    ax.set_xlabel('')

    plt.figtext(0.25, 0.3, r"$t$ [h] $\rightarrow$", ha="center", va="center",
                fontsize=base_font_size)
    ax.set_ylabel('')
    # plt.title(name)

    numRows = len(data.index)
    numCols = len(data.columns)

    curr_date = dates[0] - timedelta(hours=(numCols-1)/2)
    i = 0  # iterates through rows
    while i + 1 < numRows + numCols/6:
        if curr_date.hour == 0:
            if i + 1 > numRows:
                x_start = (i - numRows + .5)*6 + .5
                y_start = numRows
            else:
                x_start = 0
                y_start = i + .5 + 1/12
            if i*6 > numCols:
                x_end = numCols
                y_end = ((i*6 - numCols + 1)/6 + .5 - 1/12)
            else:
                x_end = (i + .5 + 1/12)*6
                y_end = 0

            ax.plot([x_start, x_end], [y_start, y_end], color="w",
                    linewidth=.25)
            ax.text(x_end, y_end, curr_date.strftime("%d %b"), rotation=45,
                    horizontalalignment="left", verticalalignment="bottom")
        i += 1
        curr_date = curr_date + timedelta(hours=6)

    ax.plot([numCols/2, numCols/2], [0, numRows], color="w", linewidth=.25)

    plt.savefig("../im/kpm/" + var + name + ".pdf", bbox_inches="tight")

    return fig

def plot_kmean(k_df, name):

    colors = ['#ff7f0e', '#0071bc', '#008080', '#76b7b2']
    # Blue, Orange, Butter Yellow, Sky Blue
    fig, ax1 = plt.subplots(figsize=(3.25, 2))

    # m
    ax1.set_xlabel('Initialization date')
    ax1.set_ylabel('$m$ [--]', color=colors[0])
    ax1.plot(k_df['date'], k_df['m'], color=colors[0],
             marker='s', linestyle='-', label='$m$')
    ax1.tick_params(axis='y', labelcolor=colors[0])
    ax1.xaxis.set_major_formatter(mdates.DateFormatter('%d %b'))
    # locator = mdates.AutoDateLocator(minticks=5, maxticks=5)
    locator = MaxNLocator(nbins=3)
    # Adjust maxticks as needed
    ax1.xaxis.set_major_locator(locator)
    # ax1.tick_params(axis='x', rotation=15)
    # ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

    # k
    ax4 = ax1.twinx()
    ax4.set_ylabel('$\\kappa$ [km hPa\\textsuperscript{-1}]', color=colors[1])
    ax4.plot(k_df['date'], k_df['k'], color=colors[1],
             marker='o', linestyle='-', label='$\\kappa$')
    ax4.tick_params(axis='y', labelcolor=colors[1])

    # # u_h
    # ax2 = ax1.twinx()
    # ax2.set_ylabel('$\\overline{u_{\\mathrm{h}}}$ [m s\\textsuperscript{-1}]',
    #                color=colors[2])
    # ax2.plot(k_df['date'], k_df['Uh'], color=colors[2],
    #          marker='*', linestyle='-', label='$\\overline{u_{\\mathrm{h}}}$')
    # ax2.tick_params(axis='y', labelcolor=colors[2])
    # ax2.spines['right'].set_position(('outward', 35))
    # # Ensure ticks are at integer values and labels are formatted as integers
    # # ax2.yaxis.set_major_locator(MaxNLocator(nbins='auto', integer=True))
    # # ax2.yaxis.set_major_formatter(FormatStrFormatter('%d'))

    # ax3 = ax1.twinx()
    # ax3.set_ylabel(
    #     '$\\overline{ \\vert  \\omega \\vert }$ [Pa s\\textsuperscript{-1}]',
    #     color=colors[3])
    # ax3.plot(k_df['date'], k_df['Omg_abs'], color=colors[3],
    #          marker='*', linestyle='-',
    #          label='$\\vert \\overline{\\omega} \\vert$')
    # ax3.tick_params(axis='y', labelcolor=colors[3])
    # ax3.spines['right'].set_position(('outward', 70))

    if YEAR == "2017":
        ax4.set_yticks([14, 16, 18, 20])
        # ax3.set_yticks([0.1, 0.15])

    fig.tight_layout()
    plt.show()

    plt.savefig("/net/scratch/schoelleh96/WP2/WP2.1/" +
                "LAGRANTO/wp21/im/kpm/k_m" + str(YEAR) + ".pdf",
                bbox_inches="tight")

# %% Application


for var in ["U", "V", "Uh", "Omg", "k"]:
    plot_k(k_df, YEAR, var)

# Ensure 'date' column is not affected by the mean calculation
columnsToAverage = k_df.columns.difference(['date', 't'])

# Group by 't' and calculate mean for the specified columns
averagedDf = k_df.groupby('date')[columnsToAverage].mean().reset_index()
averagedDf['k_after'] = (averagedDf['Uh']/1000)/(averagedDf['Omg']/100)

averagedDf["date"] = pd.to_datetime(averagedDf["date"], format='%Y%m%d_%H')
plot_kmean(averagedDf, YEAR)
