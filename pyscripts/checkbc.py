#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  7 15:12:04 2023

@author: schoelleh96
"""


# This will move the console to the right working directory.
from os.path import  dirname, abspath, exists
from os import chdir, makedirs
chdir(dirname(abspath(__file__)))

import numpy as np
from sys import path
path.append("./LIB")
import plot as pp
import calc as cc
import data as dd
import matplotlib.pyplot as plt
from datetime import datetime
from warnings import catch_warnings, simplefilter

# %% Get the data

# only every nth trajectory
n = 1

force_calc = False

date_0 = datetime(2016,5,1,6)
fname = "../../csstandalone/traj" + date_0.strftime('_%Y%m%d_%H') + ".npy"
path = ("../../csstandalone/" + date_0.strftime("dat_%Y%m%d_%H") + "/"
        + str(n) + "/")
if not exists(path):
    makedirs(path)

trajs = np.load(fname)

t_sel = np.arange(73, 74)

# Full variables for boundary calculation
Lon = trajs['lon'][::n]
Lat = trajs['lat'][::n]
P = trajs['p'][::n] # is in hPa

# Variables at selected times for visualization and cs
lon = Lon[:, t_sel]
lat = Lat[:, t_sel]
p = P[:, t_sel] # is in hPa
u = trajs['U'][::n, t_sel] # is in m/s
v = trajs['V'][::n, t_sel] # is in m/s
omg = trajs['OMEGA'][::n, t_sel] # is in P/s
T = trajs['T'][::n, t_sel] # is in K

# %% Initials

t_i = 0
alpha = 5e-3
e = 50000
N_k = 3
# u and v is in m/s, omg in P/s; result is in km/hPa
k_p = cc.calc_k(trajs['U']/1000, trajs['V']/1000, trajs['OMEGA']/100)

# Coordinates for visualization
X = lon
Y = lat
Z = p
H = cc.ptoh(p, 1013.25, 8.435) # standard atmosphere
k_h = cc.calc_k(trajs['U'], trajs['V'], cc.omg2w(trajs['OMEGA']/100,
                                                 trajs['T'], trajs['p']))

# %%

xcs, ycs, zcs = cc.coord_trans(Lon, Lat, P, "None", proj="stereo",
                             scaling = k_p)

boundscs, hullscs = dd.io_bounds(
                path + "stereop", "$\\alpha$", xcs, ycs, zcs,
                alpha)

D = dd.io_dist(path, e, lon, lat, p, True, k_p, "p", t_sel)

from scipy.sparse import csc_matrix, find, identity
from numpy import exp, flip, sort
from scipy.sparse.linalg import eigs
from scipy.linalg import eig
from warnings import catch_warnings, simplefilter

bounds = boundscs[:,t_sel]

PP = csc_matrix((xcs.shape[0],xcs.shape[0]))
for j in range(0, len(D[0])):
    K = csc_matrix((exp(-D[2][j]**2/e), (D[0][j], D[1][j])),
                   shape=(xcs.shape[0],xcs.shape[0]))
    K = K + identity(xcs.shape[0]) + K.T

    with catch_warnings():
        simplefilter("ignore")
        K = K.multiply(1/K.sum(axis=1))# 1/rowsums
    if not(bounds is None):
        K = K.tolil()
        K[np.where(bounds[:,j]==1)[0],:]=0
        K[:,np.where(bounds[:,j]==1)[0]]=0
        K = K.tocsr()
        K.eliminate_zeros()

    PP = PP + K

PP = 1/(len(D[0])) * PP

sparsity = find(PP)[0].shape[0]/(PP.shape[0]*PP.shape[1])

print("sparsity for epsilon {}: {}".format(e, sparsity))
try:
    vals, vecs = eigs(PP, k=N_k)
except:
    print("Eigs failed, using eig")
    vals, vecs = eig(PP.toarray())

vals = flip(sort(vals))[:N_k]

f, ax = plt.subplots(1, 1)
ax.scatter(np.arange(0,xcs.shape[0]), vecs[:,0]/np.max(np.abs(vecs[:,0])))

fig = plt.figure()#, constrained_layout=True)
for i in range(3):
    ax3d = fig.add_subplot(1, 3, (i+1), projection='3d')
    s = ax3d.scatter(xcs[:,t_sel], ycs[:,t_sel], zcs[:,t_sel],
                    c=np.real(vecs[:,i]))
    fig.colorbar(s, shrink=0.5, aspect=10, location="bottom")
plt.tight_layout()


fig = plt.figure()#, constrained_layout=True)

for i in range(3):
    for j in range(2):
        kclust = cc.kcluster_idx(list((vals, vecs)), N_k=(2+i), N_v=(1+i+j))

        ax3d = fig.add_subplot(2, 3, (i+1+3*j), projection='3d')
        ax3d.scatter(xcs[:,t_sel], ycs[:,t_sel], zcs[:,t_sel],
                     c=kclust.labels_)
plt.tight_layout()

