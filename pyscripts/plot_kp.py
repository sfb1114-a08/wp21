#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  4 12:26:47 2023

@author: schoelleh96
"""
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.lines import Line2D
from matplotlib.legend import Legend
Line2D._us_dashSeq    = property(lambda self: self._dash_pattern[1])
Line2D._us_dashOffset = property(lambda self: self._dash_pattern[0])
Legend._ncol = property(lambda self: self._ncols)
import tikzplotlib
from matplotlib.ticker import MaxNLocator, FormatStrFormatter

base_font_size = 10 # pt

plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.sans-serif'] = ['Helvetica']
plt.rcParams['font.size'] = base_font_size
plt.rcParams['text.latex.preamble'] = r'\usepackage{amsmath,amsfonts,amssymb}'

YEAR=2016

if YEAR==2017:
    skiprows = 28
    nrows = 57
else:
    skiprows = 4
    nrows = 24

def read_and_prepare_data(fpath):
    cols = pd.read_csv(
        fpath, sep='\s*\|\s*', skiprows=2, nrows=1,
        header=None, engine='python'
    ).iloc[0].str.strip().tolist()
    cols = [col for col in cols if col != '|']

    df = pd.read_csv(
        fpath, sep='\s*\|\s*', skiprows=skiprows, nrows=nrows,
        header=None, engine='python'
    )
    df.columns = cols
    df = df.drop(df.columns[[0, -1]], axis=1)
    df['date'] = pd.to_datetime(df['date'], format='%Y%m%d_%H')

    return df

fpath1 = "/home/schoelleh96/Nextcloud/ObsidianVault/Work/Projects/n_t.md"
fpath2 = "/home/schoelleh96/Nextcloud/ObsidianVault/Work/Projects/kp.md"

df1 = read_and_prepare_data(fpath1)
df2 = read_and_prepare_data(fpath2)

fig, ax1 = plt.subplots(figsize=(3, 2))

color = 'tab:blue'
ax1.set_xlabel('Date')
ax1.set_ylabel('$m$', color=color)
ax1.plot(df1['date'], df1['n_t'], color=color,
         marker='s', linestyle='-', label='n_t')
ax1.tick_params(axis='y', labelcolor=color)
ax1.xaxis.set_major_formatter(mdates.DateFormatter('%m-%d'))
locator = mdates.AutoDateLocator(minticks=5, maxticks=6)  # Adjust maxticks as needed
ax1.xaxis.set_major_locator(locator)


ax2 = ax1.twinx()
color = 'tab:red'
ax2.set_ylabel('$k$ [km/hPa]', color=color)
ax2.plot(df2['date'], df2['k_p'], color=color,
         marker='o', linestyle='--', label='k')
ax2.tick_params(axis='y', labelcolor=color)
# Ensure ticks are at integer values and labels are formatted as integers
ax2.yaxis.set_major_locator(MaxNLocator(nbins='auto', integer=True))
ax2.yaxis.set_major_formatter(FormatStrFormatter('%d'))

fig.tight_layout()
plt.show()

plt.savefig("/net/scratch/schoelleh96/WP2/WP2.1/" +
            "LAGRANTO/wp21/im/kpm/kpm" + str(YEAR) + ".pdf",
            bbox_inches="tight")


# tikzplotlib.save("/net/scratch/schoelleh96/WP2/WP2.1/" +
#                  "LAGRANTO/wp21/im/kpm/kpm" + str(YEAR) + ".tikz")
