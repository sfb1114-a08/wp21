#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  5 11:56:55 2023

@author: henry
"""

# %% library imports

# This will move the console to the right working directory.
from os.path import  dirname, abspath
from os import chdir
chdir(dirname(abspath(__file__)))

from sys import argv, path
import numpy as np
path.append("./LIB")
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import pandas as pd
import seaborn as sb
from sklearn.neighbors import BallTree
import pickle

# %% Initials

if argv[0]=='':
    date_0 = "20160501_06"
    nn = 100
    trapath = ("/net/scratch/schoelleh96/WP2/WP2.1/" +
                "LAGRANTO/wp21/era5/traj/2016/")
else:
    trapath = argv[1]
    date_0 = argv[2]
    from matplotlib import use
    use('agg')
    nn = int(argv[3])
# only every nth trajectory
n = 1

# cut-off distance
r = 1e5

# %% functions

def calc_Dists(trapath, date_0):

    trajs = np.load(trapath + "traj_" + date_0 + ".npy")

    Lon = trajs['lon'][::n]
    Lat = trajs['lat'][::n]
    P = trajs['p'][::n]
    U = trajs['U'][::n]
    V = trajs['V'][::n]
    Omg = trajs['OMEGA'][::n]
    T = trajs['T'][::n]

    # k_p = cc.calc_k(U/1000, V/1000, Omg/100)
    k_p=15

    Dist_list = list()

    for t in range(Lon.shape[1]):
        print(t)

        dd = np.array([np.deg2rad(Lon[:,t]), np.deg2rad(Lat[:,t])]).T
        BT = BallTree(dd, metric='haversine')
        idx, hdist = BT.query_radius(dd, r=r / 6371, return_distance=True)
        hdist = hdist * 6371

        Dists = np.zeros((Lon.shape[0], nn))

        for i in range(Lon.shape[0]):

            vdist = P[:,t][idx[i]] - P[:,t][i]

            dist = np.sqrt(np.power(hdist[i], 2) + k_p * np.power(vdist, 2))

            # remove zeros
            dist = dist[dist != 0]

            lowest_n = np.partition(dist, nn)[:nn]

            Dists[i, :] = lowest_n

        Dist_list.append(pd.DataFrame({'t' : t,
                                         'date' : date_0,
                                         'dist_mean' : Dists.mean(),
                                         'dist_sd' : Dists.std()},
                                        index = [t]))
    Dists = pd.concat(Dist_list, ignore_index=True)
    return Dists

# Dist_list = list()
# for date in dates:
#     with open(date.strftime('%Y%m%d_%H') + "nn" + str(nn) + ".pkl", 'rb') as f:
#         Dist = pickle.load(f)
#     Dist_list.append(Dist)
# Dists = pd.concat(Dist_list, ignore_index=True)


def plot_Dist(Dists, name):

    if name=="all":
        # pal = sb.cubehelix_palette(10, rot=-.25, light=.7)

        # Create a FacetGrid
        g = sb.FacetGrid(Dists, col="date", hue="date", col_wrap=4,
                         aspect=15, height=.5)

        # Map the lineplot with shading
        def plot_mean_and_sd(*args, **kwargs):
            data = kwargs.pop('data')
            ax = plt.gca()
            line = sb.lineplot(x='t', y='dist_mean', data=data, ax=ax, **kwargs)
            color = line.get_lines()[0].get_color()
            ax.fill_between(data['t'], data['dist_mean'] - data['dist_sd'],
                            data['dist_mean'] + data['dist_sd'], alpha=0.3,
                            color=color)

        g.map_dataframe(plot_mean_and_sd)

        def label(x, color, label):
            ax = plt.gca()
            ax.text(.5, .8, label, fontweight="bold", color=color,
            ha="left", va="center", transform=ax.transAxes)

        g.map(label, "t")
        # Set the subplots to overlap
        g.figure.subplots_adjust(hspace=0)
        # Remove axes details that don't play well with overlap
        g.set_titles("")
        g.set(ylabel="")
        # g.despine(bottom=True)
        # g.set_axis_labels("t", "Mean ± SD")
        # plt.tight_layout()
        g.figure.set_size_inches(14, 10)
        plt.show()
    else:
        f, ax = plt.subplots(1, 1, figsize=(14,10))
        sb.lineplot(data=Dists, x="t", y="dist_mean", ax=ax, color="blue")
        plt.fill_between(Dists['t'], Dists['dist_mean'] - Dists['dist_sd'],
                         Dists['dist_mean'] + Dists['dist_sd'], color='blue',
                         alpha=0.3)
    plt.savefig("../im/Dists/" + name + ".png")


def plot_Dist2(Dists, name, nn):
    f, ax = plt.subplots(1, 1, figsize=(14,10))
    if name=="all":
        sb.set_palette("viridis", n_colors=len(Dists['date'].unique()))
        sb.lineplot(data=Dists, x="t", y="dist_mean", hue="date", ax=ax)
        # Modify the legend to show only every 4th entry
        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles[3::4], labels[3::4], title="Date")
    else:
        f, ax = plt.subplots(1, 1, figsize=(14,10))
        sb.lineplot(data=Dists, x="t", y="dist_mean", ax=ax, color="blue")
        plt.fill_between(Dists['t'], Dists['dist_mean'] - Dists['dist_sd'],
                         Dists['dist_mean'] + Dists['dist_sd'], color='blue',
                         alpha=0.3)
    ax.set_title(str(nn) + "nearest neighbors")
    plt.savefig("../im/Dists/" + name + "nn" + str(nn) + ".png")

def plot_all_dists_3d(Dists, name, surf=False):
    fig = plt.figure()
    if surf:
        ax = fig.add_subplot(111, projection='3d')

        # Generate a colormap based on the number of unique categories
        dates = Dists['date'].unique()
        colors = plt.cm.viridis(np.linspace(0, 1, len(dates)))
        for idx, date in enumerate(dates):
            subset = Dists[Dists['date'] == date]
            ax.plot(subset['t'], [idx]*len(subset), subset['dist_mean'],
                    label=date, color=colors[idx])
            ax.plot(subset['t'], [idx]*len(subset),
                    subset['dist_mean'] - subset['dist_sd'],
                    label=date, color=colors[idx], alpha=0.3)
            ax.plot(subset['t'], [idx]*len(subset),
                    subset['dist_mean'] + subset['dist_sd'],
                    label=date, color=colors[idx], alpha=0.3)
        ax.set_xlabel("t")
        ax.set_ylabel("date")
        ax.set_zlabel("distance")
        ax.set_yticks(np.arange(len(dates)))
        ax.set_yticklabels(dates)
    else:
        ax = fig.add_subplot(111)

        def draw_contours(ax, data, levels):
            nrows, ncols = data.shape
            for i in range(nrows):
                for j in range(ncols):
                    value = data.iloc[i, j]
                    # Check vertical contours
                    if j < ncols - 1:
                        next_value = data.iloc[i, j+1]
                        for level in levels:
                            if ((value < level and next_value >= level) or
                                (value >= level and next_value < level)):
                                ax.plot([j+1, j+1], [i, i+1], color='k')
                    # Check horizontal contours
                    if i < nrows - 1:
                        next_value = data.iloc[i+1, j]
                        for level in levels:
                            if ((value < level and next_value >= level) or
                                (value >= level and next_value < level)):
                                ax.plot([j, j+1], [i+1, i+1], color='k')

        # Create the heatmap
        data = Dists.pivot(index="date", columns="t",
                                 values="dist_mean")
        cax = sb.heatmap(data, annot=False, cmap="YlGnBu", ax=ax)
        dates = data.index.tolist()
        ax.set_yticks(np.arange(3.5, len(dates), 4))
        ax.set_yticklabels(dates[3::4])
        # Overlay contours
        contour_levels = cax.collections[0].colorbar.get_ticks()
        draw_contours(ax, data, contour_levels)

    plt.savefig("../im/Dists/3d" + name + str(surf) + "nn" +
                str(nn) + ".png")

# %% execution

if date_0 == "all":
    dates = [datetime(2016, 4, 29, 6) + timedelta(hours=i)
              for i in range(0, 144, 6)]
    Dist_list = list()
    for date in dates:
        Dist = calc_Dists(trapath, date.strftime('%Y%m%d_%H'))
        Dist_list.append(Dist)
    Dists = pd.concat(Dist_list, ignore_index=True)
else:
    Dists = calc_Dists(trapath, date_0)

with open(date_0 + "kp15nn" + str(nn) + ".pkl", 'wb') as f:
    pickle.dump(Dists, f)

plot_Dist2(Dists, date_0, nn)



