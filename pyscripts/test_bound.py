#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  3 12:31:58 2023

@author: schoelleh96
"""

# %% library imports

# This will move the console to the right working directory.
from os.path import  dirname, abspath
from os import chdir, getcwd
chdir(dirname(abspath(__file__)))

from LIB import calc as cc
from LIB import plot as pp
from matplotlib import pyplot as plt
import numpy as np
from lagranto import Tra
from datetime import datetime
from alphashape import optimizealpha, alphashape
from matplotlib import use
import open3d as o3d
from scipy.spatial import ConvexHull

# %% get the data

date_0 = datetime(2016,5,1,6)
tradir = date_0.strftime("/net/scratch/schoelleh96/WP2/WP2.1/" +
                         "LAGRANTO/wp21/era5/traj/%Y/")
print("cwd: ", getcwd())
print("tradir:", tradir)
print("Date: ", date_0)

traname = date_0.strftime('traj_%Y%m%d_%H')
trajs = Tra()
trajs.load_ascii(tradir + traname, gz=False)
trajs.set_array(trajs.get_array().transpose())
print(trajs)
# use('agg')

# %%

Lon = trajs['lon']
Lat = trajs['lat']
P = trajs['p']

t_i = 30

LonN = cc.norm(Lon[::, t_i])
LatN = cc.norm(Lat[::, t_i])
PN = cc.norm(P[::, t_i])

points = list(map(tuple, np.column_stack((LatN, LonN, PN))))

alpha_shape = alphashape(points, alpha=5)

bound_arr = alpha_shape.vertices.__array__()
bound = list(map(tuple, np.column_stack((bound_arr[:,0],
                                      bound_arr[:,1],
                                      bound_arr[:,2]))))
boundary = np.asarray([p in bound for p in points])

in_out = alpha_shape.contains(points)

fig = plt.figure()
fig.canvas.manager.full_screen_toggle()
ax = fig.add_subplot(111, projection='3d')
ax.plot_trisurf(alpha_shape.vertices[:, 0], alpha_shape.vertices[:,1],
                triangles=alpha_shape.faces, Z=alpha_shape.vertices[:,2])
ax.scatter(*zip(*points), c=boundary)
#ax.scatter(*zip(*points), c=in_out, marker="^")
plt.show()

out_no_bound = np.isin(np.where(in_out==False),
                       np.where(boundary==False)).sum()
print("Outside but no boundary: ", out_no_bound)

# %% prepare data

Lon = trajs['lon']
Lat = trajs['lat']
P = trajs['p']

alphas = np.empty((Lon.shape[1], 1))

for t_i in np.arange(Lon.shape[1]):

    # Normalization

    LonN = cc.norm(Lon[:, t_i])
    LatN = cc.norm(Lat[:, t_i])
    PN = cc.norm(P[:, t_i])

    # stereographic projection

    X, Y = cc.project(Lat[:,t_i], Lon[:,t_i])
    H = cc.ptoh(P[:,t_i], 1013.25, 8.435)

    XN = cc.norm(X)
    YN = cc.norm(Y)
    HN = cc.norm(H)

# %% test boundary detection

    points = list(map(tuple, np.column_stack((LatN, LonN, PN))))
    alpha = optimizealpha(points, max_iterations=1000)

    alphas[t_i] = alpha

    print(alpha)

bounds = cc.get_bounds(LonN, LatN, PN, ini=False)

# %% test open3d

bunny = o3d.data.BunnyMesh()
mesh = o3d.io.read_triangle_mesh(bunny.path)
mesh.compute_vertex_normals()

pcd = mesh.sample_points_poisson_disk(750)
o3d.visualization.draw_geometries([pcd])
alpha = 0.03
print(f"alpha={alpha:.3f}")
mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_alpha_shape(pcd, alpha)
mesh.compute_vertex_normals()
o3d.visualization.draw_geometries([mesh], mesh_show_back_face=True)


# %%

points = list(map(tuple, np.column_stack((X, Y, H))))

# pcd = o3d.geometry.PointCloud()
# pcd.points = o3d.utility.Vector3dVector(points)

# alpha = optimizealpha(points)
# bounds = cc.get_bounds(X, Y, H, alpha=0, ini=False)

hull = ConvexHull(points)

# %%

f = plt.figure()
ax3d = f.add_axes([0, 0, 1, .5], projection="3d")
ax3d2 = f.add_axes([0, .5, 1, .5], projection="3d")

azim = 150
elev = 45
dist = 7

pp.plotxyz3d(ax3d, Lon[:, t_i], Lat[:, t_i], P[:, t_i], azim, elev,
             dist, c=bounds)

pp.plotxyz3d(ax3d2, X, Y, H, azim, elev, dist, c=bounds)


# %%

for t_i in np.arange(10,Lon.shape[1]):
    X, Y, Z = cc.coord_trans(Lon[:,t_i], Lat[:,t_i], P[:,t_i],
                             v_trans="std_atm", scaling=100)
    points = list(map(tuple, np.column_stack((X, Y, P[:, t_i]))))
    #alpha = optimizealpha(points)
    #print(alpha)
    bounds = cc.get_bounds(X, Y, Z,
                           alpha=0, ini=False)


    # %% plot

    f = plt.figure()
    ax3d = f.add_axes([0, 0, 1, .5], projection="3d")
    ax3d2 = f.add_axes([0, .5, 1, .5], projection="3d")

    azim = 150
    elev = 45
    dist = 7

    pp.plotxyz3d(ax3d, Lon[:,t_i], Lat[:,t_i], P[:,t_i], azim, elev,
                 dist, c=bounds)

    pp.plotxyz3d(ax3d2, X, Y, Z, azim, elev, dist, c=bounds)

    plt.savefig(date_0.strftime('../im/bounds/%Y%m%d_%H') + t_i)

pp.plot_clustering(ax3d, X, Y, Z, azim, elev, dist, t_i, c = bounds,
                   plot_0=False)

# %%

for r in np.arange(10,5, -1):
    X, Y, Z = cc.coord_trans(Lon[::r,t_i], Lat[::r,t_i], P[::r,t_i],
                             v_trans="std_atm", scaling=1)
    points = list(map(tuple, np.column_stack((X, Y, P[::r, t_i]))))
    alpha = optimizealpha(points)
    print(alpha)
    bounds = cc.get_bounds(Lon[::r,t_i], Lat[::r,t_i], P[::r,t_i],
                           alpha=alpha, ini=False)


alphas = np.empty((Lon.shape[1], 1))

for t_i in range(Lon.shape[1]):

for t_i in range(5):

    X, Y, Z = cc.coord_trans(Lon[:,t_i], Lat[:,t_i], P[:,t_i],
                             v_trans="std_atm", scaling=1)

    points = list(map(tuple, np.column_stack((X, Y, P))))
    alpha = optimizealpha(points)
    bounds = cc.get_bounds(Lon[:,t_i], Lat[:,t_i], P[:,t_i], alpha=alpha,
                           ini=False)
    alphas[t_i] = alpha



# %% plot

f = plt.figure()
ax3d = f.add_axes([0, 0, 1, .5], projection="3d")
ax3d2 = f.add_axes([0, .5, 1, .5], projection="3d")

azim = 150
elev = 45
dist = 7

pp.plotxyz3d(ax3d, Lon[::r,t_i], Lat[::r,t_i], P[::r,t_i], azim, elev,
             dist, c=bounds)

pp.plotxyz3d(ax3d2, X, Y, Z, azim, elev, dist, c=bounds)

pp.plot_clustering(ax3d, X, Y, Z, azim, elev, dist, t_i, c = bounds,
                   plot_0=False)

