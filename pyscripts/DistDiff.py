#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 15:08:31 2023

@author: schoelleh96
"""

# Calculate and visualize the difference between between great-circle distance
# and distance in NorthPolar Stereographic Projection

# %% library imports

# This will move the console to the right working directory.
from os.path import  dirname, abspath, exists
from os import chdir, makedirs
chdir(dirname(abspath(__file__)))

import pandas as pd
import seaborn as sb
from sys import argv, path
import numpy as np
path.append("../wp21/pyscripts/LIB")
import plot as pp
import calc as cc
import data as dd
import matplotlib.pyplot as plt
from datetime import datetime
from sklearn.neighbors import BallTree
import matplotlib as mpl
# %% Get the data

date_0 = datetime(2016,5,1,6)
path = '../../csstandalone/'
fname = path + "traj" + date_0.strftime('_%Y%m%d_%H') + ".npy"
trajs = np.load(fname)
t_sel = np.arange(0,trajs.shape[1])
n = 10
path = path + date_0.strftime("dat_%Y%m%d_%H") + "/" + str(n) + "/"

lon = trajs['lon'][::n, t_sel]
lat = trajs['lat'][::n, t_sel]
p = trajs['p'][::n, t_sel]

x, y, z = cc.coord_trans(lon, lat, p, "None", proj="stereo",
                               scaling = 1)

Dist_list = list()

for t in t_sel:

    dd = np.array([np.deg2rad(lon[:,t-t_sel[0]]),
                   np.deg2rad(lat[:,t-t_sel[0]])]).T
    BT = BallTree(dd, metric='haversine')
    idx, hdist = BT.query_radius(dd, r=3000 / 6371, return_distance=True)
    hdist = hdist * 6371

    xx = list()
    yy = list()
    v = list()
    for i in range(lon.shape[0]):
        # Save only one triangle of symmetric matrix
        hdist[i] = hdist[i][idx[i] > i]
        idx[i] = idx[i][idx[i] > i]
        valid = np.where(hdist[i] < 3000)[0]
        xx.extend([i for _ in range(len(valid))])
        yy.extend(idx[i][valid])
        v.extend(hdist[i][valid])

    xx = np.asarray(xx)
    yy = np.asarray(yy)
    v = np.asarray(v)

    stereo_dist = np.sqrt((x[xx, t] - x[yy, t])**2 +
                          (y[xx, t] - y[yy, t])**2)
    Dist_list.append(pd.DataFrame({'t': t,
                                   'Haversine': v,
                                   'Stereographic': stereo_dist,
                                   'x_i': xx,
                                   'lon_x': lon[xx, t],
                                   'lat_x': lat[xx, t],
                                   'y_i': yy,
                                   'lon_y': lon[yy, t],
                                   'lat_y': lat[yy, t],
                                   }))
Dists = pd.concat(Dist_list, ignore_index=True)
Dists['Diff'] = -(Dists['Haversine']-Dists['Stereographic'])#/Dists['haversine']
Dists['mean_lat'] = (Dists['lat_x'] + Dists['lat_y'])/2
Dists['Haversine bins'] = pd.cut(
    Dists['Haversine'],bins=pd.Series(
        np.array([0, 500, 1000, 1500, 2000, 2500,3000]),
        index=np.array([0, 500, 1000, 1500, 2000, 2500, 3000]).astype(str)),
    labels=None)

# %% Plot

fig, ax = plt.subplots(2, 2)
ax = ax.flatten()
# sb.histplot(x="dist", data=Dists, ax=ax[0])
# sb.histplot(x="dist_s", data=Dists, ax=ax[1])

sb.histplot(x="Distance in km", hue="Distance Type", data=Dists.melt(
    value_vars=["haversine", "stereographic"], var_name="Distance Type",
    value_name="Distance in km"), ax=ax[0])

# sb.scatterplot(x="dist", y="dist_s", s=.5, data=Dists, color=".15",
#                legend=False)
sb.histplot(y="haversine", x="stereographic", bins=100, pthresh=.01,
            data=Dists, cmap="mako", legend=False, cbar=True,
            norm=mpl.colors.LogNorm(), vmin=None, vmax=None,
            cbar_kws=dict(shrink=.75), ax=ax[1])
ax[1].set_aspect('equal')

# sb.kdeplot(x="dist", y="dist_s", levels=10, color="w", linewidths=1,
#            data=Dists)

sb.histplot(x="haversine", y="Diff", bins=100, pthresh=.01,
            data=Dists, cmap="mako", legend=False, cbar=True,
            norm=mpl.colors.LogNorm(), vmin=None, vmax=None,
            cbar_kws=dict(shrink=.75), ax=ax[2])
ax[2].set_ylabel("Stereographic - Haversine")

sb.boxplot(x='Haversine Bins', y="Diff", data=Dists, ax=ax[3])
ax[3].set_ylabel("Stereographic - Haversine")

# %% Example points

lat, lon = np.meshgrid(np.arange(0, 90, 5), np.arange(0, 180, 5))
lat = lat.flatten()
lon = lon.flatten()
p = 500*np.ones_like(lat)

x, y, z = cc.coord_trans(lon, lat, p, "None", proj="stereo",
                               scaling = 1)

Dist_list = list()

dd = np.array([np.deg2rad(lon),
               np.deg2rad(lat)]).T
BT = BallTree(dd, metric='haversine')
idx, hdist = BT.query_radius(dd, r=10000 / 6371, return_distance=True)
hdist = hdist * 6371

xx = list()
yy = list()
v = list()
for i in range(lon.shape[0]):
    # Save only one triangle of symmetric matrix
    hdist[i] = hdist[i][idx[i] > i]
    idx[i] = idx[i][idx[i] > i]
    valid = np.where(hdist[i] < 10000)[0]
    xx.extend([i for _ in range(len(valid))])
    yy.extend(idx[i][valid])
    v.extend(hdist[i][valid])

xx = np.asarray(xx)
yy = np.asarray(yy)
v = np.asarray(v)

stereo_dist = np.sqrt((x[xx] - x[yy])**2 +
                      (y[xx] - y[yy])**2)/1e3
Dist_list.append(pd.DataFrame({'Haversine': v,
                               'Stereographic': stereo_dist,
                               'x_i': xx,
                               'lon_x': lon[xx],
                               'lat_x': lat[xx],
                               'y_i': yy,
                               'lon_y': lon[yy],
                               'lat_y': lat[yy],
                               }))
Dists = pd.concat(Dist_list, ignore_index=True)
Dists['Diff'] = -(Dists['Haversine']-Dists['Stereographic'])#/Dists['haversine']
Dists['mean_lat'] = (Dists['lat_x'] + Dists['lat_y'])/2
Dists['Haversine bins'] = pd.cut(
    Dists['Haversine'],bins=pd.Series(
        np.array([0, 500, 1000, 1500, 2000, 2500,3000]),
        index=np.array([0, 500, 1000, 1500, 2000, 2500, 3000]).astype(str)),
    labels=None)

Dists.iteritems = Dists.items
# %% Plot

fig, ax = plt.subplots(1, 1)

sb.scatterplot(x="lat_x", y="lat_y", hue="Diff", palette="mako", s=50,
               data=Dists.loc[((Dists['lon_x']==0) &
                               (Dists['lon_y']==0)), :], ax=ax,
               legend="brief")
plt.legend(title="Stereographic - Haversine")
ax.set_xlabel("Latitude x")
ax.set_ylabel("Latitude y")

DD = Dists.loc[(np.isin(Dists['lat_x'], [0, 30, 60, 80]) &
                               (Dists['lat_y']==Dists['lat_x'])), :]

norm = mpl.colors.CenteredNorm(vcenter=0, halfrange=abs(DD['Diff']).max())
cmap = sb.color_palette(palette="vlag", as_cmap=True)
sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
sm.set_array([])

f = sb.FacetGrid(DD, col="lat_x", col_wrap=2, hue="Diff", palette="vlag")

f.map_dataframe(sb.scatterplot, x="lon_x", y="lon_y")
f.set_axis_labels("Longitude x", "Longitude y")
f.fig.subplots_adjust(right=0.9)
cax = f.fig.add_axes([0.9,0.1,0.05,0.8])
f.fig.colorbar(sm, cax=cax, label="Stereographic - Haversine", shrink=0.75,
               aspect=50)
