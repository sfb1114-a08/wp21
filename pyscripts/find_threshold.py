#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 16 10:58:57 2023

@author: schoelleh96
"""

from os.path import  dirname, abspath
from os import chdir, getcwd
chdir(dirname(abspath(__file__)))

from lagranto import Tra
from datetime import datetime
from sys import argv
from LIB import calc as cc
import scipy.stats as st
from numpy import nan_to_num, linspace
from scipy.stats import gaussian_kde
from LIB import plot as pp
from matplotlib.pyplot import savefig
# %% get the data

if argv[0]=='':
    tradir = "/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/traj/1981/"
    date_0 = datetime(1981,1,9,0)
    ptype = "hist"
else:
    tradir = argv[1]
    date_0 = datetime(int(argv[2]),int(argv[3]),int(argv[4]),int(argv[5]))
    from matplotlib import use
    use('agg')
    ptype = argv[6]

print("cwd: ", getcwd())
print("tradir:", tradir)
print("Date: ", date_0)

# define filename
traname = date_0.strftime('traj_%Y%m%d_%H')

trajs = Tra()
trajs.load_ascii(tradir + traname, gz=False)
trajs.set_array(trajs.get_array().transpose())#[:,:73])#three days only
print(trajs)

X = trajs['lon'].data
Y = trajs['lat'].data
TH = trajs['TH'].data
p = trajs['p'].data

X, Y, Z = cc.coord_trans(X, Y, p, v_trans="std_atm", scaling=-1)
dom, kclust = cc.calc_cs(X, Y, Z)

# %% plotting

# if ptype == "dist":
dist_space = linspace( -15, 40, 100 )


f7 = cc.est_dth_dist(TH,168, dist_space)
f3 = cc.est_dth_dist(TH,72, dist_space)

f7a = cc.est_dth_dist(TH[(kclust.labels_ == 0),:],168, dist_space)
f3a = cc.est_dth_dist(TH[(kclust.labels_ == 0),:],72, dist_space)
f7b = cc.est_dth_dist(TH[(kclust.labels_ == 1),:],168, dist_space)
f3b = cc.est_dth_dist(TH[(kclust.labels_ == 1),:],72, dist_space)

# f, ax = pp.plot_dth_dist(f7, f3, dist_space)
f, ax = pp.plot_dth_dist(f7a, f3a, dist_space, f7b=f7b, f3b=f3b)

savefig("./im/thresh/norm" + date_0.strftime('%Y%m%d_%H') + ".png")

# elif ptype == "hist":
dth3 = cc.dth_minmax(TH, 72)
dth7 = cc.dth_minmax(TH, 168)
fig, ax1, ax2 = pp.plot_set_hist(dth7, dth3, kclust.labels_)
savefig("./im/thresh/normhist" + date_0.strftime('%Y%m%d_%H') + ".png")
