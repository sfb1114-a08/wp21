import numpy as np
import time
from sklearn.neighbors import BallTree

# The cutoff radius is currently measured in km
radius = 200
# The ratio at which 1 hPa is converted to km to contribute to the 'distance' of air particles
ratio = 10

data = np.load('traj_20160501_06.npy')
particles, timesteps = np.shape(data)

# Array of size timesteps x particles x 2 containing latitude and longitude data
lat_lon_pos = np.array((data['lat'], data['lon'])).transpose()
# Array of size timesteps x particles containing the pressure in hPa
pressure = data['p'].transpose()

# Skeleton for a sparse symmetric matrix. All three lists contain timesteps lists representing the sparse distance
# matrix for each of the timesteps. x_list[t] and y_list[t] contain the indices and v_list[t] the values. Due to
# symmetry, we always have x_list[t][i] < y_list[t][i].
x_list = [[] for _ in range(timesteps)]
y_list = [[] for _ in range(timesteps)]
v_list = [[] for _ in range(timesteps)]

start_timer = time.time()

for t in range(timesteps):
    pos_t = np.deg2rad(lat_lon_pos[t])
    p_t = pressure[t]

    BT = BallTree(pos_t, metric='haversine')
    idx, hdist = BT.query_radius(pos_t, r=radius / 6371, return_distance=True)
    hdist = hdist * 6371

    for i in range(particles):
        hdist[i] = hdist[i][idx[i] > i]
        idx[i] = idx[i][idx[i] > i]

        pdist = p_t[idx[i]] - p_t[i]

        dist = np.sqrt(np.power(hdist[i], 2) + ratio * np.power(pdist, 2))

        valid = np.where(dist < radius)[0]
        x_list[t].extend([i for _ in range(len(valid))])
        y_list[t].extend(idx[i][valid])
        v_list[t].extend(dist[valid])

end_timer = time.time()
elapsed_time = end_timer - start_timer
print('Time to compute distances: %s seconds' % elapsed_time)
