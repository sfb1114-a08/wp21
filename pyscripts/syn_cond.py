#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Script for data analysis and visualization.
@author: schoelleh96
Created on Fri Jul 14 18:29:49 2023
"""

# Import standard libraries
from datetime import datetime, timedelta
from os import chdir
from os.path import dirname, abspath
from sys import path
import locale

# Change working directory to script location
chdir(dirname(abspath(__file__)))
locale.setlocale(locale.LC_TIME, 'en_US')
# %%
# Import third-party libraries
import pandas as pd
import xarray as xr
import numpy as np
import scipy as sc
import matplotlib.pyplot as plt
import matplotlib as mpl
import cartopy.crs as ccrs
import cartopy.util as cutil
from cdo import Cdo
from matplotlib.colors import Normalize
import matplotlib.patches as mpatches

# Import local libraries
path.append("./LIB")
import data as dd

# Set up matplotlib
# mpl.rc('font',**{'family':'serif','serif':['cmr10']})
# mpl.rc('text', usetex=True)

base_font_size = 10  # pt

plt.rcParams['text.usetex'] = True
plt.rcParams['pgf.texsystem'] = "pdflatex"
plt.rcParams['text.latex.preamble'] = (
    r'\usepackage{amsmath,amsfonts,amssymb,cmbright,standalone}')

# Define constants
YEAR = "2017"
multiple_subplots = False
var = "pv"
vecCol = "b"
plev = "surf"
levels = "150,200,250,300,350,400,450,500"

# Initialize dates
if YEAR == "2016":
    # dates = [datetime(2016, 4, 30, 0), datetime(2016, 5, 2, 0),
    #          datetime(2016, 5, 4, 0)]
    dates = [datetime(2016, 4, 29, 6) + timedelta(hours=i)
             for i in range(0, 144, 6)]
    # dates = [datetime(2016,5,6,0), datetime(2016,5,7,0)]
    # dates = pd.date_range(start='2016-04-29-06', end='2016-05-05-00',
    #                       freq='6H')
    extent = [-210, -30, 30, 90]
    central_longitude = -120
    p_levels = np.arange(980, 1005, 5)
elif YEAR == "2017":
    # dates = [datetime(2017, 1, 25, 0) + timedelta(hours=i)
    #          for i in range(0, 3*48, 48)]
    dates = [datetime(2017, 1, 23, 12) + timedelta(hours=i)
              for i in range(0, 29*6, 6)]
    extent = [-60, 120, 30, 90]
    central_longitude = 30
    p_levels = np.arange(970, 995, 5)

data_path = ("/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/" +
             "era5/")

era5_path = "/daten/erafive/arch/data/dkrz-mirror/ml00_1H/" + YEAR

cdo = Cdo()

# %%


def process_datasets(dates, data_path, cdo):
    """
    Process datasets for the given dates and data path.

    Parameters
    ----------
    dates (list): List of dates to process datasets for.
    data_path (str): Path to the data.
    cdo (Cdo): Cdo object for processing datasets.

    Returns
    -------
    xarray.Dataset: Concatenated dataset along the time dimension.
    """

    def filterW(field, n):
        """
        Apply a convolution filter to an xarray DataArray using.

        Parameters
        ----------
        - field (xarray.DataArray): 2D data to filter.
        - n (int): Number of times to apply the filter.

        Returns
        -------
        - xarray.DataArray: Filtered data.
        """
        weights = np.array([[0, 0, 1, 0, 0],
                            [0, 2, 4, 2, 0],
                            [1, 4, 8, 4, 1],
                            [0, 2, 4, 2, 0],
                            [0, 0, 1, 0, 0]])
        weights = weights / np.sum(weights)  # Ensure normalization of weights.

        # Define a wrapper function for scipy's ndimage.convolve
        # that we can apply directly to the xarray DataArray.
        def convolve(array):
            return sc.ndimage.convolve(array, weights, mode='wrap')

        # Apply the convolution n times.
        filtered_field = field.copy()

        for _ in range(n):
            filtered_field = xr.apply_ufunc(
                convolve,  # function to apply
                filtered_field,  # input DataArray
                input_core_dims=[['latitude', 'longitude']],
                output_core_dims=[['latitude', 'longitude']],
                vectorize=True,  # automatically vectorize
                dask="parallelized",  # enable parallelized computation
                output_dtypes=[field.dtype]  # specify output data type
            )

        return filtered_field

    def calculate_gradients(data, dim_name, spacing):
        # Get the dimension index for the specified dimension name
        axis = data.dims.index(dim_name)

        # Calculate gradient using numpy.gradient for second-order accuracy
        grad = np.gradient(data.values, axis=axis, edge_order=2) / spacing

        # Convert the numpy array back to xarray DataArray,
        # preserving coords, dims, and attrs
        grad_da = xr.DataArray(grad, dims=data.dims, coords=data.coords,
                               attrs=data.attrs)

        return grad_da

    def calc_Wk(data):
        g = 9.80665  # in m/s^2
        R = 6370*1000.  # in m

        n = 0  # Number of filter applications

        # Apply the filter
        u_filtered = filterW(data['u'], n)  # in m/s
        v_filtered = filterW(data['v'], n)

        # Geopotential height
        z_g = data['z'] / g  # in m

        lat = data['latitude']  # in °
        lon = data['longitude']

        # Calculate distances for gradients, considering the shape
        dx = (2 * np.pi * R * np.cos(np.deg2rad(lat)) *
              np.abs(lon[1] - lon[0]) / 360).values  # also in m
        dx = np.tile(dx[:, np.newaxis], (1, lon.shape[0]))
        dy = (2 * np.pi * R * np.abs(lat[1] - lat[0]) / 360).values

        # Now calculate gradients with correctly broadcasted dx and dy
        dudx = calculate_gradients(u_filtered.isel(time=0), 'longitude',
                                   dx)
        dudy = -calculate_gradients(u_filtered.isel(time=0), 'latitude',
                                    dy)
        dvdx = calculate_gradients(v_filtered.isel(time=0), 'longitude',
                                   dx)
        dvdy = -calculate_gradients(v_filtered.isel(time=0), 'latitude',
                                    dy)

        # Calculate vorticity and other quantities
        vort = dvdx - dudy
        divh = dudx + dvdy
        def1 = dudx - dvdy
        def2 = dudy + dvdx
        strain = np.sqrt(divh**2 + def1**2 + def2**2)
        Wk = vort / strain
        C2 = xr.where(np.abs(Wk) > 1, vort, 0)

        data['dx'] = xr.DataArray(dx.T[np.newaxis, :], dims=data.dims,
                                  coords=data.coords, attrs=data.attrs)
        data['dy'] = xr.DataArray(dy*np.ones((dx.T[np.newaxis, :]).shape),
                                  dims=data.dims, coords=data.coords,
                                  attrs=data.attrs)
        data['dudx'] = dudx
        data['dudy'] = dudy
        data['dvdx'] = dvdx
        data['dvdy'] = dvdy
        data['divh'] = divh
        data['def1'] = def1
        data['def2'] = def2
        data['strain'] = strain
        data['z_g'] = z_g
        data['C2'] = C2
        data['Wk'] = Wk
        data['vort'] = vort
        data['u_f'] = u_filtered
        data['v_f'] = v_filtered
        return data

    datasets = []
    for date in dates:
        # Retrieve and process pv data
        # dd.retrieve_dat(date, data_path, level="upper")
        ds = cdo.vertmean(
            input=cdo.sellevel(
                levels, input=cdo.selhour(
                    date.strftime('%H'), input=(
                        data_path + date.strftime('plevdat/%Y/%m-%d.nc')),
                    returnXDataset=True), returnXDataset=True),
            returnXDataset=['pv', 'u', 'v', 'z', 'w'])

        # TODO: turn cdo into xarray
        # # Open the dataset
        # ds = xr.open_dataset(data_path+date.strftime('plevdat/%Y/%m-%d.nc'))
        # ds.mean(dim="level")
        # # Select the specific hour
        # ds_hour = ds.sel(time=ds.time.dt.hour == date.strftime('%H'))
        # ds_hour.mean(dim="level")
        # # Select specific levels
        # ds_levels = ds_hour.sel(level=levels)
        # ds_levels.mean(dim="level")
        # # Compute the vertical mean
        # ds_vertmean = ds_levels.mean(dim='level')
        # ds = xr.open_dataset(data_path+date.strftime('plevdat/%Y/%m-%d.nc'))
        # ds_selected = ds.sel(
        # time=ds.time.dt.hour == date.strftime('%H')).sel(level=levels)
        # dss = ds_selected.mean(dim='level')

        # calculate W_k
        # ds = calc_Wk(ds)

        # Retrieve and process wind data
        # dd.retrieve_dat(date, data_path, level="surf")
        psl = xr.open_dataarray(data_path +
                                date.strftime('surfdat/%Y/%m-%d-%H.nc'))
        block = xr.open_dataarray(data_path +
                                  date.strftime('../Blocks/%Y.nc'))

        # Append processed data to the datasets list
        ds['psl'] = psl
        ds['block'] = block
        # velocity
        ds['abs_u'] = np.sqrt(ds.u**2 + ds.v**2)
        ds['u'] = ds.u.where(ds.abs_u > 30)
        ds['v'] = ds.v.where(ds.abs_u > 30)
        datasets.append(ds)

    return xr.concat(datasets, dim="time")


def adjust_dataset_attributes(dataset):
    """
    Adjust the attributes and scale of the data in the given dataset.

    Parameters
    ----------
    dataset (xarray.Dataset): The dataset to adjust.

    Returns
    -------
    xarray.Dataset: The adjusted dataset.
    """
    dataset.pv.attrs["units"] = "pvu"
    dataset.pv.data = dataset.pv.data * 1e6
    dataset.psl.attrs["units"] = "hPa"
    dataset.psl.data = dataset.psl.data * 1e-2
    return dataset


# Process datasets
DS = process_datasets(dates, data_path, cdo)

# Adjust dataset attributes
DS = adjust_dataset_attributes(DS)

# %%


def initialize_projection():
    """
    Initialize a stereographic projection for plotting.

    Returns
    -------
    cartopy.crs.Stereographic: The initialized projection.
    """
    return ccrs.Stereographic(central_latitude=90.0, true_scale_latitude=50.0,
                              central_longitude=central_longitude)


# Define the PiecewiseNorm class for piecewise normalization of colors
class PiecewiseNorm(Normalize):
    def __init__(self, levels, clip=False):
        self._levels = np.sort(levels)
        self._normed = np.linspace(0, 1, len(levels))
        Normalize.__init__(self, None, None, clip)

    def __call__(self, value, clip=None):
        return np.ma.masked_array(np.interp(value, self._levels, self._normed))


def create_cmap_and_norm(var="pv"):
    if var == "pv":
        color_dict = dict()
        for c in ['red', 'green', 'blue']:
            color_dict[c] = [(0., 1., 1.), (0.1, 1., 1.), (0.4, 0.85, 0.85),
                             (1., 0., 0.)]
        levels = [0, 0.5, 1, 1.5, 2, 3, 4, 5, 6, 7, 8, 10]

    elif var == "Wk":
        color_dict = {
            'red': [
                (0.0, 0.0, 0.0),  # Start with blue
                (0.45, 1.0, 1.0),  # Transition to white before -1
                (0.5, 1.0, 1.0),  # White at -1
                (0.55, 1.0, 1.0),  # Stay white just after -1
                (1.0, 1.0, 1.0),  # End with red
            ],
            'green': [
                (0.0, 0.0, 0.0),  # Blue has no green
                (0.45, 1.0, 1.0),  # Transition to white before -1
                (0.5, 1.0, 1.0),  # White at -1
                (0.55, 1.0, 1.0),  # Stay white just after -1
                (1.0, 0.0, 0.0),  # Red has no green
            ],
            'blue': [
                (0.0, 1.0, 1.0),  # Start with blue
                (0.45, 1.0, 1.0),  # Transition to white before -1
                (0.5, 1.0, 1.0),  # White at -1
                (0.55, 1.0, 1.0),  # Stay white just after -1
                (1.0, 0.0, 0.0),  # End with no blue
            ]
        }
        levels = np.linspace(-6, 6, 13)

    custom_cmap = mpl.colors.LinearSegmentedColormap('Custom cmap', color_dict)
    norm = PiecewiseNorm(levels)
    return custom_cmap, norm, levels


def add_cyclic_points(dataset):
    """
    Add cyclic points to the given dataset for plotting.

    Parameters
    ----------
    dataset (xarray.Dataset): The dataset to add cyclic points to.

    Returns
    -------
    xarray.Dataset: The dataset with cyclic points added.
    """
    def cyclic_wrapper(x, dim="longitude"):
        wrap_data, wrap_lon = cutil.add_cyclic_point(
            x.values,
            coord=x.coords[dim].data,
            axis=x.dims.index(dim)
        )
        return xr.DataArray(
            wrap_data,
            coords={dim: wrap_lon, **x.drop(dim).coords},
            dims=x.dims
        )
    return dataset.map(cyclic_wrapper, keep_attrs=True)


def plot_data(dataset, custom_cmap, norm, levels, projection,
              multiple_subplots=True, var="pv"):
    """
    Plot the data from the given dataset with specified settings.

    Parameters
    ----------
    dataset (xarray.Dataset): The dataset containing the data to plot.
    custom_cmap (matplotlib.colors.Colormap): The custom colormap for plotting.
    norm (matplotlib.colors.Normalize): The normalization for color mapping.
    levels (list): The levels for contour plotting.
    projection (cartopy.crs.Projection): The projection for plotting.
    multiple_subplots (bool): Whether to create multiple subplots or individual
    plots for each time instance.

    Returns
    -------
    plots[Plots]: A Plots object with figures, axes and contourSets.
    """
    class Plots:
        def __init__(self):
            self.figs = []
            self.cs = []
            self.ax = []

        def append(self, fig, ax, cs):
            self.figs.append(fig)
            self.ax.append(ax)
            self.cs.append(cs)
    plots = Plots()

    if multiple_subplots:
        # Plot pv data with contourf
        figure = dataset[var].plot.contourf(
            x="longitude", y="latitude", col="time", col_wrap=3,
            cmap=custom_cmap,
            norm=norm, levels=levels,
            subplot_kws=dict(projection=projection),
            transform=ccrs.PlateCarree(),
            cbar_kwargs=dict(
                orientation='horizontal', location='bottom',
                extend='neither',
                ticks=levels, fraction=0.1, shrink=0.5, pad=0.05
            ),
            figsize=(7, 5)
        )
        if var == "pv":
            # Add 2 pvu contour line
            for i, ax in enumerate(figure.axs.flat):
                dataset.pv.isel(time=i).plot.contour(
                    x="longitude", y="latitude", levels=[2], colors='k', ax=ax,
                    transform=ccrs.PlateCarree())

        plots.append(figure, None, None)
    else:
        for i in range(len(dataset.time)):
            fig, ax = plt.subplots(subplot_kw=dict(projection=projection),
                                   figsize=(8, 6))
            # Implementation for individual plots for each time instance
            ContourSet = dataset.isel(time=i)[var].plot.contourf(
                x="longitude", y="latitude", cmap=custom_cmap,
                norm=norm, levels=levels,
                ax=ax,
                transform=ccrs.PlateCarree(),
                cbar_kwargs=dict(
                    orientation='horizontal', location='bottom',
                    extend='neither',
                    ticks=levels, fraction=0.1, shrink=0.5, pad=0.05))

            # Add 2 pvu contour line
            if var == "pv":
                dataset.pv.isel(time=i).plot.contour(
                    x="longitude", y="latitude", levels=[2], colors='k',
                    ax=ax,
                    transform=ccrs.PlateCarree()
                )
            plots.append(fig, ax, ContourSet)

    return plots


def add_quiver_to_plot(dataset, plots, multiple_subplots=True, col="b"):
    """
    Add quiver arrows representing wind vectors to the plot.

    Parameters
    ----------
    dataset (xarray.Dataset): The dataset containing wind vector data.
    plots (Plots): The figures to add quiver arrows to.
    multiple_subplots (bool): Whether to create multiple subplots or individual
    plots for each time instance.
    """
    if multiple_subplots:
        # Loop over the axes in the FacetGrid object
        for i, ax in enumerate(plots.figs[0].axs.flat):
            quiver = dataset.isel(longitude=slice(None, None, 10),
                                  latitude=slice(None, None, 10),
                                  time=i).plot.quiver(
                x="longitude", y="latitude", u="u", v="v",
                scale=1000, color=col, ax=ax,
                transform=ccrs.PlateCarree(), add_guide=False, width=0.003
            )
        plt.quiverkey(
            quiver, X=0.1, Y=0.05, U=40,
            label="$40$ ms\\textsuperscript{-1}",
            coordinates='figure'  # , fontproperties=dict(size=12)
        )
    else:
        for i, figure in enumerate(plots.figs):
            ax = plots.ax[i]

            quiver = dataset.isel(longitude=slice(None, None, 10),
                                  latitude=slice(None, None, 10),
                                  time=i).plot.quiver(
                                      x="longitude", y="latitude",
                                      u="u", v="v", scale=1000, color=col,
                                      ax=ax, transform=ccrs.PlateCarree(),
                                      add_guide=False, width=0.003
            )
            ax.quiverkey(
                quiver, X=0.1, Y=0.1, U=40, label="$40  \\frac{m}{s}$",
                coordinates='figure'  # , fontproperties=dict(size=12)
            )


def add_pressure_contours(dataset, plots, levels=np.arange(980, 1005, 5),
                          multiple_subplots=True, plev="surf"):
    """
    Add pressure contours to plot.

    Parameters
    ----------
    dataset (xarray.Dataset): The dataset containing wind vector data.
    plots (Plots): The figures to add p contours to.
    multiple_subplots (bool): Whether to create multiple subplots or individual
    plots for each time instance.
    """
    if plev == "surf":
        if multiple_subplots:
            # Loop over the axes in the FacetGrid object
            for i, ax in enumerate(plots.figs[0].axs.flat):
                dataset.psl.isel(time=i).plot.contour(
                        x="longitude", y="latitude",
                        levels=levels, colors='y',
                        linestyles="solid", linewidths=1.5,
                        ax=ax, transform=ccrs.PlateCarree()
                    )
        else:
            for i, figure in enumerate(plots.figs):
                ax = plots.ax[i]
                dataset.psl.isel(time=i).plot.contour(
                        x="longitude", y="latitude",
                        levels=levels,
                        colors='y', ax=ax, transform=ccrs.PlateCarree()
                    )
    elif plev == "upper":
        if multiple_subplots:
            # Loop over the axes in the FacetGrid object
            for i, ax in enumerate(plots.figs[0].axs.flat):
                contour_set = dataset.z_g.isel(time=i).plot.contour(
                        x="longitude", y="latitude", colors='y',
                        ax=ax, transform=ccrs.PlateCarree()
                    )
                ax.clabel(contour_set, inline=True, fontsize=8, fmt='%1.1f')
        else:
            for i, figure in enumerate(plots.figs):
                ax = plots.ax[i]
                contour_set = dataset.z_g.isel(time=i).plot.contour(
                        x="longitude", y="latitude",
                        colors='y', ax=ax, transform=ccrs.PlateCarree()
                    )
                ax.clabel(contour_set, inline=True, fontsize=8) # , fmt='%1.1f')


def add_block_contours(dataset, plots, multiple_subplots=True):
    """
    Add block contours to plot.

    Parameters
    ----------
    dataset (xarray.Dataset): The dataset containing wind vector data.
    plots (Plots): The figures to add p contours to.
    multiple_subplots (bool): Whether to create multiple subplots or individual
    plots for each time instance.
    """
    handles = [mpatches.Patch(color=color, label=f'{label}')
               for label, color in
               zip(["Blocking", "Surface Cyclone", "Tropopause"],
                   ["magenta", "yellow", "black"])]
    if multiple_subplots:
        # Loop over the axes in the FacetGrid object
        for i, ax in enumerate(plots.figs[0].axs.flat):
            dataset.block.isel(time=i).plot.contour(
                x="longitude", y="latitude", levels=[0.5], colors='magenta',
                ax=ax, transform=ccrs.PlateCarree(), linewidths=2,
                linestyles="solid"
            )
        plots.figs[0].fig.legend(handles=handles, loc="lower right",
                                 title="Contour Lines", frameon=False)
    else:
        for i, figure in enumerate(plots.figs):
            ax = plots.ax[i]
            dataset.block.isel(time=i).plot.contour(
                x="longitude", y="latitude", levels=[0.5], colors='magenta',
                ax=ax, transform=ccrs.PlateCarree(), linewidths=3
            )
            figure.legend(handles=handles, loc="lower right",
                          title="Contour Lines", frameon=False)


def set_subplot_titles(dataset, plots, multiple_subplots=True):
    """
    Set the title for each subplot in the figure based on the time coordinate.

    Parameters
    ----------
    dataset (xarray.Dataset): The dataset containing time coordinate data.
    plots (Plots): The figures to add titles to.
    multiple_subplots (bool): Whether to create multiple subplots or individual
    plots for each time instance.
    """
    def get_prefix(index):
        return f"({chr(ord('a') + index)}) "

    if multiple_subplots:
        for i, ax in enumerate(plots.figs[0].axs.flat):
            # Generate title with prefix
            prefix = get_prefix(i)
            title = dataset.coords['time'].values[i].astype(
                'datetime64[s]').item().strftime('%d %B')  # '-%H:%M')
            full_title = f"{prefix}{title}"
            ax.set_title(full_title)
    else:
        for i, figure in enumerate(plots.figs):
            ax = plots.ax[i]
            title = dataset.coords['time'].values[i].astype(
                'datetime64[s]').item().strftime('%Y-%m-%d-%H:%M')
            full_title = f"{title}"
            ax.set_title(full_title)


def add_geographical_features(plots, multiple_subplots=True):
    """
    Add geographical features to the figure.

    Parameters
    ----------
    plots (Plots): The figure to add geographical features to.
    multiple_subplots (bool): Whether to create multiple subplots or individual
    plots for each time instance.
    """
    if multiple_subplots:
        plots.figs[0].map(lambda: plt.gca().coastlines())
        plots.figs[0].map(lambda: plt.gca().gridlines(linestyle='--',
                                                      alpha=0.5))
        plots.figs[0].map(lambda: plt.gca().set_extent(extent,
                                                       crs=ccrs.PlateCarree()))
    else:
        for ax in plots.ax:
            ax.coastlines()
            ax.gridlines(linestyle='--', alpha=0.5)
            ax.set_extent(extent, crs=ccrs.PlateCarree())


def set_circular_boundary(plots, multiple_subplots=True):
    """
    Set a circular boundary for the figure.

    Parameters
    ----------
    plots (Plots): The figure to add circular bounrary to.
    multiple_subplots (bool): Whether to create multiple subplots or individual
    plots for each time instance.
    """
    # theta = np.linspace(0, np.pi, 100)
    # center, radius = [0.5, 0.5], 0.5
    # verts = np.vstack([np.sin(theta), np.cos(theta)]).T * radius + center
    # verts = np.concatenate([verts, verts[-1:][::-1], verts[:1]])
    # semi_circle = mpl.path.Path(verts, closed=True)

    theta = np.linspace(0, 2*np.pi, 100)
    verts = np.vstack([np.sin(theta), np.cos(theta)]).T
    circle = mpl.path.Path(verts * radius + center)

    if multiple_subplots:
        plots.figs[0].map(lambda: plt.gca().set_boundary(
                circle, transform=plt.gca().transAxes))
    else:
        for ax in plots.ax:
            ax.set_boundary(circle, transform=ax.transAxes)


def save_plot(plots, filepath, multiple_subplots=True):
    """
    Save the figure as an image file to the specified filepath.

    Parameters
    ----------
    plots (Plots): The figure to add circular boundary to.
    filepath (str): The filepath to save the image file to.
    multiple_subplots (bool): Whether to create multiple subplots or individual
    plots for each time instance.
    """
    if multiple_subplots:
        plots.figs[0].fig.savefig(filepath + ".pdf", bbox_inches='tight')
    else:
        for i, figure in enumerate(plots.figs):
            figure.tight_layout()
            figure.figure.savefig(
                f"{filepath}_{i:02}.png", bbox_inches='tight', dpi=300)

# %%


# Initialize projection and color dictionary
Proj = initialize_projection()

# DS = DS.sel(latitude=DS.latitude[(DS.latitude <= 80)])
# cyclic points to the dataset
DS = add_cyclic_points(DS)

# Plot the data
custom_cmap, norm, clevels = create_cmap_and_norm(var=var)
Plots = plot_data(DS, custom_cmap, norm, clevels, Proj,
                  multiple_subplots=multiple_subplots, var=var)
# Add features to the plot(s)
add_quiver_to_plot(DS, Plots, multiple_subplots=multiple_subplots, col=vecCol)
add_pressure_contours(DS, Plots, levels=p_levels,
                      multiple_subplots=multiple_subplots,
                      plev=plev)
add_block_contours(DS, Plots, multiple_subplots=multiple_subplots)

# Set subplot titles, add geographical features, set circular boundary,
# and save the plot as an image
set_subplot_titles(DS, Plots, multiple_subplots=multiple_subplots)
add_geographical_features(Plots, multiple_subplots=multiple_subplots)
# set_circular_boundary(Plots, multiple_subplots=multiple_subplots)

save_plot(Plots, "/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/im/syn/syn"
          + var + YEAR, multiple_subplots=multiple_subplots)
