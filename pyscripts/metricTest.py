#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 22 17:53:35 2023

@author: schoelleh96
"""
# %% library imports

# This will move the console to the right working directory.
from os.path import  dirname, abspath
from os import chdir, getcwd
chdir(dirname(abspath(__file__)))

from LIB import plot as pp
from LIB import calc as cc
from LIB import data as dd
from sys import argv
from datetime import datetime
from lagranto import Tra
from matplotlib import pyplot as plt
from matplotlib.ticker import MaxNLocator
import numpy as np
import pandas as pd
from sklearn.neighbors import NearestNeighbors
from sklearn.neighbors import KDTree, BallTree

# %% get the data

if argv[0]=='':
    date_0 = datetime(2016,5,1,6)
    tradir = date_0.strftime("/net/scratch/schoelleh96/WP2/WP2.1/" +
                             "LAGRANTO/wp21/traj/%Y/")
else:
    tradir = argv[1]
    date_0 = datetime(int(argv[2]),int(argv[3]),int(argv[4]),int(argv[5]))
    from matplotlib import use
    use('agg')

print("cwd: ", getcwd())
print("tradir:", tradir)
print("Date: ", date_0)

# define filename
traname = date_0.strftime('traj_%Y%m%d_%H')

trajs = Tra()
trajs.load_ascii(tradir + traname, gz=False)
trajs.set_array(trajs.get_array().transpose())#[:,:73])#three days only
print(trajs)

# %% test

import timeit

lat = trajs['lat']
lon = trajs['lon']
p = trajs['p']
h = cc.ptoh(p, 1013.25, 8.435)
r = 1000

# compare metrics without tree

print(timeit.timeit("adj_dist([0.2, 1.1, 20], [-2.9, -0.4, 50])",
                    globals=globals()))
# 0.7645964119983546 with cosines
# 1.7185553000017535 with haversines

print(timeit.timeit("NN = NearestNeighbors(metric = adj_dist," +
                    "n_neighbors=20)", globals=globals(), number=100))
# 0.00018530699981056387 with haversines
# 0.00022836400239611976 with cosines
NN = NearestNeighbors(metric = adj_dist, n_neighbors=20)

M = np.array([np.deg2rad(lat[:,0]), np.deg2rad(lon[:,0]),
              h[:,0]]).T

print(timeit.timeit("NN.fit(M)", globals=globals(), number=100))
# 2.7964185499999985 with haversines
# 1.2841941409969877 with cosines

NN = NN.fit(M)

print(timeit.timeit("nn = NN.kneighbors(M)", globals=globals(), number=100))
# 302.31349145999957 with haversines
# 122.19788543100003 with cosines

nn = NN.kneighbors(M, n_neighbors=20)

print(timeit.timeit("nn[0][dist>r] = M.shape[0]", globals=globals(),
                    number=100))
# 0.0007623439996677916

print(timeit.timeit("nn = NN.radius_neighbors(M, radius=r)",
                    globals=globals(), number=100))
# 722.1621427 with haversines
# 289.8111515560013 with cosines

print(timeit.timeit("nn = NN.radius_neighbors(M, radius=r, sort_results=True)",
                    globals=globals(), number=100))
# 609.8767247220012 with haversines
# 290.669345039998 with cosines

nnr = NN.radius_neighbors(M, radius=r, sort_results=True)


### versus kdtree

print(timeit.timeit("Tree = KDTree(M)", globals=globals(), number=100))
# 0.042034949000026245

Tree = KDTree(M)

print(timeit.timeit("dist, idx = Tree.query(M, k=20)", globals=globals(),
                    number=100))
# 0.3783648109997557

dist, idx = Tree.query(M, k=20)

### versus balltree

print(timeit.timeit("Tree = BallTree(M)", globals=globals(), number=100))
# 0.016652390000217565

Tree = BallTree(M)

print(timeit.timeit("dist, idx = Tree.query(M, k=20)", globals=globals(),
                    number=100))
# 0.5208493099999032

dist, idx = Tree.query(M, k=20)

print(timeit.timeit("idx[dist>r] = M.shape[0]", globals=globals(),
                    number=100))
# 0.000831278000077873

idx[dist>r] = M.shape[0]
