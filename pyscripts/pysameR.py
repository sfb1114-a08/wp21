#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 23 16:52:55 2023

@author: schoelleh96
"""

# %% imports
from os.path import  dirname, abspath
from os import chdir, getcwd
chdir(dirname(abspath(__file__)))

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from LIB import calc as cc
from scipy.sparse import csc_matrix, find, identity
from numpy import exp
from scipy.sparse.linalg import eigs
from scipy.linalg import eig

# %% data
xpath = ("/home/schoelleh96/Nextcloud/WP2/WP2.1/diffMap/" +
         "shiny-diffusion-maps/data/swissroll_x.csv")
ypath = ("/home/schoelleh96/Nextcloud/WP2/WP2.1/diffMap/" +
         "shiny-diffusion-maps/data/swissroll_y.csv")

xpath = ("/home/schoelleh96/Nextcloud/WP2/WP2.1/diffMap/" +
         "shiny-diffusion-maps/data/gyre_x.csv")
ypath = ("/home/schoelleh96/Nextcloud/WP2/WP2.1/diffMap/" +
         "shiny-diffusion-maps/data/gyre_y.csv")

Kpath = ("/home/schoelleh96/Nextcloud/WP2/WP2.1/diffMap/" +
         "shiny-diffusion-maps/gyreKT1.csv")
Ppath = ("/home/schoelleh96/Nextcloud/WP2/WP2.1/diffMap/" +
         "shiny-diffusion-maps/gyreP.csv")

# xpath = ("/home/schoelleh96/Nextcloud/WP2/WP2.1/diffMap/" +
#          "shiny-diffusion-maps/data/bickley_wild_x.csv")
# ypath = ("/home/schoelleh96/Nextcloud/WP2/WP2.1/diffMap/" +
#          "shiny-diffusion-maps/data/bickley_wild_y.csv")

X = pd.read_csv(xpath, header=None).to_numpy()
Y = pd.read_csv(ypath, header=None).to_numpy()

KfromR = pd.read_csv(Kpath, header=0).to_numpy()[:,1:]
PfromR = pd.read_csv(Ppath, header=0).to_numpy()[:,1:]

# %% plot spectrum over eps

f, ax = plt.subplots(1,1)

# for e in [0.01, 0.02, 0.05, 0.1, 0.2, 0.5]:

for e in [0.0002, 0.0005, 0.001, 0.002, 0.04]:

    D = cc.distances(X, Y, 0.1, X.shape[0], X.shape[1], sphere=False)
    P = csc_matrix((X.shape[0], X.shape[0]))
    for j in range(0, X.shape[1]):
        K = csc_matrix((np.exp(-D[2][j]**2/e), (D[0][j],D[1][j])))
        K = K.multiply(1/K.sum(axis=1))
        P = P + K

    Q = (1/(X.shape[1] - 0)) * P

    sparsity = find(Q)[0].shape[0]/(Q.shape[0]*Q.shape[1])

    print("sparsity for epsilon {}: {}".format(e, sparsity))

    try:
        #L = (Q-identity(Q.shape[0]))/e
        #vals, vecs = eig(L.toarray())#

        #vals, vecs = eigs((1/e)*(Q-identity(Q.shape[0])), k=10)
        vals, vecs = eigs(Q, k=10)

    except:
        print("Eigs failed, using eig")
        # vals, vecs = eig(1/e*(Q-identity(Q.shape[0])).toarray())#
        vals, vecs = eig(Q.toarray())

    # E, kclust = cc.calc_cs(X, Y, Z, N_k=10, epsilon=e)
    # print(vals)
    vals = (vals - 1)/e
    ax.scatter(x=np.arange(1,11), y=np.real(vals[:10]))


# %%
E, kclust = cc.calc_cs(X, Y, epsilon=0.0001, N_k=11, sphere=False)

plt.scatter(x=np.arange(1,12), y=np.real(E[0][:11]))

plt.scatter(X, Y, c=E[1][:,1])

plt.scatter(X, Y, c=kclust.labels_)

evecs = pd.DataFrame({'x': np.real(E[1][:,1]),
                     'y': np.real(E[1][:,2])})
evecs.x = np.sign(np.max(evecs.x) + np.min(evecs.x))*evecs.x
evecs.y = np.sign(np.max(evecs.y) + np.min(evecs.y))*evecs.y
plt.scatter(evecs.x, evecs.y, c=kclust.labels_)

