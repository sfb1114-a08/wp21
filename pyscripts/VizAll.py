#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  8 10:40:17 2023

@author: schoelleh96
"""

# This will move the console to the right working directory.
from os.path import  dirname, abspath
from os import chdir
chdir(dirname(abspath(__file__)))

from sys import path
import numpy as np
path.append("./LIB")

import data as dd
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from matplotlib.widgets import Slider

import matplotlib as mpl
from pyproj import CRS, Transformer

# %% Get the data

dates = [datetime(2016, 4, 29, 6) + timedelta(hours=i)
          for i in range(0, 144, 6)]
fpath = "../era5/traj/" + dates[0].strftime('%Y')

# %% The class

class PointCloudVisualizer:
    def __init__(self, dates, d_path, new_start_every):
        self.dates = dates  # List of dates
        self.d_path = d_path
        self.new_start_every = new_start_every
        self.fig = plt.figure()
        # Set the figure to full screen
        mng = plt.get_current_fig_manager()
        mng.full_screen_toggle()
        self.ax = self.fig.add_subplot(111, projection='3d')
        self.ax.view_init(azim=0, elev=90)
        self.geo_extend = [20, 90, 0, -360]

        # Define the Stereographic projection with central latitude 90° and
        # latitude of true scale at 50°N
        self.srcp = '+proj=latlon'
        self.dstp = '+proj=stere +lat_0=90 +lat_ts=50'

        self.slider_ax = self.fig.add_axes([0.25, 0.05, 0.65, 0.03])

        self.B = dd.get_block_dat(dates)
        self.B[1], self.B[2] = self.project_coordinates(self.B[1], self.B[2])
        # Initialize a variable to hold the contour reference
        self.block_contour = None
        self.coasts = dd.get_coasts(*self.geo_extend)

        # Preload data and assume each file has shape [n_points, n_timesteps]
        # with 'lon', 'lat', 'p' keys
        self.trajectories = self.load_and_transform_data(self.d_path +
                                     date.strftime('/traj_%Y%m%d_%H') + ".npy"
                                 for date in self.dates)

        # Determine the number of colors needed,
        # assume all trajectories have same length
        trajectory_durations = self.trajectories[0]['x'].shape[1]
        self.num_colors = int(trajectory_durations/new_start_every)

        # Get the colormap
        self.color_map = mpl.colormaps['viridis']

        # Generate a list of colors from the colormap
        self.colors = [self.color_map(i / self.num_colors) for i in
                       range(self.num_colors)]

        # Determine the total duration needed for the slider based on the
        # longest trajectory
        max_duration = trajectory_durations + \
            len(self.trajectories) * self.new_start_every

        self.slider = Slider(self.slider_ax, 'Real Time', 0, max_duration - 1,
                             valinit=0, valstep=1)
        self.slider.on_changed(self.update_plot)
        # Set initial label text
        self.slider.valtext.set_text(self.format_slider_label(0))
        self.scatter_plots = []  # To keep track of scatter plots for updating

    def format_slider_label(self, val):
        # Calculate the current datetime for the slider value and format it as
        # a string.  The slider value 'val' represents the elapsed hours from
        # the start of the first trajectory
        current_time = self.dates[0] + timedelta(hours=(val-72))
        return current_time.strftime('%Y-%m-%d-%H')

    def load_and_transform_data(self, data_files):
            trajectories_data = []
            for f in data_files:
                data = np.load(f)
                lon, lat, p = data['lon'], data['lat'], data['p']
                x, y = self.project_coordinates(lon, lat)
                trajectories_data.append({'x': x, 'y': y, 'p': p})
            return trajectories_data

    def project_coordinates(self, lon, lat):
        crs1 = CRS.from_proj4(self.srcp)
        crs2 = CRS.from_proj4(self.dstp)
        trans = Transformer.from_crs(crs1, crs2, always_xy=True)
        return trans.transform(lon, lat)

    def update_contour(self, block, t):
        # Remove the previous contour plot if it exists
        if self.block_contour:
            for coll in self.block_contour.collections:
                coll.remove()

        # Project the coordinates
        # xb, yb = self.project_coordinates(block[1], block[2])

        # Draw a new contour plot
        self.block_contour = self.ax.contour(self.B[1], self.B[2],
                                             block[0][t,:,:], [0.5],
                                             zdir="z", offset=1050, colors="k")

    def plot_initial(self):
        # Plot the initial state for all trajectories
        for i, traj in enumerate(self.trajectories):
            # Cycle through the available colors
            color_idx = i % self.num_colors
            color = self.colors[color_idx]
            # Calculate the starting offset for each trajectory
            offset = i * self.new_start_every
            if offset == 0:  # If no offset, plot the first point
                sc = self.ax.scatter(traj['x'][:, 0], traj['y'][:, 0],
                                     traj['p'][:, 0], color=color, alpha=.25)
                self.scatter_plots.append(sc)
            else:  # Otherwise, do not plot anything initially
                sc = self.ax.scatter([], [], [], color=color)
                self.scatter_plots.append(sc)
        self.ax.invert_zaxis()

        # Plot coastlines
        for lon, lat in self.coasts:
            x, y = self.project_coordinates(lon, lat)
            z = [1050] * len(x)
            self.ax.plot(x, y, z, color='grey')
            # This will connect the points as line segments

        # plot lat and lon lines
        longitudes = np.arange(self.geo_extend[3], self.geo_extend[2], 60)
        latitudes = np.arange(self.geo_extend[0], self.geo_extend[1], 20)
        # Plot longitudes (meridians)
        for lon in longitudes:
            lats = np.linspace(self.geo_extend[0], self.geo_extend[1],
                               180//20 + 1)
            lons = np.full(lats.shape, lon)
            x, y = self.project_coordinates(lons, lats)

            z = np.full(x.shape, 1050)
            self.ax.plot(x, y, z, linestyle='--', color='grey')

        # Plot latitudes (parallels)
        for lat in latitudes:
            if abs(lat) == 90:
                continue  # Skip poles
            lons = np.linspace(self.geo_extend[3], self.geo_extend[2],
                               360//2 + 1)
            lats = np.full(lons.shape, lat)
            x, y = self.project_coordinates(lons, lats)
            z = np.full(x.shape, 1050)
            self.ax.plot(x, y, z, linestyle='--', color='grey')

        self.update_contour(self.B, 0)

    def update_plot(self, val):
        # Update the plot to show the state at the slider's current value
        real_time = int(val)
        self.slider.valtext.set_text(self.format_slider_label(real_time))
        for i, (sc, traj) in enumerate(zip(
                self.scatter_plots, self.trajectories)):
            # Adjust time for each trajectory's start
            traj_time = real_time - i * self.new_start_every
            # Check if within the trajectory's duration
            if 0 <= traj_time < traj['x'].shape[1]:
                sc._offsets3d = (traj['x'][:, traj_time],
                                 traj['y'][:, traj_time],
                                 traj['p'][:, traj_time])
            else:  # If not, set data to empty
                sc._offsets3d = ([], [], [])
        self.update_contour(self.B, int(real_time/6))
        self.fig.canvas.draw_idle()  # Redraw the figure

    def show(self):
        self.plot_initial()
        plt.show()

# %%

# Usage:
viz = PointCloudVisualizer(dates[15:16], fpath, 6)
viz.show()

# %% try the same using mayavi

import os
os.environ['ETS_TOOLKIT'] = 'qt'

from traits.api import HasTraits, Range, Instance, on_trait_change, Int
from traitsui.api import View, Item, HGroup, VGroup
from mayavi import mlab
from mayavi.core.ui.api import MlabSceneModel, SceneEditor
from pyproj import CRS, Transformer
from datetime import timedelta
import numpy as np

class PointCloudVisualizer(HasTraits):
    # Mayavi Scene
    scene = Instance(MlabSceneModel, ())

    # Slider for the time control
    # Traits for dynamic bounds of the Range object
    time_slider_low = Int(0)
    time_slider_high = Int(1)
    time_slider = Range('time_slider_low', 'time_slider_high', value=0)

    # The layout of the view
    view = View(
        VGroup(
            Item('scene', editor=SceneEditor(scene_class=MlabSceneModel),
                 height=400, width=600, show_label=False),
            Item('time_slider', show_label=False)
        ),
        resizable=True
    )

    def __init__(self, dates, d_path, new_start_every, **kwargs):
        super(PointCloudVisualizer, self).__init__(**kwargs)
        self.dates = dates
        self.d_path = d_path
        self.new_start_every = new_start_every
        self.srcp = '+proj=latlon'
        self.dstp = '+proj=stere +lat_0=90 +lat_ts=50'
        self.transformer = Transformer.from_crs(CRS.from_proj4(self.srcp),
                                                CRS.from_proj4(self.dstp),
                                                always_xy=True)

        # Load data
        self.trajectories = self.load_and_transform_data()

        # Calculate the range for the time slider
        self.calculate_time_slider_bounds()

        # Initial plot
        self.plot_initial()

    def load_and_transform_data(self):
        # Placeholder for your actual data loading and transforming logic
        trajectories = []
        for date in self.dates:
            data_file = f"{self.d_path}/traj_{date.strftime('%Y%m%d_%H')}.npy"
            data = np.load(data_file)
            x, y = self.transformer.transform(data['lon'], data['lat'])
            trajectories.append({'x': x, 'y': y, 'p': data['p']})
        return trajectories

    def calculate_time_slider_bounds(self):
        # Placeholder for your actual logic to calculate the bounds
        trajectory_durations = self.trajectories[0]['x'].shape[1]

        self.time_slider_high =  trajectory_durations + \
            len(self.trajectories) * self.new_start_every

    def plot_initial(self):
        # Placeholder for your initial plotting logic
        for traj in self.trajectories:
            x, y, z = traj['x'], traj['y'], traj['p']
            self.scene.mlab.points3d(x, y, z, colormap='viridis')

    @on_trait_change('time_slider')
    def update_plot(self):
        # Placeholder for updating the plot according to the slider
        pass

    def project_coordinates(self, lon, lat):
        return self.transformer.transform(lon, lat)

# %%

new_start_every = 6  # Example value
visualizer = PointCloudVisualizer(dates, fpath, new_start_every)
visualizer.configure_traits()

