#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 13 12:23:04 2023

@author: schoelleh96
"""

from pandas import read_csv, to_datetime, concat, DataFrame, Series, to_numeric
from datetime import datetime, timedelta
from itertools import product
from matplotlib.pyplot import savefig, rcParams, legend, subplots
from os.path import  dirname, abspath
from os import chdir, getcwd
chdir(dirname(abspath(__file__)))

path = "./DATA/overlap.csv"

O = read_csv(path, header=0)
O['date_0'] = to_datetime(O['date_0'])

def cohK(a,b,c,d):
    '''
    Computes Cohen's Kappa for two partitionings of a set:
        A\B | 1 | 2 |
        1   | a | b |
        2   | c | d |
        Since the choice of labels 1 and 2 is arbitrary (we want kappa=1 both
        if 1(A)=1(B) and 1(A)=1(B)), we want symetry wrt a<->b and c<->d

    Parameters
    ----------
    a : int
        see above.
    b : int
        see above.
    c : int
        see above.
    d : int
        see above.

    Returns
    -------
    None.

    '''
    def Kap(a,b,c,d):
        # total
        N = a + b + c + d
        # proportion correct with arbitrary "truth"
        pc = (a + d)/N
        # agreement expected by chance
        pr = (a + b)/N * (a + c)/N + (c + d)/N * (b + d)/N
        return (pc - pr)/(1 - pr)

    return max((Kap(a,b,c,d),Kap(b,a,d,c)))

# %%

start = datetime(2016, 4, 29, 6)
end = datetime(2016, 5, 5, 0)

df = DataFrame(columns=['date_0', 'max_T', 'use_p', 'scaling', 'N', 'cK3',
                        'cK7', '113', '103', '013', '003', '117', '107',
                        '017', '007'])
cK = Series({'11':0,'10':0,'01':0,'00':0})

timestamp = start

i = [True, 100]

while timestamp <= end:
    print(timestamp)
    for max_T in [169, 73]:
        DTH3 = O[(O.date_0 == timestamp) & (O.max_T == max_T) &
                 (O.ctype == "dth3")].cID
        DTH7 = O[(O.date_0 == timestamp) & (O.max_T == max_T) &
                 (O.ctype == "dth7")].cID
        for i in product([False, True], [1, 100, 1000]):
            O_C = O[(O.date_0 == timestamp) & (O.max_T == max_T) &
                    (O.use_p == i[0]) & (O.scaling == i[1]) &
                    (O.ctype == "cs")].cID
            if not O_C.empty:
                O_C3 = (DTH3.reset_index()['cID'].astype(str) +
                        O_C.reset_index()['cID'].astype(str)).value_counts()
                O_C7 = (DTH7.reset_index()['cID'].astype(str) +
                        O_C.reset_index()['cID'].astype(str)).value_counts()

                O_C3 = cK.add(O_C3, fill_value=0).astype(int)
                O_C7 = cK.add(O_C7, fill_value=0).astype(int)

                N = O_C3.sum()

                dd = Series({'date_0':timestamp, 'max_T': max_T, 'use_p': i[0],
                             'scaling': i[1], 'N': N,
                             'cK3': cohK(O_C3['00'], O_C3['10'],
                                         O_C3['01'], O_C3['11']),
                             'cK7': cohK(O_C7['00'], O_C7['10'],
                                         O_C7['01'], O_C7['11']),
                             '113': O_C3['11']/N, '103': O_C3['10']/N,
                             '013': O_C3['01']/N, '003': O_C3['00']/N,
                             '117': O_C7['11']/N,'107': O_C7['10']/N,
                             '017': O_C7['01']/N,'007': O_C7['00']/N})

                df = concat([df, dd.to_frame().T], ignore_index=True)

    timestamp += timedelta(hours=6)

cols = df.columns[4:]
df[cols] = df[cols].apply(to_numeric, errors='coerce')

df = df.assign(c13=df[['003', '103']].sum(1), c23=df[['013', '113']].sum(1),
               maxcs3=lambda df: df[['c13','c23']].max(1))

df = df.assign(c17=df[['007', '107']].sum(1), c27=df[['017', '117']].sum(1),
               maxcs7=lambda df: df[['c17','c27']].max(1))

# %% plot

groups = df.groupby(['use_p', 'scaling'])
fig, ax = subplots(1,1,figsize=(10,8))
fig2, ax2 = subplots(1,1,figsize=(10,8))

for name, group in groups:
    ax.scatter(x=group.cK3, y=group.maxcs3, label=name)
    ax2.scatter(x=group.cK3, y=group.cK7, label=name)

ax.legend(title="use p,  scaling")
ax2.legend(title="use p,  scaling")

ax.set_xlabel("$\kappa(.,(\Delta \Theta)_3)$", fontsize=15)
ax.set_ylabel("maximum relative set size", fontsize=15)

ax2.set_xlabel("$\kappa(.,(\Delta \Theta)_3)$", fontsize=15)
ax2.set_ylabel("$\kappa(.,(\Delta \Theta)_7)$", fontsize=15)

fig.suptitle("Overlap btw. coherent sets and 3 day $\Delta \Theta$" +
             " partitioning", fontsize=18)
fig.tight_layout()
fig.savefig("./im/overlap/k3vsize.png")
fig2.suptitle("Overlap btw. coherent sets and 3vs 7 day" +
              " $\Delta \Theta$ partitioning", fontsize=18)
fig2.tight_layout()
fig2.savefig("./im/overlap/k37.png")

# %% Is the clustering dependent on the backward time frame? Calculate cK
# between max_T 169 and max_T 72 sets.


start = datetime(1981, 1, 8, 12)
end = datetime(1981, 1, 14, 18)

dff = DataFrame(columns=['date_0', 'use_p', 'scaling','N', 'cKT', '11',
                         '10', '01', '00'])

timestamp = start

i = [True, 100]

while timestamp <= end:
    print(timestamp)
    for i in product([False, True], [1, 100, 1000]):
        O_C169 = O[(O.date_0 == timestamp) & (O.use_p == i[0]) &
                   (O.scaling == i[1]) & (O.ctype == "cs") &
                   (O.max_T == 169)].cID
        O_C73 = O[(O.date_0 == timestamp) & (O.use_p == i[0]) &
                   (O.scaling == i[1]) & (O.ctype == "cs") &
                   (O.max_T == 73)].cID
        if not O_C73.empty:
            O_CT = (O_C73.reset_index()['cID'].astype(str) +
                    O_C169.reset_index()['cID'].astype(str)).value_counts()

            O_CT = cK.add(O_CT, fill_value=0).astype(int)

            N = O_CT.sum()

            dd = Series({'date_0':timestamp, 'use_p': i[0],
                         'scaling': i[1], 'N': N,
                         'cKT': cohK(O_CT['00'], O_CT['10'],
                                     O_CT['01'], O_CT['11']),
                         '11': O_CT['11']/N, '10': O_CT['10']/N,
                         '01': O_CT['01']/N, '00': O_CT['00']/N})

            dff = concat([dff, dd.to_frame().T], ignore_index=True)

    timestamp += timedelta(hours=6)

cols = dff.columns[4:]
dff[cols] = dff[cols].apply(to_numeric, errors='coerce')

dff = dff.assign(c17=dff[['00', '10']].sum(1), c27=dff[['01', '11']].sum(1),
               maxcs7=lambda dff: dff[['c17','c27']].max(1))

dff = dff.assign(c13=dff[['00', '01']].sum(1), c23=dff[['10', '11']].sum(1),
               maxcs3=lambda dff: dff[['c13','c23']].max(1))

dff = dff.assign(maxcs=lambda dff: dff[['maxcs3','maxcs7']].max(1))

groups = dff.groupby(['use_p', 'scaling'])
fig, ax = subplots(1,1,figsize=(10,8))
for name, group in groups:
    ax.scatter(x=group.cKT, y=group.maxcs, label=name)
ax.legend(title="use p,  scaling")
ax.set_xlabel("$\kappa(._3,._7)$", fontsize=15)
ax.set_ylabel("maximum relative set size", fontsize=15)
fig.suptitle("Overlap btw. coherent sets from 7 vs 3 day data", fontsize=18)
fig.tight_layout()
fig.savefig("./im/overlap/k37vsize.png")

# %%
# get bounding box of trajs 3 days backward
from os import listdir
from os.path import isfile, join
import numpy as np
onlyfiles = [f for f in listdir("/net/scratch/schoelleh96/WP2/WP2.1/" +
                                "LAGRANTO/wp21/traj/2016")
             if isfile(join("/net/scratch/schoelleh96/WP2/WP2.1/" +
                            "LAGRANTO/wp21/traj/2016", f))]

maxlon=170
minlon=170
maxlat=50
minlat=50

for f in onlyfiles:
    trajs = Tra()
    trajs.load_ascii("./traj/2016/" + f, gz=False)
    trajs.set_array(trajs.get_array().transpose()[:,:73])
    if maxlat < np.max(trajs['lat']):
        maxlat = np.max(trajs['lat'])
    if minlat < np.min(trajs['lat']):
        minlat = np.min(trajs['lat'])

    # if maxlon < np.max(trajs['lon']):
    #     maxlon = np.max(trajs['lon'])
    # if minlon < np.min(trajs['lon']):
    #     maxlon = np.max(trajs['lon'])

print("LAT:")
print(np.max(trajs['lat']))
print(np.min(trajs['lat']))

print("LON:")
