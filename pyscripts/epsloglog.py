#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 28 12:43:24 2023

@author: schoelleh96
"""

# %% library imports

# This will move the console to the right working directory.
from os.path import  dirname, abspath, exists, join
from os import chdir
chdir(dirname(abspath(__file__)))

from sys import argv, path, exit
import numpy as np
path.append("./LIB")
import calc as cc
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import pandas as pd
import seaborn as sb
from matplotlib.colors import LogNorm, BoundaryNorm
import matplotlib.ticker as ticker
import pickle
import cmcrameri.cm as cmc
import locale
import matplotlib.colors as mcolors

# %% Initials

if argv[0] == '':
    date_0 = "20160502_00"
    trapath = ("../era5/traj/2016/")
else:
    trapath = argv[1]
    date_0 = argv[2]
    from matplotlib import use
    use('agg')

# only every nth trajectory
n = 1

YEAR = "2017"
# Initialize dates
if YEAR == "2016":
    dates = [datetime(2016, 4, 30, 0), datetime(2016, 5, 2, 0),
             datetime(2016, 5, 4, 0)]
    dates = [datetime(2016, 4, 29, 6) + timedelta(hours=i)
             for i in range(0, 144, 6)]
    # dates = [datetime(2016,5,6,0), datetime(2016,5,7,0)]
    trapath = ("../era5/traj/2016/")
elif YEAR == "2017":
    dates = [datetime(2017, 1, 23, 18) + timedelta(hours=i)
             for i in range(0, 28*6, 6)]
    trapath = ("../era5/traj/2017/")

# k_p_var = cc.calc_k(U/1000, V/1000, Omg/100)
k_p = 15

base_font_size = 10  # pt

locale.setlocale(locale.LC_TIME, 'en_US')

plt.rcParams['text.usetex'] = True
plt.rcParams['pgf.texsystem'] = "pdflatex"
plt.rcParams['text.latex.preamble'] = (
    r'\usepackage{amsmath,amsfonts,amssymb,cmbright,standalone}')
plt.rcParams['font.size'] = base_font_size

# %% functions

def get_NN(dataX, dataY, dataZ, r, m, k=15):
    from sklearn.neighbors import BallTree

    dd = np.array([np.deg2rad(dataX), np.deg2rad(dataY)]).T
    BT = BallTree(dd, metric='haversine')
    idx, dist = BT.query_radius(dd, r=r / 6371, return_distance=True)
    dist = dist * 6371
    # each element in idx/hdist corresponds to a point whose NN has
    # been queried

    for i in range(m):
        vdist = dataZ[idx[i]] - dataZ[i]

        dist[i] = np.sqrt(np.power(dist[i], 2) + np.power(k * vdist, 2))

    return idx, dist

def calc_Sums(trapath, date_0, NN=False):

    trajs = np.load(trapath + "traj_" + date_0 + ".npy")

    Lon = trajs['lon'][::n]
    Lat = trajs['lat'][::n]
    P = trajs['p'][::n]

    Ntimesteps = Lon.shape[1]
    Npoints = Lon.shape[0]

    epsilon_opt = np.logspace(2, 9, num=36)

    i = 0

    Sum_list = list()

    for t in range(Ntimesteps):
        print(t)
        # Parallelization only works if there arent too many traj,
        # crashes otherwise (RAM probably)
        # D = delayed(cc.distances)(Lon[:,t], Lat[:,t], 3*np.sqrt(1e10),
        # D = cc.distances(Lon[:, t], Lat[:, t], 3*np.sqrt(1e10),
        #                  Npoints, dataZ=P[:, t],
        #                  customMetric=True, k=k_p)
        # D = dd.io_dist(path, 1e5, Lon[:,[t]], Lat[:,[t]], P[:,[t]], True,k_p,
        #                 "p", [t], force_calc=False)

        # Do NN search
        if NN:
            eps = 1e5
            at_least_5_NN = False
            while not (at_least_5_NN):
                D = get_NN(Lon[:, t], Lat[:, t], P[:, t], 3*np.sqrt(eps),
                           Npoints, k=k_p)
                at_least_5_NN = True
                for d in D[0]:
                    if d.shape[0] < 6:
                        print(d.shape[0])
                        at_least_5_NN = False
                        break
                eps = eps * 10
                print(eps)
            mean = np.mean([np.mean(np.sort(arr)[1:6]) for arr in D[1]])
            df = pd.DataFrame({'t': t,
                               'date': date_0,
                               'NNdist': mean},
                               index = [i])
            Sum_list.append(df)
            i += 1
        else:
            for e in epsilon_opt:
                # vals = delayed(np.exp)(-np.square(D[2])/e)
                vals = np.exp(-np.square(D[2])/e)

                # df = delayed(pd.DataFrame)({'eps' : e,
                df = pd.DataFrame({'eps': e,
                                   't': t,
                                   'date': date_0,
                                   'sums': ((2*vals.sum() +
                                             Npoints)
                                            / (Npoints**2)),  # sums are normalized
                                   'm': Npoints,
                                   'dist': D[2].mean()},
                                  index=[i])
                Sum_list.append(df)
                i += 1

    # Sum_list = compute(*Sum_list)
    Sums = pd.concat(Sum_list, ignore_index=True)

    return Sums


def calc_dd(group):
    leps = np.log(group["eps"]).reset_index(drop=True)
    lS = np.log(group["sums"] * np.square(group["m"])).reset_index(drop=True)

    # This is the derivative of log(S(eps) with respect to log(eps):
    dlS = lS.diff().dropna() / leps.diff().dropna()
    # We assume that the steps in the logarithm of eps are uniform

    e_max = np.argmax(dlS)
    d = 2 * dlS[e_max]

    density = ((np.log(np.pi) + leps[e_max])/2 +
               (np.log(group["m"].reset_index(drop=True)[0]) - lS[e_max])/d)
    rho = density * d

    # meandist = np.mean(- np.log((group["sums"] * np.square(group["m"])
    #                              - group["m"]) / 2) *
    #                    (2 * group["eps"] / (group["m"] * (group["m"] - 1))))
    meandist = np.mean(np.sqrt(-np.log(group["sums"]) * group["eps"]))
    dist = np.mean(group["dist"])
    NNdist = np.mean(group["NNdist"])
    return pd.Series({"dimension": d, "density": density, "rho": rho,
                      "e_max": np.exp(leps[e_max]), "L": np.exp(density),
                      "meandist": meandist, "dist": dist, "NNdist": NNdist})


def plot_epsloglog(Sums, name, in3d=False):
    fig = plt.figure(figsize=(6.5,3.25))
    if in3d:
        ax = fig.add_subplot(111, projection='3d')
        if name == "all":
            dates = Sums['date'].unique()
            colors = plt.cm.viridis(np.linspace(0, 1, len(dates)))
            for idx, date in enumerate(dates):
                subset = Sums[Sums['date'] == date]
                ax.plot(subset['eps'], [idx]*len(subset), subset['sums'],
                        label=date, color=colors[idx])

        else :
            t = Sums['t'].unique()
            colors = plt.cm.viridis(np.linspace(0, 1, len(t)))
            for idx, t_i in enumerate(t):
                subset = Sums[Sums['t'] == t_i]
                ax.plot(np.log10(subset['eps']), [idx]*len(subset),
                        np.log10(subset['sums']),
                        label=t_i, color=colors[idx])
            ax.set_ylabel("t")

        def generate_log_ticks(values):
            log_values = np.log10(values)
            min_val, max_val = (np.floor(log_values.min()),
                                np.ceil(log_values.max()))
            ticks = np.arange(min_val, max_val+1)
            return ticks, 10**ticks

        xticks, xticklabels = generate_log_ticks(Sums['eps'])
        ax.set_xticks(xticks)
        ax.set_xticklabels([int(label) if label >= 1 else round(label, 2)
                            for label in xticklabels])

        zticks, zticklabels = generate_log_ticks(Sums['sums'])
        ax.set_zticks(zticks)
        ax.set_zticklabels([int(label) if label >= 1 else round(label, 2)
                            for label in zticklabels])

        ax.set_xlabel("epsilon")
        ax.set_zlabel("sums")

        plt.savefig("../im/epsloglog/3d" + name + ".png", bbox_inches="tight")

    else:
        ax = fig.add_subplot(111)
        if name == "all":
            sb.lineplot(data=Sums, x="eps", y="sums", hue="date", marker=".",
                        dashes=False, legend=True)
        else:
            colors = [cmc.managua(i/len(Sums['t'].unique()))
                      for i in range(len(Sums['t'].unique()))]

            # Use the generated colors in your Seaborn plot
            palette = sb.color_palette(colors)
            # palette = sb.color_palette("icefire",
            #                             n_colors=len(Sums['t'].unique()),
            #                             as_cmap=False)
            # palette = sb.diverging_palette(240, 10, n=len(Sums['t'].unique()),
            #                                 center="light")
            sb.lineplot(data=Sums, x="eps", y="sums", hue="t",  # marker=".",
                        dashes=False, legend=True, palette=palette,
                        linewidth=.5)
        evaluated_x_values = Sums['eps'].unique()
        sb.rugplot(x=evaluated_x_values, height=0.025, color='green')
        # plt.setp(plt.gca().get_lines(), markersize=2)
        ax.set_xlabel("$\\epsilon$ [km\\textsuperscript{2}]")
        ax.set_ylabel("$S_{t}(\\epsilon) m^{-2}$ [--]")
        handles, labels = plt.gca().get_legend_handles_labels()
        desired_labels = [Sums['t'].min(), Sums['t'].quantile(0.25),
                          Sums['t'].median(), Sums['t'].quantile(0.75),
                          Sums['t'].max()]
        desired_labels = [str(int(d)) for d in desired_labels]
        filtered_handles = [h for h, l in zip(handles, labels)
                            if l in desired_labels]
        desired_labels = [r'${}$'.format(d) for d in desired_labels]
        # Add a legend outside of the plot
        legend = plt.legend(filtered_handles, desired_labels)
        legend.set_title("$t$ [h]")
        for legobj in legend.legend_handles:
            legobj.set_linewidth(2)  # Thicker lines in the legend
        plt.xscale('log')
        plt.yscale('log')
        plt.savefig("../im/epsloglog/" + name + ".pdf",
                    bbox_inches="tight")

def plot_epsloglog_heat(Sums, name):
    fig = plt.figure(figsize=(5.6, 4))
    ax = fig.add_subplot(111)

    def draw_contours(ax, data, levels):
        nrows, ncols = data.shape
        for i in range(nrows):
            for j in range(ncols):
                value = data.iloc[i, j]
                # Check vertical contours
                if j < ncols - 1:
                    next_value = data.iloc[i, j+1]
                    for level in levels:
                        if ((value < level and next_value >= level) or
                            (value >= level and next_value < level)):
                            ax.plot([j+1, j+1], [i, i+1], color='k')
                # Check horizontal contours
                if i < nrows - 1:
                    next_value = data.iloc[i+1, j]
                    for level in levels:
                        if ((value < level and next_value >= level) or
                            (value >= level and next_value < level)):
                            ax.plot([j, j+1], [i+1, i+1], color='k')

    # Create the heatmap
    data = Sums.pivot(index="t", columns="eps",
                             values="sums")
    norm = LogNorm(vmin=data.min().min(), vmax=data.max().max())
    cbar_ax = fig.add_axes([.91, .3, .03, .4])  # Position of colorbar
    cax = sb.heatmap(data, annot=False, cmap="YlGnBu", ax=ax, norm=norm,
                     cbar_ax=cbar_ax)
    cbar_ax.set_yscale('log')
    cbar_ax.yaxis.set_major_locator(plt.LogLocator(base=10, numticks=5))
    xticks = cax.get_xticks()
    xlabels = [label.get_text() for label in cax.get_xticklabels()]
    xticks = xticks[::5]
    xlabels = xlabels[::5]

    # Format the chosen xlabels to be in scientific notation
    formatter = ticker.ScalarFormatter(useMathText=False)
    formatted_labels = [formatter.format_data(float(label))
                        for label in xlabels]

    # Update x-axis ticks and labels
    cax.set_xticks(xticks)
    cax.set_xticklabels(formatted_labels)
    # Overlay contours
    contour_levels = cax.collections[0].colorbar.get_ticks()
    draw_contours(ax, data, contour_levels)
    plt.savefig("../im/epsloglog/heat" + name + ".png")

# %% execution

# time.sleep(60*3600)


def load_or_calc_data(date_0, trapath, dat_path, force_calc=False):
    """
    Load or calculate data based on existence and the force_calc flag.

    Parameters
    ----------
        date_0 (str): The date identifier or 'all' for processing all dates.
        trapath (str): The path to the trajectory data.
        dat_path (str): The directory path for storing/loading the pickle files
        force_calc (bool): If True, force the recalculation of data.
    """
    def calc_and_save(date):
        """Helper function to calculate and save data."""
        sums = calc_Sums(trapath, date)
        filename = join(dat_path, f"{date}S.pkl")
        with open(filename, "wb") as f:
            print("Saving data to:", filename)
            pickle.dump(sums, f)
        return sums

    if date_0 == "all":
        Sum_list = []
        for date in dates:
            filename = join(dat_path, f"{date.strftime('%Y%m%d_%H')}S.pkl")
            print(filename)
            print(exists(filename))
            if not force_calc and exists(filename):
                with open(filename, "rb") as f:
                    S = pickle.load(f)
            else:
                S = calc_and_save(date.strftime('%Y%m%d_%H'))
            Sum_list.append(S)
        Sums = pd.concat(Sum_list, ignore_index=True)
    else:
        filename = join(dat_path, f"{date_0}S.pkl")
        if not force_calc and exists(filename):
            with open(filename, "rb") as f:
                Sums = pickle.load(f)
        else:
            Sums = calc_and_save(date_0)

    return Sums

def loadOrCalcData(date_0, trapath, datPath, forceCalc=False, calcNN=False):
    def calcAndSave(date):
        sums = calc_Sums(trapath, date, NN=calcNN)
        filename = join(datPath, f"{date}S.pkl")
        if exists(filename) and not forceCalc:
            with open(filename, "rb") as f:
                existingSums = pickle.load(f)
            sums = pd.merge(existingSums, sums, on=['t', 'date'])
        with open(filename, "wb") as f:
            print("Saving data to:", filename)
            pickle.dump(sums, f)
        return sums

    if date_0 == "all":
        SumList = []
        for date in dates:
            filename = join(datPath, f"{date.strftime('%Y%m%d_%H')}S.pkl")
            print(filename)
            print(exists(filename))
            if not forceCalc and exists(filename) and not calcNN:
                with open(filename, "rb") as f:
                    S = pickle.load(f)
            else:
                S = calcAndSave(date.strftime('%Y%m%d_%H'))
            SumList.append(S)
        Sums = pd.concat(SumList, ignore_index=True)
    else:
        filename = join(datPath, f"{date_0}S.pkl")
        if not forceCalc and exists(filename) and not calcNN:
            with open(filename, "rb") as f:
                Sums = pickle.load(f)
        else:
            Sums = calcAndSave(date_0)

    return Sums

# Example usage:
Sums = load_or_calc_data(date_0, trapath, 'pickle/', force_calc=False)


# if date_0 == "all":
#     Sum_list = list()
#     for date in dates:
#         Sums = calc_Sums(trapath, date.strftime('%Y%m%d_%H'))
#         # Sums = Sums.groupby(['eps', 'date'])['sums'].mean().reset_index()
#         Sum_list.append(Sums)
#     Sums = pd.concat(Sum_list, ignore_index=True)

# else:
#     Sums = calc_Sums(trapath, date_0)

# with open("pickle/" + date_0 + "S.pkl", "wb") as f:
#     print("saving" + date_0)
#     pickle.dump(Sums, f)

# # exit()
# # %% load if present

# if date_0 == "all":

#     Sum_list = list()
#     for date in dates:
#         if exists("pickle/" + date.strftime('%Y%m%d_%H') + "S.pkl"):
#             with open("pickle/" + date.strftime('%Y%m%d_%H')
#                       + "S.pkl", "rb") as f:
#                 S = pickle.load(f)
#                 # plot_epsloglog(S, date.strftime('%Y%m%d_%H'))
#         Sum_list.append(S)
#     Sums = pd.concat(Sum_list, ignore_index=True)

# else:
#     with open("pickle/" + date_0 + "S.pkl", "rb") as f:
#         Sums = pickle.load(f)

# exit()


Sums['t'] = Sums['t']-72
# %%

def plot_dd(Densities, name, var):

    fig = plt.figure(figsize=(3.2, 3.4))
    ax = fig.add_subplot(111)
    def draw_contours(ax, data, levels):
            nrows, ncols = data.shape
            for i in range(nrows):
                for j in range(ncols):
                    value = data.iloc[i, j]
                    # Check vertical contours
                    if j < ncols - 1:
                        next_value = data.iloc[i, j+1]
                        for level in levels:
                            if ((value < level and next_value >= level) or
                                (value >= level and next_value < level)):
                                ax.plot([j+1, j+1], [i, i+1], color='k',
                                        linewidth=0.8)
                    # Check horizontal contours
                    if i < nrows - 1:
                        next_value = data.iloc[i+1, j]
                        for level in levels:
                            if ((value < level and next_value >= level) or
                                (value >= level and next_value < level)):
                                ax.plot([j, j+1], [i+1, i+1], color='k',
                                        linewidth=0.8)

    # Create the heatmap
    data = Densities.pivot(index="date", columns="t",
                           values=var)
    if var == "e_max":
        unique_values = np.unique(data)
        num_unique_values = len(unique_values)

        discrete_cmap = mcolors.ListedColormap(
            cmc.batlow(np.linspace(0, 1, num_unique_values)))

        # Create BoundaryNorm for discrete intervals
        boundaries = np.append(unique_values, unique_values[-1] + 1)
        norm = BoundaryNorm(boundaries=boundaries, ncolors=discrete_cmap.N,
                            clip=True)

        # Generate the heatmap with discrete color mapping
        cax = sb.heatmap(data, annot=False, cmap=discrete_cmap, ax=ax,
                         antialiased=True, norm=norm, linewidths=0,
                         linecolor='white', rasterized=True,
                         cbar_kws={"orientation": "horizontal"})

        # Adjust the colorbar to show discrete ticks
        cbar = cax.collections[0].colorbar
        tick_locs = 0.5 * (boundaries[:-1] + boundaries[1:])  # Midpoints of the intervals
        cbar.set_ticks(tick_locs)
        cbar.set_ticklabels([f'{v:.0e}'.replace('e+0', 'e') for v in
                             unique_values])
        cbar.ax.tick_params(labelrotation=45)

    else:
        cax = sb.heatmap(data, annot=False, cmap=cmc.batlow, ax=ax,
                         antialiased=True, linewidths=0, linecolor='k',
                         rasterized=True,
                         cbar_kws={"orientation": "horizontal"})
        cbar = cax.collections[0].colorbar
    ax.set_yticks([])  # Remove y-axis ticks
    ax.set_yticklabels([])  # Remove y-axis tick labels

    if var == "dimension":
        cbar.set_label('$d$')
        # ax.text(-0.1, 1.05, '(a)', transform=ax.transAxes, fontsize=10,
        #         va='top')
    elif var == "density":
        # ax.text(-0.1, 1.05, '(b)', transform=ax.transAxes, fontsize=10,
        #         va='top')
        cbar.set_label('$\\ell$')
    elif var == "L":
        # ax.text(-0.1, 1.05, '(b)', transform=ax.transAxes, fontsize=10,
        #         va='top')
        cbar.set_label('$\\ell$ [km]')
    elif var in ["meandist", "dist", "NNdist"]:
        # ax.text(-0.1, 1.05, '(b)', transform=ax.transAxes, fontsize=10,
        #         va='top')
        if var == "meandist":
            cbar.set_label('$\\overline{\\mathrm{dist}^{\\ast}}$ [km]')
        elif var == "NNdist":
            cbar.set_label(
                '$\\overline{\\mathrm{dist}_{\\mathrm{NN},5}}$ [km]')
        else:
            cbar.set_label('$\\overline{\\mathrm{dist}}$ [km]')
    elif var == "rho":
        cbar.set_label('$\\rho$')
    elif var == "e_max":
        cbar.set_label('$\\epsilon^{\\ast}$ [km\\textsuperscript{2}]')

    # dates = data.index.tolist()

    # y_labels = [datetime.strptime(date, "%Y%m%d_%H").strftime("%d %B") if
    #             date[-2:] == '00' else '' for date in dates]

    # ax.set_yticks(np.arange(0, len(dates)))
    # tick_positions = [i + .5 for i, label in enumerate(y_labels) if label]
    # ax.set_yticks(tick_positions)
    # if not (var in ["density", "L"]):
    #     ax.set_yticklabels([label for label in y_labels if label])
    # if name == "all2016":
    #     ax.set_yticks(np.arange(3.5, len(dates), 4))
    #     if var == "dimension":
    #         ax.set_yticklabels(
    #             [datetime.strptime(d, "%Y%m%d_%H").strftime("%d %B")
    #             for d in dates[3::4]])
    # elif name == "all2017":
    #     ax.set_yticks(np.arange(1.5, len(dates), 4))
    #     if var == "dimension":
    #         ax.set_yticklabels(
    #             [datetime.strptime(d, "%Y%m%d_%H").strftime("%d %B")
    #              for d in dates[1::4]])

    t = data.columns.tolist()
    ax.set_xticks([0.5, int(len(t)/2 + 0.5) - 0.5, len(t)-0.5])
    tick_labels = [r'${}$'.format(i) for i in
                   [t[0], t[int(len(t)/2)], t[-1]]]
    ax.set_xticklabels(tick_labels, rotation=0)

    ax.set_xlabel('')

    plt.figtext(0.25, 0.3, r"$t$ [h] $\rightarrow$", ha="center", va="center",
                fontsize=base_font_size)
    ax.set_ylabel('')
    # Overlay contours
    # contour_levels = cax.collections[0].colorbar.get_ticks()
    # draw_contours(ax, data, contour_levels)

    # # Adding stair-case lines
    # numRows = len(data.index)
    # numCols = len(data.columns)

    # for row in range(4, 2*numRows+4, 4):
    #     if row % 4 == 0:  # Ensure it's the 00UTC row
    #         for col in range(0, numCols, 6):
    #             x = [col + .5, col + 6.5, col + 6.5]
    #             y = [row - col/6 - .5, row - col/6 - .5, row - col/6 - 1.5]
    #             ax.plot(x, y, color="w", linewidth=.25)

    # Adding diagonal lines with a slope of 1/6
    numRows = len(data.index)
    numCols = len(data.columns)

    curr_date = dates[0] - timedelta(hours=(numCols-1)/2)
    i = 0  # iterates through rows
    while i + 1 < numRows + numCols/6:
        if curr_date.hour == 0:
            if i + 1 > numRows:
                x_start = (i - numRows + .5)*6 + .5
                y_start = numRows
            else:
                x_start = 0
                y_start = i + .5 + 1/12
            if i*6 > numCols:
                x_end = numCols
                y_end = ((i*6 - numCols + 1)/6 + .5 - 1/12)
            else:
                x_end = (i + .5 + 1/12)*6
                y_end = 0

            ax.plot([x_start, x_end], [y_start, y_end], color="w",
                    linewidth=.25)
            ax.text(x_end, y_end, curr_date.strftime("%d %b"), rotation=45,
                    horizontalalignment="left", verticalalignment="bottom")
        i += 1
        curr_date = curr_date + timedelta(hours=6)

    ax.plot([numCols/2, numCols/2], [0, numRows], color="w", linewidth=.25)

    plt.savefig("../im/epsloglog/" + var + name + ".pdf", bbox_inches="tight")

    return fig


# %%

plot_epsloglog(Sums, date_0)
plot_epsloglog_heat(Sums, date_0)

Densities = Sums.groupby(["date", "t"]).apply(calc_dd).reset_index()
f1 = plot_dd(Densities, date_0 + YEAR, "density")
f2 = plot_dd(Densities, date_0 + YEAR, "dimension")
f3 = plot_dd(Densities, date_0 + YEAR, "rho")
f4 = plot_dd(Densities, date_0 + YEAR, "e_max")
f5 = plot_dd(Densities, date_0 + YEAR, "L")
f6 = plot_dd(Densities, date_0 + YEAR, "meandist")
f7 = plot_dd(Densities, date_0 + YEAR, "dist")
f8 = plot_dd(Densities, date_0 + YEAR, "NNdist")

# %%

import matplotlib.pyplot as plt
from datetime import datetime
import matplotlib.dates as mdates

def plot_3d_surface(Densities, name, var):
    # Pivot the DataFrame to create a grid
    data = Densities.pivot(index="date", columns="t", values=var)

    # Create x, y grid for the surface plot
    t = data.columns.values
    date_values = np.array([datetime.strptime(d, "%Y%m%d_%H") for d in data.index])
    X, Y = np.meshgrid(t, date_values)

    # Create a figure for 3D plotting
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111, projection='3d')

    # Convert 'date' values to numbers for plotting (if required)
    Y = mdates.date2num(Y)

    # Create a 3D surface plot
    surf = ax.plot_surface(X, Y, data.values, cmap='YlGnBu', edgecolor='none')

    # Add color bar
    cbar = fig.colorbar(surf, ax=ax, shrink=0.5, aspect=5)
    cbar.set_label(var)

    # Set labels
    ax.set_xlabel('t')
    ax.set_ylabel('Date')
    ax.set_zlabel(var)

    # Format date on the y-axis
    ax.yaxis.set_major_locator(mdates.AutoDateLocator())
    ax.yaxis.set_major_formatter(mdates.DateFormatter("%Y-%m-%d"))

    # Rotate for better view
    ax.view_init(30, 220)

    # Save and return the figure
    plt.savefig("../im/epsloglog/3d_" + var + name + ".pdf", bbox_inches="tight")
    return fig

# Example usage
# plot_3d_surface(Densities, 'example', 'density')

# %% Investigate why l not 105km


data = np.genfromtxt("../era5/startf/2016/disregard/startf_20160502_00_reg",
                     delimiter='\t', names=['lon', 'lat', 'p'])
