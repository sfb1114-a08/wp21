#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  5 15:01:12 2023

@author: henry
"""

# =============================================================================
# This script plots trajectories based on whether they were diabatically heated
# or not. Usage: python main.py <directory with trajectory data> <Year> <Month>
# <Day> <Hour> <plot_option>
# =============================================================================

# %% library imports

# This will move the console to the right working directory.
from os.path import  dirname, abspath
from os import chdir, getcwd
chdir(dirname(abspath(__file__)))

from sys import argv, path, exit
path.append("./LIB")
import plot as pp
import calc as cc
# import data as dd
from datetime import datetime
import numpy as np
from itertools import product

# %% get the data

if argv[0]=='':
    date_0 = datetime(2016,5,2,0)
    tradir = date_0.strftime("/net/scratch/schoelleh96/WP2/WP2.1/" +
                             "LAGRANTO/wp21/era5/traj/%Y/")
else:
    tradir = argv[1]
    date_0 = datetime(int(argv[2]),int(argv[3]),int(argv[4]),int(argv[5]))
    from matplotlib import use
    use('agg')

print("cwd: ", getcwd())
print("tradir:", tradir)
print("Date: ", date_0)

trajs = np.load(tradir + date_0.strftime('traj_%Y%m%d_%H') + ".npy")

# %% calculate diabatic heating

# dtfrom matplotlib import useh7 = cc.dth_minmax(trajs['TH'],168)
# dth3 = cc.dth_minmax(trajs['TH'],72)


# %% Calculate coherent sets
if argv[6] in ["split4", "set_sep4", "set_comb3d", "save_D", "traj"]:

    Lon = trajs['lon']
    Lat = trajs['lat']
    p = trajs['p']

    # H = cc.ptoh(trajs['p'].data, 1013.25, 8.435) # standard atmosphere

    # X, Y, Z = cc.coord_trans(trajs['lon'].data, trajs['lat'].data,
    #                          trajs['p'].data, v_trans="std_atm",
    #                          scaling=-1)

    # dom, kclust = cc.calc_cs(X, Y, Z)

    # do statistics
    #dd.save_to_file(date_0, trajs, dth3, dth7, kclust)

    pp.plot_wrapper(trajs, None, date_0, "traj", None)

    exit()
    if argv[6] == "save_D":
        bounds = cc.get_bounds(Lon[:,0], Lat[:,0], p[:,0])
        print("{} of {} points are boundary".format(bounds.sum(), len(bounds)))

        for i in product([1, 10, 100, 1000, -1],
                         [1000, 2000, 5000, 10000, 20000]):
            if i[0] == -1:
                points_1 = np.array([Lat[:,1:].flatten(),
                                  Lon[:,1:].flatten()]).T
                points_0 = np.array([Lat[:,:-1].flatten(),
                                  Lon[:,:-1].flatten()]).T
                haveDist =  np.mean(haversine_vector(points_0, points_1))

                dH = np.mean(abs(H[:,1:] - H[:,:-1]))
                k = haveDist/dH
            else:
                k = i[0]
            D = cc.distances(np.deg2rad(Lon), np.deg2rad(Lat), i[1],
                             Lon.shape[0],
                             Lon.shape[1], dataZ=H, customMetric=True, k=k)
            dd.save_D(D, k, date_0, i[1])

else:
    pp.plot_wrapper(trajs, dth3, date_0, argv[6])
