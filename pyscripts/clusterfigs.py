#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  3 15:07:16 2024

@author: schoelleh96
"""

locale.setlocale(locale.LC_TIME, 'en_US')

plt.rcParams['text.usetex'] = True
plt.rcParams['pgf.texsystem'] = "pdflatex"
plt.rcParams['text.latex.preamble'] = (
    r'\usepackage{amsmath,amsfonts,amssymb,cmbright,standalone}')

fig, ax = plt.subplots(1,1, figsize=(3., 2.))
pp.plot_spectra(D, 3*np.sqrt(epsilon_opt), epsilon_opt, lon, lat, p, k_p,
                ax, boundscs, True, path + "spnalp" + str(alpha),
                t_sel, "p")
plt.savefig("/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/im/" +
            "clustersnapshots/spectrum20170126_18.pdf", dpi=300,
            bbox_inches="tight")

# %%

plt.rcParams['font.size'] = 20

c = kclust.labels_
fig = plt.figure()
ax3d = fig.add_axes([0,0,1,1], projection="3d")
X, Y, Z = Xcs, Ycs, Zcs
X, Y, Z = X[c!=0], Y[c!=0], Z[c!=0]
c = c[c!=0]
c[c==5] = 0
minxy = np.min([np.min(X), np.min(Y)])
maxxy = np.max([np.max(X), np.max(Y)])
ax3d.set_xlim((minxy, maxxy))
ax3d.set_ylim((minxy, maxxy))
# ax3d.set_xlim((np.min(X), np.max(X)))
# ax3d.set_ylim((np.min(Y), np.max(Y)))
ax3d.set_zlim((np.min(Z), np.max(Z)))

set1_colors = plt.get_cmap('Set1').colors
custom_colors = [set1_colors[i] for i in [0, 1, 2, 3, 4]]
from matplotlib.colors import ListedColormap
custom_cmap = ListedColormap(custom_colors)

points = ax3d.scatter(X[:,plot_0],Y[:,plot_0],Z[:,plot_0],c=c,
                       cmap=custom_cmap,
                      # cmap="Set1",
                      alpha=0.2, s=16)
points = ax3d.scatter(X[:,t_i],Y[:,t_i],Z[:,t_i],c=c,
                       cmap=custom_cmap,
                      # cmap="Set1",
                      alpha=0.2, s=16)
# points = ax3d.scatter(X[c!=0,t_i],Y[c!=0,t_i],Z[c!=0,t_i],c=c,
#                       cmap=custom_cmap,
#                       #cmap="Set1",
#                       alpha=0.2, s=16)
for i, c_i in enumerate(np.unique(c)):#[c!=2])):
    mean_x = np.mean(X[c==c_i, :], axis=0)
    mean_y = np.mean(Y[c==c_i, :], axis=0)
    mean_z = np.mean(Z[c==c_i, :], axis=0)
    ax3d.plot(mean_x, mean_y, mean_z, color=points.to_rgba(c_i))
xb, yb, _ = cc.coord_trans(B[1], B[2], B[1], "None", 1,
                           proj="stereo")
ax3d.contour(xb, yb, B[0][int(72/6),:,:], [0.5], zdir="z",
             offset=1000,
             colors="k")
coast = coasts
for lon, lat in coast:
    xc, yc, _ = cc.coord_trans(lon, lat, lon, "None", 1,
                               proj="stereo")
    z = [1000] * len(xc)
    ax3d.plot(xc, yc, z, color='grey')
    # This will connect the points as line segments

# longitudes = np.arange(-270, -91, 60)
# longitudes = np.arange(-60, 121, 60)
longitudes = np.arange(-180, 181, 60)

latitudes = np.arange(10, 91, 20)
# Plot longitudes (meridians)
for lon in longitudes:
    lats = np.linspace(latitudes[0], latitudes[-1], 180//20 + 1)
    lons = np.full(lats.shape, lon)
    xc, yc, _ = cc.coord_trans(lons, lats, lons, "None", 1,
                               proj="stereo")
    z = np.full(xc.shape, 1000)
    ax3d.plot(xc, yc, z, linestyle='--', color='grey')

# Plot latitudes (parallels)
for lat in latitudes:
    if abs(lat) == 90:
        continue  # Skip poles
    lons = np.linspace(longitudes[-1], longitudes[0], 360//2 + 1)
    lats = np.full(lons.shape, lat)
    xc, yc, _ = cc.coord_trans(lons, lats, lons, "None", 1,
                               proj="stereo")
    z = np.full(xc.shape, 1000)
    ax3d.plot(xc, yc, z, linestyle='--', color='grey')
ax3d.invert_zaxis()
for lon, lat in coast:
    xc, yc, _ = cc.coord_trans(lon, lat, lon, "None", 1,
                               proj="stereo")
    z = [1000] * len(xc)
    ax3d.plot(xc, yc, z, color='grey')
    # This will connect the points as line segments

ax3d.set_xticks([])

ax3d.set_yticks([])
ax3d.xaxis.label.set_visible(False)
ax3d.yaxis.label.set_visible(False)
ax3d.zaxis.labelpad = 15  # Increase the value to move the label further away
ax3d.set_zlabel("$p$ [hPa]")
ax3d.xaxis._axinfo['grid'].update({'visible': False})
ax3d.yaxis._axinfo['grid'].update({'visible': False})
ax3d.xaxis.pane.fill = False
ax3d.yaxis.pane.fill = False
ax3d.zaxis.pane.fill = False

ax3d.spines['left'].set_visible(False)

ax3d.spines['right'].set_visible(False)
ax3d.spines['bottom'].set_visible(False)
ax3d.spines['top'].set_visible(False)

# 20160502
# text_coords = {0: [-3000, 3200, 800], 1: [-500, 3500, 900],
#                2: [-1300, 3600, 200],
#                3: [2000, 4400, 900], 4: [-3700, 4600, 400],
#                5: [-4000, 4400, 500]}

# # 20160504
# text_coords = {0: [2000, 4200, 180], 1: [3500, 4000, 380],
#                 2: [1000, -3000, 550],
#                 3: [1000, -1000, 450]}

# 20160126
text_coords = {0: [-3500, -1000, 50], 1: [-4500, -6000, 900],
                2: [-4000, -6000, 400],
                3: [500, -2000, 450], 4: [-1500, -2000, 500]}

for i in np.unique(invclean):
    x, y, z = text_coords[i]
    ax3d.text(x, y, z, f'{i}', fontsize = 20,
              color=colors[i], bbox=dict(facecolor='white',
                                         edgecolor='black',
                                         boxstyle='round,pad=0.1',
                                         alpha=None))

# ax3d.zaxis._axinfo['label']['space_factor'] = 5

plt.savefig("/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/" +
            "im/clustersnapshots/20170126_18_3d.pdf")

# %%

plt.rcParams['font.size'] = base_font_size

datclean = dat[:, :]
invclean = inv[:]
plt.rcParams['font.size'] = 10
fig, ax = plt.subplots(1,1, figsize=(3., 2.4))
# #theta
# text_coords = {0: [-25, (12, -17)], 1: [-25, (0, 12)], 2: [-25, (4, 12)],
#                 3: [-10, (5, -15)], 4: [-16, (0, -14)],
#                 5: [-30, (-10, -10)]}
# q
# text_coords = {0: [-29, (10, 20)], 1: [-50, (10, 0)], 2: [-55, (0, 20)],
#                3: [-45, (0, -9)], 4: [-30, (-5, 20)],
#                5: [-18, (-15, 5)]}

# #u
# text_coords = {0: [-121, (12, 17)], 1: [-35, (-30, 30)],
#                2: [-85, (-10, 20)],
#                 3: [-20, (0, -30)]}

# #v
# text_coords = {0: [-22, (-20, 25)], 1: [-22, (-20, 25)],
#                2: [-95, (-10, 20)],
#                 3: [-20, (0, -30)]}

#theta2017
text_coords = {0: [-65, (-5, 5)], 1: [-40, (20, -25)],
               2: [-55, (0, -15)],
                3: [-2, (-5, -30)],
                4: [-10, (-5, -40)]}

pp.plot_spaghetti(ax, datclean, alpha_spag, colors=colors, inv=invclean,
                  text_coords=text_coords)
ax.legend_.remove()
ax.set_ylabel("$\\theta$ [K]")
# ax.set_ylabel("$q$ [g kg\\textsuperscript{-1}]")
# ax.set_ylabel("$v$ [m s\\textsuperscript{-1}]")

# ax.set_ylabel("$v$ [m s\\textsuperscript{-1}]")
# plt.savefig("/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/im/" +
#             "clustersnapshots/20160502_00_theta.pdf", bbox_inches="tight")
# plt.savefig("/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/im/" +
#             "clustersnapshots/20160504_00_v.pdf", bbox_inches="tight")
plt.savefig("/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/im/" +
            "clustersnapshots/20170126_18_theta.pdf", bbox_inches="tight")


# %%
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import pandas as pd

def createPolarPlotWithSpecialMarkers(trajs, inv, cmap, text_coords=None):
    """
    Create a polar plot showing the temporal development of median U and V
    velocity components, with special markers at the first timestep and then
    every 24 timesteps. Each marker type appears only once in the legend.

    Parameters:
    - trajs: Dictionary containing 'U' and 'V' keys with ndarray of shape (m, n_t).
    - inv: An ndarray of size m indicating cluster assignment for each trajectory.
    - colors: A dictionary mapping cluster labels to color strings.
    """
    plt.figure(figsize=(3.4,3.4))
    ax = plt.subplot(111, polar=True)
    legend_handles = []
    # Plot lines and special markers for each cluster
    for cluster in np.unique(inv):
        idx = np.where(inv == cluster)[0]
        color = cmap(cluster)

        # Calculate median U and V for the cluster
        median_u = np.median(trajs['U'][idx,:], axis=0)
        median_v = np.median(trajs['V'][idx,:], axis=0)

        # Convert to polar coordinates
        angles = np.arctan2(median_u, median_v)
        radii = np.sqrt(median_u**2 + median_v**2)

        # Plot the trajectory line for the cluster
        ax.plot(angles, radii, label=f'Cluster {cluster}', color=color)
        markers = ['o', 's', '^', '*', 'v', 'X', 'p', 'h']
        # Plot special markers
        for i in range(len(angles)):
            if i % 24 == 0:
                marker = markers[int(i/24)]
                ax.plot(angles[i], radii[i], marker=marker, markersize=8,
                        linestyle='None', color=color)
                if cluster == np.unique(inv)[0]:  # Add legend entry only for the first cluster
                    legend_handles.append(mlines.Line2D([], [], color='k',
                                                    marker=marker,
                                                    markersize=10,
                                                    label=f'${i-72}$',
                                                    linestyle='None'))
        if not (text_coords is None):
            angle_i = angles[text_coords[cluster][0]]
            radius_i = radii[text_coords[cluster][0]]
            ax.annotate(f'{cluster}', xy=(angle_i, radius_i),
                                    xytext=text_coords[cluster][1],
                                    textcoords='offset points',
                                    color=color,
                                    arrowprops=dict(arrowstyle="-",
                                                    color=color))

    plt.legend(handles=legend_handles, loc='best', title="$t$ [h]",
               borderpad = .6, framealpha=1,
               labelspacing = 0.25, handletextpad = .75, borderaxespad = 0,
               handleheight = 1, handlelength = .2)

    # Example wind speeds to label and their angles (just for demonstration)
    wind_speeds = [10, 20, 30, 40]  # Example wind speeds
    angles_for_labels = 4* [0]  # Example angles where you want to place labels
    for wind_speed, angle in zip(wind_speeds, angles_for_labels):
            label = f"${wind_speed}$" + " $\\mathrm{ms}^{-1}$"  # LaTeX-style formatting
            ax.annotate(label, xy=(angle, wind_speed), xytext=(angle, wind_speed),
                        textcoords='data', ha='center', va='center',
                        arrowprops=dict(arrowstyle="->", connectionstyle="arc3"))
    # Adjust plot appearance
    ax.set_theta_zero_location('N')
    ax.set_theta_direction(-1)
    ax.set_yticklabels([])

    # Assuming `ax` is your polar axes and you want to label specific angles
    # Example angles: 0, π/2, π, 3π/2
    angles_rad = np.linspace(0,2*np.pi, 8, endpoint=False)
    angle_labels = [f'${d}^{{\circ}}$' for d in np.linspace(0, 360, 8, endpoint=False).astype(int)]
    ax.set_xticks(angles_rad)  # Set the angles where you want the labels
    ax.set_xticklabels(angle_labels)  # Set the LaTeX formatted labels
    plt.savefig("/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/im/" +
                "clustersnapshots/20170126_18_uvpolar.pdf")

# %%

mapping = {0: 3, 1: 2, 2: 1, 3: 0, 4: 4, 5:5}

# Apply the mapping using list comprehension
invclean = np.array([mapping[label] for label in inv])

text_coords = {0: [-58, (-15, 5)], 1: [-5, (0, -15)],
               2: [-2, (-15, -15)],
                3: [-35, (-15, 00)],
                4: [-10, (-4, -40)]}

createPolarPlotWithSpecialMarkers(trajs[inv!=5, :][:, t_sel], invclean, custom_cmap,
                                  text_coords=text_coords)

