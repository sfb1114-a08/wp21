#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 15:10:45 2024

@author: schoelleh96
"""
from os.path import  dirname, abspath, exists
from os import chdir
chdir(dirname(abspath(__file__)))

from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import pickle
import pandas as pd
import seaborn as sb
import numpy as np
import matplotlib.dates as mdates

# %% Initials

date_0 = "all"

k_p = 15

base_font_size = 10 # pt

plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.sans-serif'] = ['Helvetica']
plt.rcParams['font.size'] = base_font_size
plt.rcParams['text.latex.preamble'] = r'\usepackage{amsmath,amsfonts,amssymb}'



def get_data(YEAR):

    # Initialize dates
    if YEAR == "2016":
        dates = [datetime(2016, 4, 30, 0), datetime(2016,5,2,0),
                  datetime(2016,5,4,0)]
        dates = [datetime(2016, 4, 29, 6) + timedelta(hours=i)
                  for i in range(0, 144, 6)]
        # dates = [datetime(2016,5,6,0), datetime(2016,5,7,0)]
    elif YEAR == "2017":
        dates = [datetime(2017, 1, 23, 18) + timedelta(hours=i)
                  for i in range(0, 28*6, 6)]

    if date_0 == "all":

        Sum_list = list()
        for date in dates:
            if exists("pickle/" + date.strftime('%Y%m%d_%H') + "S.pkl"):
                with open("pickle/" + date.strftime('%Y%m%d_%H')
                          + "S.pkl", "rb") as f:
                    S = pickle.load(f)
                    # plot_epsloglog(S, date.strftime('%Y%m%d_%H'))
            Sum_list.append(S)
        Sums = pd.concat(Sum_list, ignore_index=True)

    else:
        with open("pickle/" + date_0 + "S.pkl", "rb") as f:
            Sums = pickle.load(f)

    Sums['t'] = Sums['t']-72

    def calc_dd(group):
        leps = np.log(group["eps"]).reset_index(drop=True)
        lS = np.log(group["sums"]).reset_index(drop=True)

        # This is the derivative of log(S(eps) with respect to log(eps):
        dlS = lS.diff().dropna() / leps.diff().dropna()
        # We assume that the steps in the logarithm of eps are uniform

        e_max = np.argmax(dlS)
        d = 2 * dlS[e_max]

        density = ((lS[e_max] - np.log(group["m"].reset_index(drop=True)[0]))/d -
                   (leps[e_max] + np.log(2*np.pi))/2)
        rho = density * d
        return pd.Series({"dimension": d, "density": density, "rho" : rho})

    Densities = Sums.groupby(["date", "t"]).apply(calc_dd).reset_index()

    Densities['date'] = pd.to_datetime(Densities['date'], format='%Y%m%d_%H')

    if YEAR=="2017":
        skiprows = 28
        nrows = 57
    else:
        skiprows = 4
        nrows = 24

    def read_and_prepare_data(fpath):
        cols = pd.read_csv(
            fpath, sep='\s*\|\s*', skiprows=2, nrows=1,
            header=None, engine='python'
        ).iloc[0].str.strip().tolist()
        cols = [col for col in cols if col != '|']

        df = pd.read_csv(
            fpath, sep='\s*\|\s*', skiprows=skiprows, nrows=nrows,
            header=None, engine='python'
        )
        df.columns = cols
        df = df.drop(df.columns[[0, -1]], axis=1)
        df['date'] = pd.to_datetime(df['date'], format='%Y%m%d_%H')

        return df

    fpath = "/home/schoelleh96/Nextcloud/ObsidianVault/Work/Projects/n_t.md"

    df1 = read_and_prepare_data(fpath)

    # Step 1: Adjust Datetime in `densities`
    Densities['date'] = Densities.apply(
        lambda row: row['date'] + pd.DateOffset(hours=row['t']), axis=1
    )

    # Step 2: Merge DataFrames using an inner join
    merged_df = pd.merge(
        df1,
        Densities,
        left_on='date',
        right_on='date',
        how='inner'
    )


    # Convert dates to a numeric value (e.g., days since the start of the dataset)
    merged_df['date_numeric'] = (merged_df['date'] - merged_df['date'].min()).dt.days

    return merged_df

df_list = list()
for YEAR in ["2016", "2017"]:
    df_list.append(get_data(YEAR))

df = pd.concat(df_list, ignore_index=True)


# %%  Plotting

fig, ax = plt.subplots(1, 2, figsize=(8, 3), sharey=True)

for i,d in enumerate(["dimension", "density"]):
    # Step 3: Plotting with a colorbar
    scatter_plot = sb.scatterplot(
        data=df,
        y='n_t',
        x=d,
        hue='t',
        palette='viridis',  # Using viridis colormap
        size=3,
        ax=ax[i],
        edgecolor='none',  # Remove marker borders
    )
    if d == "dimension":
        ax[i].set_xlabel("$d$")
    elif d == "density":
        ax[i].set_xlabel("$\ell$")
    scatter_plot.legend_.remove()



ax[0].set_ylabel("$m$")
# Adding a colorbar
norm = plt.Normalize(vmin=df['t'].min(), vmax=df['t'].max())
sm = plt.cm.ScalarMappable(cmap='viridis', norm=norm)
sm._A = []  # ScalarMappable needs a dummy array attribute '_A' to work with colorbar
fig.colorbar(sm, ax=ax, orientation='vertical', label='$t$[h]')

plt.show()
plt.savefig("/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/im/" +
            "mvd/mvd" + date_0 + "both" + ".pdf", bbox_inches="tight")