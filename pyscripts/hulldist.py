#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  8 17:15:57 2023

@author: schoelleh96
"""

# %% library imports

# This will move the console to the right working directory.
from os.path import  dirname, abspath, exists
from os import chdir, makedirs
chdir(dirname(abspath(__file__)))

from sys import argv, path
import numpy as np
path.append("./LIB")
import plot as pp
import calc as cc
import data as dd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import cmcrameri.cm as cmc
from datetime import datetime
from sklearn.neighbors import BallTree
from scipy.sparse import csc_matrix
import pandas as pd
import seaborn as sb
import time
from matplotlib import use
import locale
# use('agg')
plt.rcParams["axes.grid"] = False
locale.setlocale(locale.LC_TIME, 'en_US')

plt.rcParams['text.usetex'] = True
plt.rcParams['pgf.texsystem'] = "pdflatex"
plt.rcParams['text.latex.preamble'] = (
    r'\usepackage{amsmath,amsfonts,amssymb,cmbright,standalone}')
plt.rcParams['font.size'] = 10

# %% Get the data


def curate_data(data, bins):
    """
    Prepare histogram data and calculate statistics for each array in data.

    Parameters
    ----------
        data (list of np.ndarray): List of numpy arrays,
        bins (np.ndarray): Array of bin edges for histograms.

    Returns
    -------
        tuple: A tuple containing the histogram matrix and a list of statistics
    """
    hist_data = np.zeros((len(data), len(bins) - 1))
    stats = []

    for i, array in enumerate(data):
        hist, _ = np.histogram(array, bins=bins)
        hist_data[i, :] = hist

        # Calculate statistics
        #  mean_val = np.mean(array)
        median_val = np.median(array)
        q1, q3 = np.percentile(array, [25, 75])
        #  p5, p95 = np.percentile(array, [5, 95])

        # Convert stats to bin indices
        stats.append((np.digitize([median_val, q1, q3], # , p5, p95],
                                  bins) - 1))

    return hist_data, stats


dates = [datetime(2016, 5, 2, 0), datetime(2016, 5, 4, 0),
         datetime(2017, 1, 26, 18)]

Mins_list = list()

for date_0 in dates:

    Mins = pd.DataFrame(columns=['date', 't', 'dist', 'bound'])

    calcNN = False
    # date_0 = datetime(2017, 1, 26, 18)


    # only every nth trajectory
    n = 1

    trapath = date_0.strftime("../era5/traj/%Y/")

    path = '../../csstandalone/'

    path = (path + date_0.strftime("dat_%Y%m%d_%H") + "/"
            + str(n) + "/")

    if not exists(path):
        makedirs(path)

    trajs = np.load(trapath + "traj_" + date_0.strftime('%Y%m%d_%H') + ".npy")

    t_sel = np.arange(0, trajs.shape[1])

    Lon = trajs['lon'][::n]
    Lat = trajs['lat'][::n]
    P = trajs['p'][::n]
    U = trajs['U'][::n]
    V = trajs['V'][::n]
    Omg = trajs['OMEGA'][::n]
    T = trajs['T'][::n]

    k_p = cc.calc_k(U/1000, V/1000, Omg/100)
    H = cc.ptoh(P, 1013.25, 8.435)  # standard atmosphere
    k_h = cc.calc_k(U, V, cc.omg2w(Omg/100, T, P))

    k_p = 15

    # Calculate distances


    def unnorm(x_sd, x):
        return (x_sd*x.std(axis=0) + x.mean(axis=0))


    eps = 1e6
    r = 3*np.sqrt(eps)
    alpha = 1e-3

    # xun, yun, zun = cc.coord_trans(Lon, Lat, P, "None", proj="stereo", scaling = 1)
    # x, y, z = cc.norm(xun), cc.norm(yun), cc.norm(zun)

    x, y, z = cc.coord_trans(Lon, Lat, P, "None", k_p, proj="stereo")

    # # Test inverse operation:
    # xin = unnorm(x, xun)
    # yin = unnorm(y, yun)

    # lonin, latin = cc.project(xun, yun, Inverse=True)
    # lonin[lonin<0] +=360
    # print(np.allclose(lon, lonin), np.allclose(lat, latin)): True True

    # get the boundary
    bounds, hulls = dd.io_bounds(path + "stereop",
                                 "$\\alpha$", x, y, z, alpha, force_calc=False)

    # get the distances

    # D = dd.io_dist(path, eps, Lon, Lat, P, True, k_p, "p", t_sel)

    x_list = list()
    y_list = list()
    v_list = list()

    d_list = list()

    # calculate boundary point distances
    for j in range(Lon.shape[1]):
        print("t = {}".format(j))
        if calcNN:
            # select only boundary points
            lon_t = Lon[bounds[:, j] == 1, j]
            lat_t = Lat[bounds[:, j] == 1, j]
            dat = np.array([np.deg2rad(lon_t), np.deg2rad(lat_t)]).T
            BT = BallTree(dat, metric='haversine')
            idx, hdist = BT.query_radius(dat, r=r / 6371,
                                         return_distance=True)
            hdist = hdist * 6371

            x_t = list()
            y_t = list()
            v_t = list()

            p_t = P[bounds[:,j] == 1,j]

            for i in range(lon_t.shape[0]):
                # for each point get nearest neighbors
                hdist[i] = hdist[i][idx[i] > i]
                idx[i] = idx[i][idx[i] > i]

                vdist = p_t[idx[i]] - p_t[i]

                dist = np.sqrt(np.power(hdist[i], 2) + k_p * np.power(vdist, 2))

                valid = np.where(dist < r)[0]
                x_t.extend([i for _ in range(len(valid))])
                y_t.extend(idx[i][valid])
                v_t.extend(dist[valid])

            x_t = np.asarray(x_t)
            y_t = np.asarray(y_t)
            v_t = np.asarray(v_t)

            x_list.append(np.int_(x_t))
            y_list.append(np.int_(y_t))
            v_list.append(v_t)
        else:
            # Legacy: too slow

            # array of size n_points x 3
            # points_t = np.array([x[:,j], y[:,j], z[:,j]]).T
            # def find_point_idx(pt):
                # return np.where([np.allclose(pt, r) for r in points_t])[0][0]
            # Since (edges_t.reshape(-1,3).reshape(-1, 2, 3) ==
            # edges_t).flatten().all() is True, we can flatten and reshape
            # start = time.time()
            # p_idx_t = np.apply_along_axis(find_point_idx, 1,
                                          # edges_t.reshape(-1,3)).reshape(-1, 2)
            # diff = time.time() - start
            # print(diff) # 167s

            # Note that (points_t[p_idx_t.reshape(-1), :].reshape(-1, 2, 3) ==
            # edges_t).all() is True.
            # We now simply apply the indices to the points in Lon/Lat/P
            # points_llp = np.array([Lon[:,j], Lat[:,j], P[:,j]]).T
            # points_llp_bound = points_llp[p_idx_t.reshape(-1), :].reshape(-1, 2, 3)

            # start = time.time()

            # array of size n_edges x 2 x 3
            # edges_t = hulls[j].vertices[hulls[j].edges_unique]

            # edge_lengths = np.linalg.norm(edges_t[:, 0] - edges_t[:, 1], axis=1)

            edge_lengths = hulls[j].edges_unique_length

            # xbd = edges_t.reshape(-1, 3)[:, 0]
            # ybd = edges_t.reshape(-1, 3)[:, 1]
            # pbd = edges_t.reshape(-1, 3)[:, 2]
            # xbdin = unnorm(xbd, xun[:,j])
            # ybdin = unnorm(ybd, yun[:,j])
            # pbdin = unnorm(pbd, zun[:,j])

            # lonbd, latbd = cc.project(xbd, ybd, Inverse=True)
            # lonbd[lonbd < 0] += 360
            # points_llp_bound = np.array((lonbd, latbd, pbd)).T.reshape(-1, 2, 3)
            # diff = time.time() - start

            # print(diff) # 0.032s

            # Now we can calculate the edgelength in custom metric units
            # dist_bound = cc.adj_dist_vec(
                # np.array([points_llp_bound[:, 0, 1], points_llp_bound[:, 0, 0],
                          # points_llp_bound[:, 0, 2]]),
                # np.array([points_llp_bound[:, 1, 1], points_llp_bound[:, 1, 0],
                          # points_llp_bound[:, 1, 2]]), 1)
            # use k=1, bc p already scaled
            d_list.append(edge_lengths)
            #        d_list.append(dist_bound)

    # D_bound = (x_list, y_list, v_list)

    # 2D Histogram: heatmap


    def plot_heatmap(hist_data, stats, bins, date_0, t_sel):
        plt.figure(figsize=(2, 2))
        ax = sb.heatmap(hist_data, cmap=cmc.hawaii, rasterized=True,
                        linecolor="none", linewidths=0,
                        cbar_kws={"orientation": "horizontal",
                                  "pad": 0.05})

        ax.grid(False)

        plt.xlabel('Edge length [km]')
        ax.yaxis.set_major_locator(
                ticker.FixedLocator(t_sel + t_sel.max()))
        if date_0 == datetime(2016, 5, 2, 0):
            print("true")
            plt.ylabel('$t$ [h]')
            ax.set_yticklabels([f"{t.astype(int)}" for t in t_sel])
            from matplotlib.lines import Line2D
            legend_elements = [Line2D([0], [0], color="black", lw=2,
                                      label=('$P_{25},$ \n $P_{50}$, \n $P_{75}$'))]
            ax.legend(handles=legend_elements, loc='right')
        else:
            plt.ylabel('')
            # ax.set_yticks([])  # Remove y-axis ticks
            ax.set_yticklabels([])  # Remove y-axis tick labels

        # X-axis ticks and labels
        mid_bins = 0.5 * (bins[:-1] + bins[1:])  # Mid-points of the bins
        tick_spacing = len(bins) // 5  # Defining tick spacing for better visibility

        # Defining the tick positions
        tick_positions = bins[::tick_spacing]/bins.max() * (len(bins) - 1)
        # Select ticks at intervals
        # Ensure the number of tick labels matches the number of tick positions
        tick_labels = [f"{int(bins[i])}"
                       for i in range(0, len(bins), tick_spacing)]

        # Setting the ticks and labels on the x-axis
        ax.xaxis.set_major_locator(ticker.FixedLocator(tick_positions))  # Set ticks
        ax.set_xticklabels(tick_labels, rotation=45)  # Set labels
        ax.set_xlabel("Edge Length [km]")
        ax.xaxis.set_ticks_position('top')
        ax.xaxis.set_label_position('top')

        # Add lines for statistics
        colors = ["k", "k", "k", "k"]  # , "k", "k"]
        for index, stat in enumerate(stats):
            for i, val in enumerate(stat):
                plt.axvline(x=val, ymin=index / len(hist_data),
                            ymax=(index + 1) / len(hist_data), color=colors[i],
                            linewidth=1)

        # Adding a colorbar label
        cbar = ax.collections[0].colorbar
        cbar.set_label('Frequency [-]')

        plt.show()
        plt.savefig(f"../im/bounds/bounds_{date_0.strftime('%Y%m%d_%H')}.pdf",
                    bbox_inches="tight")


    bins = np.linspace(0, 1e3, 51)  # Define bins suitable for your data

    # Data curation
    hist_data, stats = curate_data(d_list, bins)

    # Plotting
    print(date_0)
    plot_heatmap(hist_data, stats, bins, date_0, t_sel[::24]-72)

# exit()

# %% Violinplot: distance in km vs t; diff by points in boundary and not

Mins_list = list()

for j in range(Lon.shape[1]):
    print(j)
    K = csc_matrix((D[2][j], (D[0][j], D[1][j])), shape=(Lon.shape[0],
                                                          Lon.shape[0]))
    K = K + K.T
    K = K.toarray()
    K[K==0] = np.nan
    mins = np.nanmin(K, axis=0)

    Mins_list.append(pd.DataFrame({'t' : j,
                              'dist' : mins,
                              'bound' : False}))
    if calcNN:
        K_bound = csc_matrix((v_list[j], (x_list[j], y_list[j])),
                             shape=(Lon[bounds[:,j] == 1,j].shape[0],
                                    Lon[bounds[:,j] == 1,j].shape[0]))
        K_bound = K_bound + K_bound.T
        K_bound = K_bound.toarray()
        K_bound[K_bound==0] = np.nan
        mins = np.nanmin(K_bound, axis=0)

        Mins_list.append(pd.DataFrame({'t' : j,
                                  'dist' : mins,
                                  'bound' : True}))
    else:
        Mins_list.append(pd.DataFrame({'t' : j,
                                  'dist' : d_list[j],
                                  'bound' : True}))

Mins = pd.concat(Mins_list, ignore_index=True)

Mins['T'] = (Mins['t']/18).astype(int)

f = sb.FacetGrid(Mins, row="bound", sharey=False)
f.map_dataframe(sb.histplot, x="t", y="dist", discrete=(True, False))

#from matplotlib import use
#use('agg')

if calcNN:
    f = sb.violinplot(data=Mins, x="T", y="dist", hue="bound", split=True,
                  inner="quart")
else:
    f, ax = plt.subplots(2, 1, figsize=(14,10))
    # sb.violinplot(data=Mins.loc[Mins["bound"] == True,:], x="T", y="dist",
    #               inner="quart", ax=ax[0], cut = 0.5)
    # sb.violinplot(data=Mins.loc[Mins["bound"] == False,:], x="T", y="dist",
    #               inner="quart", ax=ax[1], cut=0.5)
    sb.boxplot(data=Mins.loc[Mins["bound"] == True,:], y="T", x="dist",
               ax=ax[0], orient="h")
    sb.boxplot(data=Mins.loc[Mins["bound"] == False,:], y="T", x="dist",
               ax=ax[1], orient="h")
    ax[0].set_title("Boundary Triangle Edges")
    ax[1].set_title("All Points NN")
    plt.savefig(path + ('BoundDist.png'))

# f, ax = plt.subplots(2, 1, figsize=(14,10))
# # sb.violinplot(data=Mins.loc[Mins["bound"] == True,:], x="T", y="dist",
# #               inner="quart", ax=ax[0], cut = 0.5)
# # sb.violinplot(data=Mins.loc[Mins["bound"] == False,:], x="T", y="dist",
# #               inner="quart", ax=ax[1], cut=0.5)
# sb.boxplot(data=Mins.loc[Mins["bound"] == True,:], y="T", x="dist",
#            ax=ax[0], orient="h", hue="kp")
# sb.boxplot(data=Mins.loc[Mins["bound"] == False,:], y="T", x="dist",
#            ax=ax[1], orient="h", hue="kp")
# ax[0].set_title("Boundary Triangle Edges")
# ax[1].set_title("All Points NN")
# plt.savefig('BoundDistAll.png')
