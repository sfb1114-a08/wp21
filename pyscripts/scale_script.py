#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 10:13:07 2023

@author: schoelleh96
"""

# =============================================================================
# This script plots the changes in lon/lat/p made by individual air parcels
# along given trajectories.
# =============================================================================

# %% imports

# This will move the console to the right working directory.
from os.path import  dirname, abspath
from os import chdir, getcwd
chdir(dirname(abspath(__file__)))

from lagranto import Tra
from LIB.PLOT import *
from LIB.CALC import *

from matplotlib.pyplot import savefig
# %% Get the data

tradir = "/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/traj/1981/"
traname = "traj_19810110_00"

traj = Tra()
traj.load_ascii(tradir+traname, gz=False)
traj.set_array(traj.get_array().transpose()[:,:169]) # seven days only
print(traj)

H = ptoh(traj['p'].data, 1013.25, 8.435)

(X, Y) = dlonlat_dxy(0, traj['lon'].data, 0, traj['lat'].data)

fig = plot_scales(H, X=X)

fig.suptitle("X/Z-changes in 12 hours")

savefig("./im/scales/xzscales.png")

fig = plot_scales(H, Y=Y)

fig.suptitle("Y/Z-changes in 12 hours")

savefig("./im/scales/yzscales.png")