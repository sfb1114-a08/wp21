#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 12 15:23:31 2024

@author: schoelleh96
"""

# This will move the console to the right working directory.
import os
os.chdir(os.path.dirname(os.path.abspath(__file__)))

import pickle
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb
import matplotlib.colors as mcolors
import matplotlib.dates as mdates
import numpy as np
import matplotlib.collections as mcoll
import matplotlib.lines as mlines
# %% get data

wcb_path = "wcbs/"

wcbs = [f for f in os.listdir(wcb_path) if
        os.path.isfile(os.path.join(wcb_path, f))]
wcbs.sort()

YEAR = 2016

def get_data(wcbs, YEAR):
    i = 0
    wcb_list = list()
    for wcb in wcbs:
        if wcb[:4] == str(YEAR):
            with open("wcbs/" + wcb, "rb") as f:
                df = pickle.load(f)
            df['id'] = i
            wcb_list.append(df)
            i += 1

    wcb_df = pd.concat(wcb_list, ignore_index=True)

    # Sort the DataFrame by time in descending order
    # wcb_df.sort_values(by='t', ascending=False, inplace=True)

    if YEAR==2017:
        skiprows = 28
        nrows = 57
    else:
        skiprows = 4
        nrows = 24

    def read_and_prepare_data(fpath):
        cols = pd.read_csv(
            fpath, sep='\s*\|\s*', skiprows=2, nrows=1,
            header=None, engine='python'
        ).iloc[0].str.strip().tolist()
        cols = [col for col in cols if col != '|']

        df = pd.read_csv(
            fpath, sep='\s*\|\s*', skiprows=skiprows, nrows=nrows,
            header=None, engine='python'
        )
        df.columns = cols
        df = df.drop(df.columns[[0, -1]], axis=1)
        df['date'] = pd.to_datetime(df['date'], format='%Y%m%d_%H')

        return df

    fpath = "/home/schoelleh96/Nextcloud/ObsidianVault/Work/Projects/n_t.md"

    df1 = read_and_prepare_data(fpath)
    df1['m'] = 0
    return wcb_df, df1

# %% plot

def plot_wcb(wcb_df, df1):

    # Create the plot
    fig, ax = plt.subplots(figsize=(3.4, 2.4))

    # Create a colormap
    cmap = plt.cm.viridis

    # Calculate the earliest starting time for each id
    start_times = wcb_df.groupby('id')['t'].min()
    # Normalize these times
    norm = mcolors.Normalize(vmin=start_times.min().timestamp(),
                             vmax=start_times.max().timestamp())

    # Plot each id's data
    for id_value in wcb_df['id'].unique():
        id_df = wcb_df.query('id == @id_value')

        # Determine line width based on mean of m for each id
        line_width = id_df['m'].iloc[0] / wcb_df['m'].max() * 2 + 1
        # Adjust scaling factor as needed

        # Get color for the id based on its starting time
        color = cmap(norm(start_times[id_value].timestamp()))

        # color = cmap(norm(id_df['m'].iloc[0]))
        # sb.lineplot(y='theta_m', x='t', data=id_df, color=color)
        sb.lineplot(x='t', y='theta_m', data=id_df, linewidth=line_width,
                    color=color, ax=ax, zorder = 2, alpha=1)
        # # Shade the interquartile range
        # plt.fill_between(x=id_df['t'], y1=id_df['theta_q1'], y2=id_df['theta_q3'],
        #                  color=color, alpha=0.3)

        df1.loc[df1['date'] == id_df['t'].iloc[-1], 'm'] += id_df['m'].iloc[-1]

    ax.set_ylabel("$\\theta$ [K]")
    ax.set_xlabel("Date")
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%d %b'))
    locator = mdates.AutoDateLocator(minticks=4, maxticks=6)  # Adjust maxticks as needed
    ax.xaxis.set_major_locator(locator)
    df1['ratio'] = df1['m']/df1['n_t']

    df1 = df1[df1['ratio'] != 0]
    # Create a secondary y-axis for the bar chart
    ax2 = ax.twinx()

    # Set labels for the secondary y-axis
    # ax2.set_ylabel('$\\sum_{\\Delta \\Theta > 5 \\mathrm{K}}m_i / \\sum m_i$')
    ax2.set_ylabel("WCB fraction [-]")
    # Creating a custom color gradient line at the bottom of the plot
    y_min = ax.get_ylim()[0]  # Get the minimum y-value of the plot
    n_segments = 500  # Number of line segments
    points = np.array([np.linspace(mdates.date2num(start_times.iloc[0]),
                                   mdates.date2num(start_times.iloc[-1]),
                                   n_segments),
                       [y_min]*n_segments]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)
    lc = mcoll.LineCollection(segments, cmap=cmap, norm=norm, linewidth=10,
                              zorder=3, alpha=1)
    lc.set_array(np.linspace(start_times.min().timestamp(),
                             start_times.max().timestamp(), n_segments))
    ax.add_collection(lc)

    # Create custom legend entries for line widths
    # line_widths = [1, 3]  # Example line widths
    # line_labels = wcb_df['m'].max() * (np.array(line_widths)-1)
    line_labels = np.array([1000, 5000])
    line_widths = line_labels / wcb_df['m'].max() * 2 + 1

    def format_scientific(value):
        """Custom function to format number in scientific notation without '+' sign."""
        return "${:.0e}$".format(value).replace('e+0', 'e').replace('e+', 'e')
    # Generate legend lines with labels in scientific notation without '+'
    legend_lines = [mlines.Line2D([], [], color='black', lw=width,
                                  label=format_scientific(label))
                    for width, label in zip(line_widths, line_labels)]
    # Add legend to the plot
    if YEAR==2016:
        ax.legend(handles=legend_lines, title='$m_i$', loc="upper right",
                  bbox_to_anchor=(1, 1), labelspacing = 0,
                  handletextpad = 0.3, borderaxespad = 0.1,
                  handleheight = 0)
    elif YEAR==2017:
        ax.legend(handles=legend_lines, title='$m_i$', loc="lower left",
                  bbox_to_anchor=(.45, .05), labelspacing = 0,
                  handletextpad = 0.3, borderaxespad = 0.1,
                  handleheight = 0)

    # Plot the bar chart on the secondary y-axis
    ax2.bar(df1['date'], df1['ratio'], color='grey', alpha=.5, width=0.25,
            zorder = 1)
    ax2.set_ylim([0,1])

    for line in ax.get_lines():
        ax.draw_artist(line)

    ax.set_zorder(2)
    ax2.set_zorder(1)
    ax.patch.set_visible(False)

    plt.savefig("../im/wcbs/wcb_" + str(YEAR) + ".pdf", bbox_inches="tight")

    return fig, ax, ax2
    # Create a new axes for the colorbar, with controlled position
    # cbar_ax = fig.add_axes([0.91, 0.12, 0.02, 0.76])  # [left, bottom, width, height]

    # Create colorbar in the new axes
    # sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    # sm.set_array([])
    # cbar = plt.colorbar(sm, cax=cbar_ax)
    # cbar.set_label('Start Time')

# %%
wcb_df, nt_df = get_data(wcbs, YEAR)

f, a1, a2 = plot_wcb(wcb_df, nt_df)