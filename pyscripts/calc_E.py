#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 19 15:18:01 2023

@author: schoelleh96
"""

# %% library imports

# This will move the console to the right working directory.
from os.path import  dirname, abspath, exists
from os import chdir, makedirs
chdir(dirname(abspath(__file__)))

from sys import argv
import numpy as np
from LIB import plot as pp
from LIB import calc as cc
from LIB import data as dd
import matplotlib.pyplot as plt
from datetime import datetime
from matplotlib.widgets import (Slider, CheckButtons, RadioButtons,
                                RangeSlider, Button)

# %% Get the data

startkp = True

date_0 = datetime(2016,5,1,6)

epsilon_opt = np.array([25000, 50000, 100000, 250000, 500000])

# only every nth trajectory
for n in [1, 2, 5, 10]:

    if startkp:
        fname = date_0.strftime('trajkp_%Y%m%d_%H') + ".npy"
        path = "./" + date_0.strftime("datkp_%Y%m%d_%H") + "/" + str(n) + "/"
    else:
        fname = date_0.strftime('traj_%Y%m%d_%H') + ".npy"
        path = "./" + date_0.strftime("dat_%Y%m%d_%H") + "/" + str(n) + "/"

    print(n)

    trajs = np.load(fname)

    lon = trajs['lon'][::n]
    lat = trajs['lat'][::n]
    p = trajs['p'][::n]
    u = trajs['U'][::n]
    v = trajs['V'][::n]
    omg = trajs['OMEGA'][::n]
    T = trajs['T'][::n]

    # %% Initials

    H = cc.ptoh(p, 1013.25, 8.435) # standard atmosphere

    k_p = cc.calc_k(u/1000, v/1000, omg/100)
    k_h = cc.calc_k(u, v, cc.omg2w(omg, T, p))

    if not exists(path):
        makedirs(path)

    for epsilon in epsilon_opt:
        print(epsilon)
        for z_coord_type in ["p", "H"]:
            print(z_coord_type)

            if z_coord_type=="p":
                x, y, z = cc.coord_trans(lon, lat, p, "None", 1, proj="stereo")
                x, y, z = cc.norm(x), cc.norm(y), cc.norm(z)
                bounds, hulls = dd.io_bounds(path + "stereopnorm",
                                             "opt. $\\alpha$", x, y,
                                             z, 0.1)
                D = dd.io_dist(path, 100000, lon, lat, p, True, k_p, z_coord_type)
                E = dd.io_eigen(path + "spnopt", lon.shape[0], 0, lon.shape[1],
                                D[0], D[1], D[2], epsilon, bounds)
            else:
                x, y, z = cc.coord_trans(lon, lat, p, "std_atm", 1,
                                         proj="stereo")
                x, y, z = cc.norm(x), cc.norm(y), cc.norm(z)
                bounds, hulls = dd.io_bounds(path + "stereohnorm",
                                             "opt. $\\alpha$", x, y,
                                             z, 0.1)
                D = dd.io_dist(path, 100000, lon, lat, H, True, k_h,
                               z_coord_type)
                E = dd.io_eigen(path + "spnopt", lon.shape[0], 0, lon.shape[1],
                                D[0], D[1], D[2], epsilon, bounds)
                D = dd.io_dist(path, 100000, lon, lat, H, True, k_h, z_coord_type)
