#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 16 11:15:39 2023

@author: schoelleh96
"""

# This will move the console to the right working directory.
from os.path import  dirname, abspath
from os import chdir, getcwd
chdir(dirname(abspath(__file__)))

from LIB import plot as pp
from LIB import calc as cc
from LIB import data as dd
from sys import argv
from datetime import datetime
from lagranto import Tra
from numpy import arange, empty, c_, array
from matplotlib.pyplot import savefig, subplots
from matplotlib.ticker import FixedLocator, FixedFormatter

# %% get the data

if argv[0]=='':
    tradir = ("/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/" +
              "wp21/traj/1981/fineRes/")
    date_0 = datetime(1981,1,9,0)
else:
    tradir = argv[1]
    date_0 = datetime(int(argv[2]),int(argv[3]),int(argv[4]),int(argv[5]))

print("cwd: ", getcwd())
print("tradir:", tradir)
print("Date: ", date_0)

# define filename
traname = date_0.strftime('traj_%Y%m%d_%H')

trajs = Tra()
trajs.load_ascii(tradir + traname, gz=False)
trajs.set_array(trajs.get_array().transpose())#[:,:73])#three days only
print(trajs)

X = trajs['lon'].data
Y = trajs['lat'].data

dom, kclust = cc.calc_cs(X, Y)

shrink_fac = arange(2,21)

correct = empty((shrink_fac.shape[0] + 1, 2))
correct[0,:] = (1,1)

for count, s in enumerate(shrink_fac):

    sel_points = arange(0, trajs.get_array().shape[0], s)

    trajs_sparse = Tra()
    trajs_sparse.set_array(trajs.get_array()[sel_points, :])
    print(trajs_sparse)

    Xsp = trajs_sparse['lon'].data
    Ysp = trajs_sparse['lat'].data

    domsp, kclustsp = cc.calc_cs(Xsp, Ysp)

    correct[count+1,:] = (s,
                          (kclust.labels_[sel_points] ==
                           kclustsp.labels_).sum()/kclustsp.labels_.shape[0])

    print("{:.2f}/{:.2f}".format(correct[count, 1], kclustsp.labels_.shape[0]))

fig, ax = subplots(1,1, figsize=(10,8))
ax.bar(arange(0,shrink_fac.shape[0]+1), correct[:,1])
positions = arange(0,shrink_fac.shape[0]+1)
ax.xaxis.set_major_locator(FixedLocator(positions))
ax.xaxis.set_major_formatter(FixedFormatter(
    correct[:,0].astype(int).astype(str)))
ax.set_title("Trajectory Start Point Density Dependency")
ax.set_xlabel("Reduction Factor")
ax.set_ylabel("Relative Agreement")
savefig("./im/reductionAgreement.png")