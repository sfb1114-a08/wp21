#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  2 16:54:05 2023

@author: schoelleh96
"""
import PIL
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D

from cartopy.feature import NaturalEarthFeature

fig, ax = plt.subplots(1,1, figsize=(18, 36),
                       subplot_kw={'projection':ccrs.PlateCarree()})
coast = NaturalEarthFeature(category='physical', scale='110m',
                            facecolor='none', name='coastline')
feature = ax.add_feature(coast, edgecolor='black', linewidth=2.5)
ax.axis('off')
plt.savefig('im/coastlines_PC.png')

# load bluemarble with PIL
bm = PIL.Image.open('/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/im/' +
                    'Equirectangular_projection_SW.jpg')
bm = PIL.Image.open('/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/im/' +
                    'coastlines_PC.png')

# it's big, so I'll rescale it, convert to array, and divide by 256 to get RGB
# values that matplotlib accept
#bm = np.array(bm.resize([d/5 for d in bm.size]))/256.
bm = np.array(bm)/256.

# coordinates of the image - don't know if this is entirely accurate, but probably close
lons = np.linspace(-180, 180, bm.shape[1]) * np.pi/180
lats = np.linspace(-90, 90, bm.shape[0])[::-1] * np.pi/180

# repeat code from one of the examples linked to in the question, except for specifying facecolors:
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

x = np.outer(np.cos(lons), np.cos(lats)).T
y = np.outer(np.sin(lons), np.cos(lats)).T
z = np.outer(np.ones(np.size(lons)), np.sin(lats)).T
ax.plot_surface(x, y, z, rstride=4, cstride=4, facecolors = bm)

# plot some points above/around the globe
phi, theta = np.mgrid[0:pi:50j, 0:2*pi:50j]
r2 = 1.5*r
x = r2 * sin(phi) * cos(theta)
y = r2 * sin(phi) * sin(theta)
z = r2 * cos(phi) + 0.8* sin(sqrt(x**2 + y**2)) * cos(2*theta)
ax.scatter(x, y, z, s=1, color='red')

ax.set_axis_off()
ax.set_aspect(1)

plt.show()

# %%

import itertools

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
import numpy as np

import cartopy.feature
from cartopy.mpl.patch import geos_to_path
import cartopy.crs as ccrs

fig = plt.figure()
ax = Axes3D(fig, xlim=[-60, 90], ylim=[0, 90], zlim=[0, 18])
ax.set_zlim(bottom=0)
ax.view_init(azim=-70, elev=25)
ax.dist=7.5

target_projection = ccrs.PlateCarree(central_longitude=180)

feature = cartopy.feature.NaturalEarthFeature('physical', 'coastline', '110m')
geoms = feature.geometries()

geoms = [target_projection.project_geometry(geom, feature.crs)
         for geom in geoms]

paths = list(itertools.chain.from_iterable(geos_to_path(geom) for geom in geoms))

# At this point, we start working around mpl3d's slightly broken interfaces.
# So we produce a LineCollection rather than a PathCollection.
segments = []
for path in paths:
    vertices = [vertex for vertex, _ in path.iter_segments()]
    vertices = np.asarray(vertices)
    segments.append(vertices)

lc = LineCollection(segments, color='black')

ax.add_collection3d(lc)

ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Height')

# %%

from os.path import  dirname, abspath
from os import chdir, getcwd
chdir(dirname(abspath(__file__)))

from LIB import plot as pp
from LIB import calc as cc
from LIB import data as dd
from sys import argv
from datetime import datetime
from lagranto import Tra
from matplotlib import pyplot as plt
import numpy as np

# %% get the data

if argv[0]=='':
    date_0 = datetime(2016,5,1,6)
    tradir = date_0.strftime("/net/scratch/schoelleh96/WP2/WP2.1/" +
                             "LAGRANTO/wp21/traj/%Y/")
else:
    tradir = argv[1]
    date_0 = datetime(int(argv[2]),int(argv[3]),int(argv[4]),int(argv[5]))
    from matplotlib import use
    use('agg')

print("cwd: ", getcwd())
print("tradir:", tradir)
print("Date: ", date_0)

# define filename
traname = date_0.strftime('traj_%Y%m%d_%H')

trajs = Tra()
trajs.load_ascii(tradir + traname, gz=False)
trajs.set_array(trajs.get_array().transpose())#[:,:73])#three days only
print(trajs)

# %%
n_t = len(trajs['lon'][0])
n_tra = len(trajs['lon'])

for i in range(0,n_tra, 1):
    ax.plot(xs=trajs['lon'][i], trajs['lat'][i],
            cc.ptoh(trajs['p'][i], 1013.25, 8.435))#, c=trajs['TH'][i])


for i in range(0,n_tra, 1):
    print(i)
    segment = np.vstack((trajs['lon'][i], trajs['lat'][i]))
    lon0 = 180 #center of map
    bleft = lon0-181.
    bright = lon0+181.
    segment[0,segment[0]> bright] -= 360.
    segment[0,segment[0]< bleft]  += 360.
    threshold = 180.  # CHANGE HERE
    isplit = np.nonzero(np.abs(np.diff(segment[0])) > threshold)[0]
    subsegs = np.split(segment,isplit+1,axis=+1)

    #plot the tracks
    for seg in subsegs:
        x,y = seg[0],seg[1]

        segments = pp.make_segments(x, y)
        # lc = LineCollection(segments, color='black')

        lc = LineCollection(segments, array=trajs['TH'][i],
                            cmap=cmap, norm=norm, linewidth=0.8, alpha=0.2)
        ax.add_collection3d(lc, zs = cc.ptoh(trajs['p'][i,1:], 1013.25, 8.435))



for t in range(-(n_t - 1), 1):
    ax.scatter(trajs['lon'][::2,t]+180, trajs['lat'][::2,t],
               cc.ptoh(trajs['p'][::2,t], 1013.25, 8.435), c=trajs['TH'][::2,t])

    plt.show()


# %%

import cartopy.crs as ccrs
import cartopy.feature
from cartopy.mpl.patch import geos_to_path
import itertools
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d
from matplotlib.collections import PolyCollection, LineCollection
import numpy as np


def add_contourf3d(ax, contour_set):
    proj_ax = contour_set.collections[0].axes
    for zlev, collection in zip(contour_set.levels, contour_set.collections):
        paths = collection.get_paths()
        # Figure out the matplotlib transform to take us from the X, Y
        # coordinates to the projection coordinates.
        trans_to_proj = collection.get_transform() - proj_ax.transData

        paths = [trans_to_proj.transform_path(path) for path in paths]
        verts = [path.vertices for path in paths]
        codes = [path.codes for path in paths]
        pc = PolyCollection([])
        pc.set_verts_and_codes(verts, codes)

        # Copy all of the parameters from the contour (like colors) manually.
        # Ideally we would use update_from, but that also copies things like
        # the transform, and messes up the 3d plot.
        pc.set_facecolor(collection.get_facecolor())
        pc.set_edgecolor(collection.get_edgecolor())
        pc.set_alpha(collection.get_alpha())

        ax3d.add_collection3d(pc, zs=zlev)

    # Update the limit of the 3d axes based on the limit of the axes that
    # produced the contour.
    proj_ax.autoscale_view()

    ax3d.set_xlim(*proj_ax.get_xlim())
    ax3d.set_ylim(*proj_ax.get_ylim())
    ax3d.set_zlim(Z.min(), Z.max())

def add_feature3d(ax, feature, clip_geom=None, zs=None):
    """
    Add the given feature to the given axes.
    """
    concat = lambda iterable: list(itertools.chain.from_iterable(iterable))

    target_projection = ax.projection
    geoms = list(feature.geometries())

    if target_projection != feature.crs:
        # Transform the geometries from the feature's CRS into the
        # desired projection.
        geoms = [target_projection.project_geometry(geom, feature.crs)
                 for geom in geoms]

    if clip_geom:
        # Clip the geometries based on the extent of the map (because mpl3d
        # can't do it for us)
        geoms = [geom.intersection(clip_geom) for geom in geoms]

    # Convert the geometries to paths so we can use them in matplotlib.
    paths = concat(geos_to_path(geom) for geom in geoms)

    # Bug: mpl3d can't handle edgecolor='face'
    kwargs = feature.kwargs
    if kwargs.get('edgecolor') == 'face':
        kwargs['edgecolor'] = kwargs['facecolor']

    polys = concat(path.to_polygons(closed_only=False) for path in paths)

    if kwargs.get('facecolor', 'none') == 'none':
        lc = LineCollection(polys, **kwargs)
    else:
        lc = PolyCollection(polys, closed=False, **kwargs)
    ax3d.add_collection3d(lc, zs=zs)

# %%

def f(x, y):
    x, y = np.meshgrid(x, y)
    return (1 - x / 2 + x**5 + y**3 + x*y**2) * np.exp(-x**2 -y**2)


nx, ny = 512, 512
X = np.linspace(-180, 180, nx)
Y = np.linspace(10, 89, ny)
Z = f(np.linspace(-3, 3, nx), np.linspace(-3, 3, ny))


fig = plt.figure()
ax3d = fig.add_axes([0, 0, 1, 1], projection='3d')

# Make an axes that we can use for mapping the data in 2d.
proj_ax = plt.figure().add_axes([0, 0, 1, 1], projection=ccrs.NorthPolarStereo())
#cs = proj_ax.contourf(X, Y, Z, transform=ccrs.PlateCarree(), alpha=0.5)

ax3d.projection = proj_ax.projection
#add_contourf3d(ax3d, cs)
ax3d.scatter(X, Y, X, transform=ccrs.PlateCarree())

# Use the convenience (private) method to get the extent as a shapely geometry.
clip_geom = proj_ax._get_extent_geom().buffer(0)


zbase = ax3d.get_zlim()[0]
add_feature3d(ax3d, cartopy.feature.OCEAN, clip_geom, zs=zbase)
add_feature3d(ax3d, cartopy.feature.LAND, clip_geom, zs=zbase)
#add_feature3d(ax3d, cartopy.feature.COASTLINE, clip_geom, zs=zbase)

# Put the outline (neatline) of the projection on.
outline = cartopy.feature.ShapelyFeature(
    [proj_ax.projection.boundary], proj_ax.projection,
    edgecolor='black', facecolor='none')
#add_feature3d(ax3d, outline, clip_geom, zs=zbase)


# Close the intermediate (2d) figure
plt.close(proj_ax.figure)
plt.show()