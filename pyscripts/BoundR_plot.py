#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 16:29:08 2023

@author: schoelleh96
"""

# %% library imports

# This will move the console to the right working directory.
from os.path import  dirname, abspath, exists
from os import chdir, makedirs
chdir(dirname(abspath(__file__)))

from sys import argv, path
import numpy as np
path.append("./LIB")
import calc as cc
import data as dd
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import pandas as pd
import seaborn as sb

# %% Initials

if argv[0]=='':
    date_0 = "20160430_00"
    trapath = ("/net/scratch/schoelleh96/WP2/WP2.1/" +
                "LAGRANTO/wp21/era5/traj/2016/")
else:
    trapath = argv[1]
    date_0 = argv[2]
    from matplotlib import use
    use('agg')

# only every nth trajectory
n = 1

# %% functions

def calc_Bound_Ratio(trapath, date_0):

    trajs = np.load(trapath + "traj_" + date_0 + ".npy")

    Lon = trajs['lon'][::n]
    Lat = trajs['lat'][::n]
    P = trajs['p'][::n]
    U = trajs['U'][::n]
    V = trajs['V'][::n]
    Omg = trajs['OMEGA'][::n]
    T = trajs['T'][::n]

    # k_p = cc.calc_k(U/1000, V/1000, Omg/100)
    k_p=15

    x, y, z = cc.coord_trans(Lon, Lat, P, "None", k_p, proj="stereo")

    d_path = ("/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/csstandalone/dat_"
              + date_0 + "/" + str(n) + "/stereop")

    if not exists("/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/csstandalone/" +
                  "/dat_" + date_0 + "/" + str(n) + "/"):
        makedirs("/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/csstandalone/" +
                  "/dat_" + date_0 + "/" + str(n) + "/")

    bounds, hulls = dd.io_bounds(d_path, "$\\alpha$", x, y,
                                 z, 1e-3,
                                 True)

    Bound_list = list()

    for t in range(bounds.shape[1]):
        print(t)

        Bound_Ratio = bounds[:,t].sum()/bounds.shape[0]

        Bound_list.append(pd.DataFrame({'t' : t,
                                        'date' : date_0,
                                        'Bound_Ratio' : Bound_Ratio},
                                        index = [t]))
    Bound_Ratio = pd.concat(Bound_list, ignore_index=True)
    return Bound_Ratio

def plot_Bound_Ratio(Bound_Ratio, name):
    f, ax = plt.subplots(1, 1, figsize=(14,10))
    if name=="all":
        sb.set_palette("viridis", n_colors=len(Bound_Ratio['date'].unique()))
        sb.lineplot(data=Bound_Ratio, x="t", y="Bound_Ratio", hue="date",
                    ax=ax)
        # Modify the legend to show only every 4th entry
        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles[3::4], labels[3::4], title="Date")
    else:
        f, ax = plt.subplots(1, 1, figsize=(14,10))
        sb.lineplot(data=Bound_Ratio, x="t", y="Bound_Ratio", ax=ax,
                    color="blue")
    plt.savefig("../im/BoundR/2d" + name + ".png")

def plot_all_Bound_3d(Bound_Ratio, name, surf=False):
    fig = plt.figure(figsize=(14,10))
    if surf:
        ax = fig.add_subplot(111, projection='3d')

        # Generate a colormap based on the number of unique categories
        dates = Bound_Ratio['date'].unique()
        colors = plt.cm.viridis(np.linspace(0, 1, len(dates)))
        for idx, date in enumerate(dates):
            subset = Bound_Ratio[Bound_Ratio['date'] == date]
            ax.plot(subset['t'], [idx]*len(subset), subset['Bound_Ratio'],
                    label=date, color=colors[idx])
        ax.set_xlabel("t")
        ax.set_ylabel("date")
        ax.set_zlabel("bound ratio")
        ax.set_yticks(np.arange(3, len(dates), 4))
        ax.set_yticklabels(dates[3::4])
    else:
        ax = fig.add_subplot(111)

        def draw_contours(ax, data, levels):
            nrows, ncols = data.shape
            for i in range(nrows):
                for j in range(ncols):
                    value = data.iloc[i, j]
                    # Check vertical contours
                    if j < ncols - 1:
                        next_value = data.iloc[i, j+1]
                        for level in levels:
                            if ((value < level and next_value >= level) or
                                (value >= level and next_value < level)):
                                ax.plot([j+1, j+1], [i, i+1], color='k')
                    # Check horizontal contours
                    if i < nrows - 1:
                        next_value = data.iloc[i+1, j]
                        for level in levels:
                            if ((value < level and next_value >= level) or
                                (value >= level and next_value < level)):
                                ax.plot([j, j+1], [i+1, i+1], color='k')

        # Create the heatmap
        data = Bound_Ratio.pivot(index="date", columns="t",
                                 values="Bound_Ratio")
        sb.heatmap(data, annot=False, cmap="YlGnBu", ax=ax)
        dates = data.index.tolist()
        ax.set_yticks(np.arange(3.5, len(dates), 4))
        ax.set_yticklabels(dates[3::4])
        # Overlay contours
        contour_levels = [0.1, 0.2, 0.3, 0.4, 0.5]
        draw_contours(ax, data, contour_levels)

    plt.savefig("../im/BoundR/" + name + str(surf) + ".png")

# %% execution

if date_0 == "all":
    dates = [datetime(2016, 4, 29, 6) + timedelta(hours=i)
              for i in range(0, 144, 6)]
    Bound_list = list()
    for date in dates:
        BoundR = calc_Bound_Ratio(trapath, date.strftime('%Y%m%d_%H'))
        Bound_list.append(BoundR)
    Bound_Ratio = pd.concat(Bound_list, ignore_index=True)
else:
    Bound_Ratio = calc_Bound_Ratio(trapath, date_0)

plot_Bound_Ratio(Bound_Ratio, date_0)
# plot_all_Bound_3d(Bound_Ratio, date_0)
# plot_all_Bound_3d(Bound_Ratio, date_0, True)
