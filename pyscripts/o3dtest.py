#!/usr/bin/python3

# Test open3d functionality wrt surface reconstruction

import open3d as o3d
import numpy as np
from lagranto import Tra
from datetime import datetime
import alphashape
from matplotlib import pyplot as plt

# test bunny case

bunny = o3d.data.BunnyMesh()
mesh = o3d.io.read_triangle_mesh(bunny.path)
mesh.compute_vertex_normals()

pcd = mesh.sample_points_poisson_disk(750)
o3d.visualization.draw_geometries([pcd])

xyz = np.asarray(pcd.points)
pcd = o3d.geometry.PointCloud()
pcd.points = o3d.utility.Vector3dVector(xyz)
o3d.visualization.draw_geometries([pcd])

mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_alpha_shape(pcd, alpha=1/2)
mesh.compute_vertex_normals()
o3d.visualization.draw_geometries([mesh], mesh_show_back_face=True)

alp = alphashape.alphashape(xyz, 2)

fig = plt.figure()
ax = plt.axes(projection='3d')
ax.plot_trisurf(*zip(*alp.vertices), triangles=alp.faces)
plt.show()

tetra_mesh, pt_map = o3d.geometry.TetraMesh.create_from_point_cloud(pcd)

for alpha in np.logspace(np.log10(10), np.log10(0.01), num=6):
    print(f"alpha={alpha:.3f}")
    mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_alpha_shape(pcd, 1/alpha, tetra_mesh, pt_map)
    mesh.compute_vertex_normals()
    print("o3d vertices={}".format(np.asarray(mesh.vertices).shape))
#    o3d.visualization.draw_geometries([mesh], mesh_show_back_face=True)
    alp = alphashape.alphashape(xyz, alpha)
    print("alph vertices={}".format(alp.vertices.shape))
    # find points in both hulls
    both = (np.asarray(mesh.vertices)[:,None]==alp.vertices).all(-1).any(1)
    print("overlap={}".format(both.sum()))
#    fig = plt.figure()
#    ax = plt.axes(projection='3d')
#    ax.plot_trisurf(*zip(*alp.vertices), triangles=alp.faces)
#    plt.show()


# test ball pivoting

radii = [0.005, 0.01, 0.02, 0.04]
rec_mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_ball_pivoting(
    pcd, o3d.utility.DoubleVector(radii))
o3d.visualization.draw_geometries([pcd, rec_mesh])
    
# test start points
    
date_0 = datetime(2016,5,1,6)
tradir = date_0.strftime("/net/scratch/schoelleh96/WP2/WP2.1/" +
                         "LAGRANTO/wp21/era5/traj/%Y/")
traname = date_0.strftime('traj_%Y%m%d_%H')
trajs = Tra()
trajs.load_ascii(tradir + traname, gz=False)
trajs.set_array(trajs.get_array().transpose())
print(trajs)

Lon = trajs['lon']
Lat = trajs['lat']
P = trajs['p']
t_i = 0

LonNormed = (Lon[:,t_i] - Lon[:,t_i].mean())/(Lon[:,t_i].std())
LatNormed = (Lat[:,t_i] - Lat[:,t_i].mean())/(Lat[:,t_i].std())
pNormed = (P[:,t_i] - P[:,t_i].mean())/(P[:,t_i].std())

points = list(map(tuple, np.column_stack((LonNormed, LatNormed, pNormed))))

pcd2 = o3d.geometry.PointCloud()
pcd2.points = o3d.utility.Vector3dVector(points)

o3d.visualization.draw_geometries([pcd2])

rec_mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_ball_pivoting(
    pcd2, o3d.utility.DoubleVector(radii))
o3d.visualization.draw_geometries([pcd2, rec_mesh])


# not working: alpha shapes

mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_alpha_shape(pcd2, alpha=0.05)
mesh.compute_vertex_normals()
o3d.visualization.draw_geometries([mesh], mesh_show_back_face=True)

tetra_mesh, pt_map = o3d.geometry.TetraMesh.create_from_point_cloud(pcd2)

for alpha in np.logspace(np.log10(0.5), np.log10(0.01), num=4):
    print(f"alpha={alpha:.3f}")
    mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_alpha_shape(
        pcd2, alpha, tetra_mesh, pt_map)



