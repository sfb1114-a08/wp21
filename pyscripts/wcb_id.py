#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 12 12:39:42 2024

@author: schoelleh96
"""

# This will move the console to the right working directory.
from os.path import  dirname, abspath
from os import chdir
chdir(dirname(abspath(__file__)))

from sys import path
from datetime import datetime, timedelta
import numpy as np
path.append("./LIB")

import data as dd
import calc as cc

import pandas as pd
import pickle

# %% Get the data

# only every nth trajectory
n = 1

force_calc = False

YEAR="2017"
# Initialize dates
if YEAR == "2016":
    dates = [datetime(2016, 4, 29, 6) + timedelta(hours=i)
              for i in range(0, 144, 6)]
    # dates = [datetime(2016,5,6,0), datetime(2016,5,7,0)]
elif YEAR == "2017":
    dates = [datetime(2017, 1, 23, 18) + timedelta(hours=i)
              for i in range(0, 28*6, 6)]

for date_0 in dates:

    # date_0 = datetime(2016,5,2,0)
    fname = "../era5/traj/" + date_0.strftime('%Y/traj_%Y%m%d_%H') + ".npy"
    path = ("../../csstandalone/" + date_0.strftime("dat_%Y%m%d_%H") + "/" +
        str(n) + "/")
    trajs = np.load(fname)

    t_sel = np.arange(0,int(trajs.shape[1]/2)+1)

    alpha = 1e-3
    e = 50000
    k_p = 15
    # Full variables for boundary calculation
    Lon = trajs['lon'][::n]
    Lat = trajs['lat'][::n]
    P = trajs['p'][::n] # is in hPa
    Theta = trajs['TH'][::n, t_sel]


    # Variables at selected times for visualization and cs
    lon = Lon[:, t_sel]
    lat = Lat[:, t_sel]
    p = P[:, t_sel] # is in hPa

    # %% Calculate E

    xcs, ycs, zcs = cc.coord_trans(Lon, Lat, P, "None", proj="stereo",
                             scaling = k_p)
    # xcs, ycs, zcs = cc.norm(xcs), cc.norm(ycs), cc.norm(zcs)

    boundscs, hullscs = dd.io_bounds(
                path + "stereop", "$\\alpha$", xcs, ycs, zcs,
                alpha, False)

    bound_meth = "$\\alpha$"

    D = list((list(), list(), list()))
    E = dd.io_eigen((path + "spnalp" + str(alpha) + str(t_sel[0]) +
                     str(t_sel[-1])),
                    lon.shape[0], t_sel[0], t_sel[-1], D[0], D[1], D[2], e,
                    boundscs, 20, lon, lat, p, True, k_p, "p",
                    force_calc=force_calc)

    # %% find spectral gap and cluster

    Ediff = np.diff(np.real(E[0]))

    gap  = np.abs(Ediff[2:]).argmax()+2

    kclust = cc.kcluster_idx(E, gap+1)

    # %% identify wcb
    i = 0
    for c in np.unique(kclust.labels_):
        th_curr = Theta[kclust.labels_ == c, :]
        th_m = np.percentile(th_curr, 50, axis=0)
        if th_m[-1] - th_m[0] > 5:
            th_q1 = np.percentile(th_curr, 25, axis=0)
            th_q3 = np.percentile(th_curr, 75, axis=0)
            df = pd.DataFrame({'theta_m' : th_m,
                               'theta_q1' : th_q1,
                               'theta_q3' : th_q3,
                               't' : [date_0 -
                                      timedelta(hours=int(t_sel[-1] - i))
                                      for i in t_sel],
                               'start_date' : date_0,
                               'm' : (kclust.labels_ == c).sum()})
            with open("wcbs/" + date_0.strftime('%Y%m%d_%H') + "_" + str(i) +
                      ".pkl", "wb") as f:
                pickle.dump(df, f)
            i = i + 1