#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  1 16:29:23 2023

@author: schoelleh96
"""

# %% library imports

# This will move the console to the right working directory.
from os.path import  dirname, abspath
from os import chdir, getcwd
chdir(dirname(abspath(__file__)))

from LIB import plot as pp
from LIB import calc as cc
from LIB import data as dd
from sys import argv
from datetime import datetime
from lagranto import Tra
from matplotlib import pyplot as plt
from matplotlib.ticker import MaxNLocator
import numpy as np
import pandas as pd

# %% get the data

if argv[0]=='':
    date_0 = datetime(2016,5,1,6)
    tradir = date_0.strftime("/net/scratch/schoelleh96/WP2/WP2.1/" +
                             "LAGRANTO/wp21/traj/%Y/")
else:
    tradir = argv[1]
    date_0 = datetime(int(argv[2]),int(argv[3]),int(argv[4]),int(argv[5]))
    from matplotlib import use
    use('agg')

print("cwd: ", getcwd())
print("tradir:", tradir)
print("Date: ", date_0)

# define filename
traname = date_0.strftime('traj_%Y%m%d_%H')

trajs = Tra()
trajs.load_ascii(tradir + traname, gz=False)
trajs.set_array(trajs.get_array().transpose())#[:,:73])#three days only
print(trajs)

# %% coord trans
from scipy.sparse import csc_matrix, identity, find
from scipy.sparse.linalg import eigs
from scipy.linalg import eig

X = trajs['lon'].data
Y = trajs['lat'].data
p = trajs['p'].data

top = (p[:,0]!=150)

X = trajs['lon'].data[top,:]
Y = trajs['lat'].data[top,:]
p = trajs['p'].data[top,:]

# (X, Y) = dlonlat_dxy(0, trajs['lon'].data, 0, trajs['lat'].data)
# H = ptoh(trajs['p'].data, 1013.25, 8.435) # standard atmosphere

# X, Y, Z = cc.coord_trans(trajs['lon'].data, trajs['lat'].data,
#                          trajs['p'].data, v_trans="std_atm",
#                          scaling=1000)

sc = 100

X, Y, Z = cc.coord_trans(X, Y,
                         p, v_trans="std_atm",
                          scaling=sc)

# X = X[:, 24:]
# Y = Y[:, 24:]
# Z = Z[:, 24:]
# %% 3d im

for t in np.arange(0,X.shape[1]):
    f, x = pp.plotxyz3d(X[:,t], Y[:,t], Z[:,t], (np.min(X), np.max(X)),
                        (np.min(Y), np.max(Y)), (np.min(Z), np.max(Z)),
                        azim = 120, elev = 45, dist = 8,
                        c=p[:,0])
    plt.savefig("im/scat3d/" + date_0.strftime('traj_%Y%m%d_%H')
                + "{0:03}.png".format(t))

# %% spectra

f, ax = plt.subplots(3,3)
# radius = [100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000]
radius = [5000, 10000, 20000, 50000]

for i, r in enumerate(radius):
    # for e in [2000, 5000, 10000, 15000, 20000, 30000, 50000]:
    for e in [15000, 20000, 30000, 50000]:

        D = cc.distances(X, Y, r , X.shape[0], X.shape[1], dataZ=Z)
        P = csc_matrix((X.shape[0], X.shape[0]))
        for j in range(0, X.shape[1]):
            K = csc_matrix((np.exp(-D[2][j]**2/e), (D[0][j],D[1][j])))
            K = K.multiply(1/K.sum(axis=1))
            P = P + K

        Q = 1/(X.shape[1] - 0) * P

        sparsity = find(Q)[0].shape[0]/(Q.shape[0]*Q.shape[1])

        print("sparsity for epsilon {}, r {}: {}".format(e, r, sparsity))

        try:
            vals, vecs = eigs(Q, k=21)
        except:
            print("Eigs failed, using eig")
            vals, vecs = eig(Q.toarray())
        # E, kclust = cc.calc_cs(X, Y, Z, N_k=10, epsilon=e
        vals = np.flip(np.sort(vals))
        print(vals)

        vals = (vals - 1)/e
        ax.flatten()[i].scatter(x=np.arange(1,21), y=np.real(vals[:20]),
                                label=e)
        ax.flatten()[i].xaxis.set_major_locator(MaxNLocator(integer=True))

    ax.flatten()[i].legend(loc="lower left")
    ax.flatten()[i].set_title("R = {}".format(r))
f.suptitle("Scaling = {}".format(sc))
# %% clustering & embedding

f = plt.figure()

r=5000
e=50000
azim = 150
elev = 45
dist = 7
N_k = 2

D = cc.distances(X, Y, r , X.shape[0], X.shape[1], dataZ=Z)
P = csc_matrix((X.shape[0], X.shape[0]))
for j in range(0, X.shape[1]):
    K = csc_matrix((np.exp(-D[2][j]**2/e), (D[0][j],D[1][j])))
    K = K.multiply(1/K.sum(axis=1))
    P = P + K

Q = 1/(X.shape[1] - 0) * P

sparsity = find(Q)[0].shape[0]/(Q.shape[0]*Q.shape[1])

try:
    vals, vecs = eigs(Q, k=21)
except:
    print("Eigs failed, using eig")
    vals, vecs = eig(Q.toarray())
# E, kclust = cc.calc_cs(X, Y, Z, N_k=10, epsilon=e
vals = np.flip(np.sort(vals))
print(vals)

#N_k = ((vals - 1) > -1e-9).sum()

vals = (vals - 1)/e

# Eigenvalues

ax = f.add_subplot(2,2,1)
ax.scatter(x=(np.arange(1,21)).astype(int), y=np.real(vals[:20]),
           label="Eigenvalues for eps={} and R={}".format(e, r))
ax.legend(loc="lower left")
ax.xaxis.set_major_locator(MaxNLocator(integer=True))

kclust = cc.kcluster_idx((vals,vecs), N_k)

# Embedding

ax = f.add_subplot(2,2,2)

evecs = pd.DataFrame({'x': np.real(vecs[:,0]),
                     'y': np.real(vecs[:,1])})
evecs.x = np.sign(np.max(evecs.x) + np.min(evecs.x))*evecs.x
evecs.y = np.sign(np.max(evecs.y) + np.min(evecs.y))*evecs.y

ax.scatter(evecs.x, evecs.y, c=kclust.labels_)
ax.set_xlabel("EV 1")
ax.set_ylabel("EV 2")

ax = f.add_subplot(2,2,3)

evecs = pd.DataFrame({'x': np.real(vecs[:,2]),
                     'y': np.real(vecs[:,3])})
evecs.x = np.sign(np.max(evecs.x) + np.min(evecs.x))*evecs.x
evecs.y = np.sign(np.max(evecs.y) + np.min(evecs.y))*evecs.y

ax.scatter(evecs.x, evecs.y, c=kclust.labels_)
ax.set_xlabel("EV 3")
ax.set_ylabel("EV 4")

# clustering

lons = np.linspace(-180, 180,100)
lats = np.linspace(0, 90, 100)
(lons, lats) = np.meshgrid(lons, lats)
(x, y, z) = cc.lonlatrtoxyz(lons, lats, r=6371)

# for i in range(4,7):
#     ax = f.add_subplot(2,3,i, projection="3d")

#     ax.view_init(azim=azim, elev=elev)
#     ax.dist=dist
#     ax.set_xlim((np.min(X), np.max(X)))
#     ax.set_ylim((np.min(Y), np.max(Y)))
#     ax.set_zlim((np.min(Z), np.max(Z)))

#     # ax.plot_surface(x, y, z, color="blue", antialiased=False, alpha=0.2)
#     ax.plot_wireframe(x, y, z, color="blue", rstride=10, cstride=10, alpha=0.2)
#     t = int((X.shape[1]-1)*(i-4)/2)
#     ax.scatter(X[:,t],Y[:,t],Z[:,t],c=kclust.labels_)

from matplotlib.widgets import Slider

ax = f.add_subplot(2, 2, 4, projection="3d")
f.subplots_adjust(bottom=0.25)

ax.view_init(azim=azim, elev=elev)
ax.dist=dist
ax.set_xlim((np.min(X), np.max(X)))
ax.set_ylim((np.min(Y), np.max(Y)))
ax.set_zlim((np.min(Z), np.max(Z)))

# ax.plot_surface(x, y, z, color="blue", antialiased=False, alpha=0.2)
ax.plot_wireframe(x, y, z, color="blue", rstride=10, cstride=10, alpha=0.2)

t_0 = 0
points = ax.scatter(X[:,t_0],Y[:,t_0],Z[:,t_0],c=kclust.labels_)

axis_color = 'lightgoldenrodyellow'
t_slider_ax  = f.add_axes([0.1, 0.1, 0.8, 0.02], facecolor=axis_color)
t_slider = Slider(t_slider_ax, 'T', 0, X.shape[1]-1, valinit=t_0)

dist_slider_ax  = f.add_axes([0.1, 0.05, 0.8, 0.02], facecolor=axis_color)
dist_slider = Slider(dist_slider_ax, 'Zoom', 0, 10, valinit=dist)

def slider_on_changed(val):

    azim_i = ax.azim
    elev_i = ax.elev
    ax.clear()

    dist_i = dist_slider.val
    ax.view_init(azim=azim_i, elev=elev_i)
    ax.dist=dist_i
    ax.set_xlim((np.min(X), np.max(X)))
    ax.set_ylim((np.min(Y), np.max(Y)))
    ax.set_zlim((np.min(Z), np.max(Z)))

    # ax.plot_surface(x, y, z, color="blue", antialiased=False, alpha=0.2)
    ax.plot_wireframe(x, y, z, color="blue", rstride=10, cstride=10, alpha=0.2)

    points = ax.scatter(X[:,t_0],Y[:,t_0],Z[:,t_0],c=kclust.labels_)
    t_i = int(t_slider.val)

    points = ax.scatter(X[:,t_i],Y[:,t_i],Z[:,t_i],c=kclust.labels_)
    f.canvas.draw_idle()

t_slider.on_changed(slider_on_changed)
dist_slider.on_changed(slider_on_changed)

plt.show()

