#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 11 10:41:37 2023

@author: schoelleh96
"""

import cdsapi
import os

# c = cdsapi.Client()
# c.retrieve('reanalysis-era5-complete', { # Requests follow MARS syntax
#                                          # Keywords 'expver' and 'class' can be dropped. They are obsolete
#                                          # since their values are imposed by 'reanalysis-era5-complete'
#     'date'    : '2013-01-01',            # The hyphens can be omitted
#     'levelist': '1/10/100/137',          # 1 is top level, 137 the lowest model level in ERA5. Use '/' to separate values.
#     'levtype' : 'ml',
#     'param'   : '130',                   # Full information at https://apps.ecmwf.int/codes/grib/param-db/
#                                          # The native representation for temperature is spherical harmonics
#     'stream'  : 'oper',                  # Denotes ERA5. Ensemble members are selected by 'enda'
#     'time'    : '00/to/23/by/6',         # You can drop :00:00 and use MARS short-hand notation, instead of '00/06/12/18'
#     'type'    : 'an',
# }, 'output')                             # Output file; in this example containing fields in grib format. Adapt as you wish.


c = cdsapi.Client()

c.retrieve("reanalysis-era5-complete", {
    "class": "ea",
    "dataset": "era5",
    "date": "2016-05-01",
    "expver": "1",
    "levelist": "2000",
    "levtype": "pv",
    "param": "54.128",
    "stream": "oper",
    "time": "00:00:00/01:00:00/02:00:00/03:00:00/04:00:00/05:00:00/06:00:00/07:00:00/08:00:00/09:00:00/10:00:00/11:00:00/12:00:00/13:00:00/14:00:00/15:00:00/16:00:00/17:00:00/18:00:00/19:00:00/20:00:00/21:00:00/22:00:00/23:00:00",
    "type": "an",
    'grid'    : '1.0/1.0',               # Latitude/longitude. Default: spherical harmonics or reduced Gaussian grid
    'format'  : 'netcdf',                # Output needs to be regular lat-lon, so only works in combination with 'grid'!
}, "20160501Tropopause.nc")