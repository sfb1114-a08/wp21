#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 12:11:01 2023

@author: schoelleh96
"""

# =============================================================================
# This script calculates the agreement between classification based on lh and
# classification based on coherent sets. Usage: overlap_script.py <directory
# with trajectory data> <Year> <Month> <Day> <Hour>
# =============================================================================

# %% library imports

# This will move the console to the right working directory.
from os.path import  dirname, abspath
from os import chdir, getcwd
chdir(dirname(abspath(__file__)))

from LIB import plot as pp
from LIB import calc as cc
from LIB import data as dd
from sys import argv
from datetime import datetime
from lagranto import Tra
from itertools import product

# %% get the data

if argv[0]=='':
    tradir = "/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/traj/1981/"
    date_0 = datetime(1981,1,9,0)
else:
    tradir = argv[1]
    date_0 = datetime(int(argv[2]),int(argv[3]),int(argv[4]),int(argv[5]))

#print("cwd: ", getcwd())
#print("tradir:", tradir)
print("Date: ", date_0)

# define filename
traname = date_0.strftime('traj_%Y%m%d_%H')

trajs = Tra()
trajs.load_ascii(tradir + traname, gz=False)
trajs.set_array(trajs.get_array().transpose())#  three days only
print(trajs)

# %% calculate diabatic heating

dth7 = cc.dth_minmax(trajs['TH'],168)
dth3 = cc.dth_minmax(trajs['TH'],72)

# %% Coordinate Transformation, cs calculation and statistics

for max_T in [169, 73]:

    trajs.set_array(trajs.get_array()[:,:max_T])

    v_trans = "None"
    scaling = 1
    use_p = False

    dd.save_overlap((dth3>2)*1, v_trans, scaling, use_p, date_0, "dth3", max_T)
    dd.save_overlap((dth7>2)*1, v_trans, scaling, use_p, date_0, "dth7", max_T)

    for use_p in [False, True]:
        print("use_p: ", use_p)
        if use_p:
            for v_trans in ["std_atm"]:
                print("v_trans: ", v_trans)
                for scaling in [1, 100, 1000, -1]:
                    print("scaling: ", scaling)
                    X, Y, Z = cc.coord_trans(trajs['lon'].data,
                                             trajs['lat'].data,
                                             trajs['p'].data, v_trans, scaling)
                    dom, kclust = cc.calc_cs(X, Y, Z)
                    dd.save_overlap(kclust.labels_, v_trans, scaling, use_p,
                                    date_0, "cs", max_T)
        else:
            dom, kclust = cc.calc_cs(trajs['lat'].data, trajs['lon'].data)
            dd.save_overlap(kclust.labels_, v_trans, scaling, use_p, date_0,
                            "cs", max_T)
